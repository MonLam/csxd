# vue-app

> 基于Vue-CLI3的多应用脚手架

### 内置功能

- 多应用启动
- CDN配置定义
- proxy配置定义
- 常用的css及js工具
- 常用的通用组件

### 代码结构
```

├─src                   应用目录
│  ├─assets             公共资源目录（css、img等）
│  ├─common             公共类库目录（JS）
│  │  ├─filters         Vue-filters
│  │  ├─libs            通用的类库包
│  │  ├─plug            通用插件包
│  │  └─util.js         通用的工具方法
│  ├─components         公共组件目录
│  ├─pages              页面模块目录
│  │  ├─app             app模块项目
│  │  |  ├─main.js      页面入口文件
│  │  |  ├─app.vue      页面入口组件
│  │  |  ├─index.html   自定义页面html模版
|  |  |  └─...更多的目录文件
|  |  |
│  |  └─...更多模块
|  |
├─config                配置目录
|  ├─proxy.js           代理设置文件
│  └─cdn.js             cdn配置文件

```


### 模块项目定义

1. 在 src/pages 创建对应目录，目录名称即为模块名称
2. 在模块项目目录下创建 main.js 入口文件
3. 按需创建在模块项目目录下创建 index.html
4. 按需创建需要的文件目录即可，对应路径结构如下

```

├── src                                         应用目录
│   └── pages                                   页面模块目录
│       └── demo                                模块项目名称
│            ├── main.js                        模块项目入口文件（必须）
│            ├── index.html                     模块项目html模板（非必须，可自定义）
│            └── ...更多文件目录

```

## 指定项目启动或者打包

```base

# 运行项目，默认启动app项目模块
yarn dev

# 指定运行demo项目
yarn dev --page demo

# 打包项目，默认打包app项目模块
yarn build

# 打包指定demo项目
yarn build --page demo

```

## CDN配置

1. 修改 config/cdn.js 配置文件，以模块名称为节点名称进行配置
2. 重启【开发模式】


## proxy配置

1. 修改 config/proxy.js 配置文件，以模块名称为节点名称进行配置，配置方式请参考vue-cli proxy代理配置
2. 重启【开发模式】