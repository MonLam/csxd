'use strict'
const isProduction = process.env.NODE_ENV === 'production'
const WebpackCdnPlugin = require('webpack-cdn-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const cdns = require('./config/cdn.js')
const proxys = require('./config/proxy.js')

// 导入compression-webpack-plugin
// const CompressionWebpackPlugin = require('compression-webpack-plugin');
// 定义压缩文件类型
// const productionGzipExtensions = ['js', 'css']

const yargs = require('yargs')
const path = require('path')
const fs = require('fs')

// 获取项目页面名称
let project = (yargs.argv.page && yargs.argv.page !== true) ? yargs.argv.page : 'app'
// 获取项目页面路径
let project_path = path.join(__dirname, 'src/pages', project)
// 获取CDN配置
let cdn = cdns[project] || cdns['default'] || []
// 获取proxy配置
let proxyTable = proxys[project] || false
// 项目页面入口文件HTML模板
let template = path.join(project_path, 'index.html')
// 项目页面入口文件
let entry = path.join(project_path, 'main.js')
if (!fs.existsSync(entry)) {
    console.log('入口文件不存在：' + entry);
    process.exit();
}

// 输出提示
if (yargs.argv._[0] == 'serve' || yargs.argv._[0] == 'build') {
    console.log('')
    console.log('正在运行项目页面：' + project)
    console.log('运行项目页面路径：' + project_path)
    console.log('')
}

module.exports = {
    // 禁用eslint
    lintOnSave: false,
    // 打包时不生成.map文件
    productionSourceMap: false,
    // 打包资源存放路径
    assetsDir: './static/chat/' + project + '/',
    // 项目部署的基础路径 默认/，放在子目录时使用./或者加你的域名
    publicPath: isProduction ? '.' : '/',
    // 扩展chain配置
    chainWebpack: config => {
        // 生产环境配置
        if (isProduction) {
            // 存在项目自定义的CDN，配置CDN
            if (cdn) {
                config.plugin('cdn').use(WebpackCdnPlugin, [{
                    modules: cdn,
                    prodUrl: ':path'
                }])
            }

            // 增加manifest
            config.plugin('manifest').use(ManifestPlugin, [{
                fileName: 'manifest.json'
            }])
        }

        // 存在项目自定义的页面模板，则修改默认的页面模板
        if (fs.existsSync(template)) {
            config.plugin('html').tap(args => {
                args[0].template = template
                return args
            })
        }

        // 修复热更新HMR
        config.resolve.symlinks(true);
        // 移除 prefetch 插件
        config.plugins.delete('prefetch');
        // 移除 preload 插件
        config.plugins.delete('preload');
        // 设置目录别名alias
        config.resolve.alias
            .set('assets', '@/assets')
            .set('components', '@/components')
            .set('common', '@/common')
            .set('page', project_path)
            .set('view', 'page/view')
            .set('api', 'page/api')
    },
    // 扩展webpack配置
    configureWebpack: config => {
        // 定义入口文件
        config.entry.app = entry

        // if (isProduction) {
        //     plugins: [
        //         new CompressionWebpackPlugin({
        //             filename: '[path].gz[query]',
        //             algorithm: 'gzip',
        //             test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'), //匹配文件名
        //             threshold: 102409, //对100K以上的数据进行压缩
        //             minRatio: 0.8,
        //             deleteOriginalAssets: false, //是否删除源文件
        //         })
        //     ];
        // }
    },
    // 开发服务环境配置
    devServer: {
        // 默认启动serve 打开index页面
        index: 'index.html',
        // 默认打开页面
        // open: true,
        // 允许其他机器访问
        host: '0.0.0.0',
        // 默认打开8080端口
        port: 8080,
        // 不支持使用https
        https: false,
        hotOnly: false,
        // 设置跨域代理
        proxy: proxyTable,
    },
}