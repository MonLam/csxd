/**
 * 跨域代理配置文件
 */
module.exports = {
    app: {
        '/*': {
            target: 'https://gdmon.com/',
            secure: true,
            changeOrigin: true
        }
    },
    client: {
        '/*': {
            target: 'http://dev.test/',
            secure: true,
            changeOrigin: true
        }
    },
    waiter: {
        '/*': {
            target: 'http://dev.test/',
            secure: true,
            changeOrigin: true
        }
    }
}