/**
 * 模板生成构造器
 * 
 * @author Mon
 * @version 1.0.0
 */

const path = require('path')
const basePath = path.resolve(__dirname, '../src')
const commands = ['page', 'component'];
const buildComponent = require('./generae/component')
const buildPage = require('./generae/page')

// 获取生成类型
const generae = process.argv[2]
if (commands.indexOf(generae) < 0) {
    console.log('请输入支持的生成指令[page|component]')
    console.log('示例：yarn generae {page|component}')
    process.exit(0)
}

// 获取生成的page或者component名称
const name = process.argv[3]
if (!name) {
    console.log('名称不能为空！')
    console.log('示例：yarn generae page index')
    process.exit(0)
}

// 生成器生成
const obj = (generae == 'component') ? new buildComponent(basePath, name) : new buildPage(basePath, name)
if (!obj.build()) {
    console.log(obj.getError())
    process.exit(0)
}

console.log(`生成${generae} ${name}成功`)
process.exit(0)
