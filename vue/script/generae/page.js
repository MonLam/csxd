const fs = require('fs')

/**
 * 创建component
 */
module.exports = class Component {
    // 构造方法
    constructor(basePath, name) {
        this.basePath = basePath
        this.name = name
        this.error = null
    }

    // 构建文件
    build() {
        let mainTmp = `import Vue from 'vue'
import App from 'page/App.vue'
import filters from 'common/filters/index'
import 'assets/mon-ui/base.css'

// 注入全局过滤器
Object.keys(filters).forEach(item => {
    Vue.filter(item, filters[item])
})

Vue.config.productionTip = false
new Vue({
    render: h => h(App)
}).$mount('#app')
`

        let viewTmp = `<template>
    <div class="${this.name}-warp">
        {{componentName}}
    </div>
</template>
<script>
export default {
    name: '${this.name}',
    data(){
        return {
            componentName: '${this.name}'
        }
    }
}
</script>
<style lang="less">

</style>`

        // 创建目录及文件
        let dir = `${this.basePath}/pages/${this.name}`
        if (fs.existsSync(dir)) {
            this.error = `page ${this.name} exists!`
            return false
        }

        // 创建目录
        fs.mkdirSync(dir)
        // 创建文件
        fs.writeFileSync(`${dir}/index.js`, mainTmp)
        fs.writeFileSync(`${dir}/App.vue`, viewTmp)

        return true
    }

    // 获取错误信息
    getError() {
        let error = this.error
        this.error = null
        return error
    }
}