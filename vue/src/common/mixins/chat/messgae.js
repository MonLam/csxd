import Util from 'common/util'
/**
 * 处理通信聊天信息
 */
export default {
    data() {
        return {
            // 聊天列表
            chats: []
        }
    },
    methods: {
        // 发送图片信息
        chat_img(url, send, time) {
            // 通信时间
            let date = ''
            if (time) {
                date = Util.formatDate(time, 'm-d-H-i')
            }
            let temp = '<div class="' + send + '"><div class="message-tips">' + date + '</div><img src="' + url + '" class="file-img" alt="图片加载失败" /></div>';
            this.chats.push({
                type: send,
                message: false,
                content: temp
            });
        },
        // 写入tips信息
        chat_tips(str) {
            let temp = '<div class="tips">' + str + "</div>";
            this.chats.push({
                type: "tips",
                message: false,
                content: temp
            });
        },
        // 图文卡片信息
        chat_card(data, send, time) {
            // 通信时间
            let date = ''
            if (time) {
                date = Util.formatDate(time, 'm-d-H-i')
            }
            console.log(data)
            let temp = '<div class="message-tips">' + date + '</div>\
                            <a href="'+ data.link + '" target="_blank">\
                            <div class="message-card" title="'+ data.title + '">\
                                <div class="message-card-img"><img src="' + data.img + '" class="card-img" alt="图片加载失败" /></div>\
                                <div class="message-card-msg">\
                                    <div class="message-card-msg-title">'+ data.title + '</div>\
                                    <div class="message-card-msg-desc">'+ data.desc + '</div>\
                                </div>\
                            </a>\
                        </div>';
            this.chats.push({
                type: send,
                message: false,
                content: temp
            });
        },
        // 写入文字信息
        chat_msg(str, send, time) {
            // 替换a链接
            str = this._parseLink(str);

            // 替换所有的换行符
            str = str.replace(/\r\n/g, " <br> ");
            str = str.replace(/\n/g, " <br> ");
            // 替换所有的空格（中文空格、英文空格都会被替换）
            str = str.replace(/\s/g, "&nbsp;");
            // 替换a链接转义后特殊的空格符号，解决多个链接正则混淆匹配
            str = str.replace(/&my_nbsp;/ig, ' ');
            // 转换表情图片
            if (this.emoji) {
                str = this.emoji.parse(str);
            }
            // 通信时间
            let date = ''
            if (time) {
                date = Util.formatDate(time, 'm-d-H-i')
            }
            let postion = send == 'ask' ? 'right' : 'left';
            let temp = '<div class="' + send + '">\
                            <div class="message-tips">'+ date + '</div>\
                            <div class="message">\
                                <span class="arrow-' + postion + '-out"></span>\
                                <span class="arrow-' + postion + '-in"></span>\
                                ' + str + '\
                            </div>\
                        </div>';
            this.chats.push({
                type: send,
                message: false,
                content: temp
            });
        },
        // 解析生成A链接
        _parseLink(str) {
            let reg = /\[monlink:([a-zA-z]+:\/\/[^\s]*=>?[^\s]*):monlink\]/g;
            return str.replace(reg, function (str, target) {
                let tempArr = target.split("=>");
                let url = tempArr.shift();
                let title = tempArr.pop();
                title = title == "" ? url : title;
                // &my_nbsp;为特殊的空格表示位，需要再重新转换多一次空格，该设置解决多个链接的情况下，正则匹配为1个链接
                return (
                    '<a&my_nbsp;href="' + url + '"&my_nbsp;target="_blank">' + title + "</a>"
                );
            });
        },
    }
}