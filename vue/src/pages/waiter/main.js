import Vue from 'vue'
import App from 'page/App.vue'
import { router } from 'page/router'
import 'mon-util/mon-ui/base.css'
import './main.css'


// 绑定closet接口方法
if (!Element.prototype.closest) {
    Element.prototype.closest = function (el, selector) {
        const matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;
        while (el) {
            if (matchesSelector.call(el, selector)) {
                return el;
            } else {
                el = el.parentElement;
            }
        }
        return null;
    }
}

import { MonToastr as toastr } from 'mon-util'
Vue.use(toastr)

Vue.config.productionTip = false
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')