// Http服务
import Http from 'common/plug/Http'
// 事件工厂
import Event from 'common/plug/Eventbus'
// 提示组件
import { MonToastr as Toastr, MonLoadingModal as LoadingModal } from 'mon-util'
// 绑定事件
Event.on('http-query-faild', (target, data) => {
    let msg = data.msg || '请求失败';
    Toastr.send(msg)
}).on('http-response-faild', (target, error) => {
    Toastr.send(error.message)
}).on('http-before', (data) => {
    LoadingModal.start(2)
}).on('http-after', (data) => {
    LoadingModal.finish()
})

/**
 * 获取文章列表
 */
export const pull = (app, visitorid, user_id) => {
    let url = '/chat/pull/' + user_id + '/' + app + '/' + visitorid
    return Http.query({
        url: url
    })
}

/**
 * 获取客户信息
 */
export const getClientInfo = (visitorid) => {
    let url = '/chat/waiter/getClientInfo/' + visitorid
    return Http.query({
        url: url
    })
}

/**
 * 保存客户信息
 */
export const saveClientInfo = (visitorid, data) => {
    let url = '/chat/waiter/saveClientInfo/' + visitorid
    return Http.query({
        url: url,
        data: data,
        method: 'post'
    })
}

/**
 * 获取历史记录
 */
export const getHistory = (search) => {
    let url = '/chat/waiter/history'
    return Http.query({
        url: url,
        method: 'post',
        data: search
    })
}

/**
 * 修改密码
 */
export const modifyPwd = (old, pwd) => {
    let url = '/chat/waiter/modifypwd'
    return Http.query({
        url: url,
        method: 'post',
        data: {
            oldpwd: old,
            password: pwd
        }
    })
}

/**
 * 退出登录
 */
export const logout = () => {
    let url = '/chat/waiter/logout'
    return Http.query({
        url: url
    })
}

/**
 * 可转移客服列表
 */
export const transfer = (app) => {
    let url = '/chat/waiter/transfer'
    return Http.query({
        url: url,
        method: 'post',
        data: {
            app: app
        }
    })
}

/**
 * 图文信息列表
 */
 export const news = (app) => {
    let url = '/chat/waiter/news'
    return Http.query({
        url: url,
        method: 'post',
        data: {
            app: app
        }
    })
}


export default {
    pull,
    getClientInfo,
    saveClientInfo,
    getHistory,
    modifyPwd,
    logout,
    transfer,
    news
}