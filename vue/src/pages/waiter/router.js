import Vue from 'vue'
import Router from 'vue-router'
import {MonLoadingBar as LoadingBar} from 'mon-util';
Vue.use(Router)

/**
 * 路由配置
 */
export const routers = [
    {
        path: '/',
        name: 'chat',
        meta: {
            // 是否缓存组件
            keepAlive: true,
            title: '当前会话',
        },
        component: () => import('view/chat.vue')
    },
    {
        path: '/history',
        name: 'history',
        meta: {
            title: '历史会话',
        },
        component: () => import('view/history.vue')
    },
    {
        path: '/setting',
        name: 'setting',
        meta: {
            title: '设置',
        },
        component: () => import('view/setting.vue')
    },
]


// 注册路由
export const router = new Router({
    routes: routers
})

// 路由前置
router.beforeEach((to, from, next) => {
    LoadingBar.start()
    next()
})

// 路由后置
router.afterEach(to => {
    LoadingBar.finish()
    window.scrollTo(0, 0)
})
