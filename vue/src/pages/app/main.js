import Vue from 'vue'
import App from 'page/App.vue'
import router from 'page/router/index'
import store from 'page/store/index'
import filters from 'common/filters/index'
import 'mon-util/mon-ui/base.css'

// 引入自己开发的组件
import monUtil from 'mon-util'
Vue.use(monUtil)

// 注入全局过滤器
Object.keys(filters).forEach(item => {
    Vue.filter(item, filters[item])
})

// 事件工厂
import Event from 'common/plug/Eventbus'
// 绑定事件
Event.on('list', (args) => {
    console.log(args)
})
// .on('http-before', (target) => {
//     console.log(target, 123)
// })

// 挂载Vue.
Vue.use(Event)

// 验证器
import Validate from "common/plug/Validate";
// 挂载Vue.$validate
Vue.use(Validate)

// Http
import Http from 'common/plug/Http'
// 绑定事件
Event.on('http-query-faild', (target, error) => {
    console.log(target)
}).on('http-response-faild', (target, error) => {
    // console.log(target, error)
    console.log(JSON.parse(JSON.stringify(error)))
})
// 挂载Vue.$http
Vue.use(Http)

Vue.config.productionTip = false
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
