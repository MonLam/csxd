// Http服务
import Http from 'common/plug/Http'
// 事件工厂
import Event from 'common/plug/Eventbus'
// 提示组件
import { MonToastr as Toastr, MonLoadingModal as LoadingModal } from 'mon-util'
// 绑定事件
Event.on('http-query-faild', (target, data) => {
    let msg = data.msg || '请求失败';
    Toastr.send(msg)
}).on('http-response-faild', (target, error) => {
    Toastr.send(error.message)
}).on('http-before', (data) => {
    LoadingModal.start(2)
}).on('http-after', (data) => {
    LoadingModal.finish()
})

/**
 * 获取文章列表
 */
export const pull = (app, visitorid, user_id) => {
    let url = '/chat/pull/' + user_id + '/' + app + '/' + visitorid
    return Http.query({
        url: url
    })
}

export default {
    pull
}