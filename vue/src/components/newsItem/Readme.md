## 新闻图文列表组件

### 参数

| 参数名 | 类型 | 是否必须 | 描述 |
| -------- | -------- | -------- | -------- |
| title     | String     | 是     | 图文栏目标题     |
| item     | Object     | 否     | 图文栏目详细信息，具体请看item说明     |

#### item参数说明

| 参数名 | 类型 | 描述 |
| -------- | -------- | -------- |
| img     | String     | 图片URL，不为空则渲染图片     |
| desc     | String     | 内容简述     |
| user     | String     | 用户名称     |
| date     | String     | 时间日期     |
| read     | String|Number    | 阅读数     |

更多参数可自行添加，item的数据回通过事件回调返回给上层业务组件

### 事件

| 事件名  | 描述 |
| --------  | -------- |
| clickEvent     | 点击事件，根据点击的元素返回参数     |

#### clickEvent事件参数说明

| 参数名 | 类型 | 描述 |
| -------- | -------- | -------- |
| el     | String     | 点击位置，包含title、img、user     |
| item     | Object     | 组件item数据     |
| title     | String     | 组件title数据     |

