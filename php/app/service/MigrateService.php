<?php

namespace app\service;

use mon\orm\Db;
use mon\env\Config;
use mon\util\Instance;
use FilesystemIterator;
use app\libs\exception\MigrateException;

/**
 * Mysql数据备份迁移服务
 * 
 * @author Mon <985558837@qq.com>
 * @version 1.0.0
 */
class MigrateService
{
    use Instance;

    /**
     * 文件指针
     *
     * @var resource
     */

    private $fp = null;

    /**
     * 备份文件信息 part - 卷号，name - 文件名
     *
     * @var array
     */
    private $file = [];

    /**
     * 当前打开文件大小
     *
     * @var integer
     */
    private $size = 0;

    /**
     * 数据库配置
     *
     * @var array
     */
    private $dbconfig = [];

    /**
     * 备份配置
     *
     * @var array
     */
    private $config = [
        // 数据库备份路径
        'path' => './data/',
        // 数据库备份卷大小
        'part' => 20971520,
        // 数据库备份文件是否启用压缩 0不压缩 1 压缩
        'compress' => 0,
        // 压缩级别
        'level' => 9,
    ];

    /**
     * 数据库备份构造方法
     *
     * @param array $config 配置信息
     */
    public function __construct($config = [])
    {
        if (!$config) {
            $config = Config::instance()->get('admin.migrate', []);
        }
        $this->config = array_merge($this->config, $config);
        // 初始化文件名
        $this->setFile();
        // 初始化数据库连接参数
        $this->setDbConfig();
    }

    /**
     * 析构方法，用于关闭文件资源
     */
    public function __destruct()
    {
        if (!is_null($this->fp)) {
            $this->config['compress'] ? @gzclose($this->fp) : @fclose($this->fp);
        }
    }

    /**
     * 获取MySQL链接
     *
     * @return \mon\orm\db\Query
     */
    public function connect()
    {
        return Db::connect($this->dbconfig);
    }

    /**
     * 设置配置信息
     *
     * @param array $config 配置信息
     * @return MigrateService
     */
    public function setConfig(array $config = [])
    {
        $this->config = array_merge($this->config, $config);
        return $this;
    }

    /**
     * 设置备份文件名
     *
     * @param mixed $file 文件信息
     * @return MigrateService
     */
    public function setFile($file = null)
    {
        if (is_null($file)) {
            $this->file = ['name' => date('Ymd-His'), 'part' => 1];
        } else {
            if (!array_key_exists("name", $file) && !array_key_exists("part", $file)) {
                $this->file = $file['1'];
            } else {
                $this->file = $file;
            }
        }

        return $this;
    }

    /**
     * 设置数据库连接必备参数
     *
     * @param array $dbconfig 数据库连接配置信息
     * @return MigrateService
     */
    public function setDbConfig($dbconfig = [])
    {
        if (empty($dbconfig)) {
            $this->dbconfig = Config::instance()->get('database', []);
        } else {
            $this->dbconfig = $dbconfig;
        }
        return $this;
    }

    /**
     * 获取数据库表数据
     *
     * @param string $table 表名
     * @param boolean $status 是否查询表状态
     * @return array
     */
    public function tableData($table = null, $status = true)
    {
        if (is_null($table)) {
            $list = $this->connect()->query("SHOW TABLE STATUS");
        } else {
            if ($status) {
                $list = $this->connect()->query("SHOW FULL COLUMNS FROM `{$table}`");
            } else {
                $list = $this->connect()->query("SHOW COLUMNS FROM `{$table}`");
            }
        }
        return array_map('array_change_key_case', (array) $list);
    }

    /**
     * 数据库备份文件列表
     *
     * @throws MigrateException
     * @return array
     */
    public function fileList()
    {
        // 检查文件是否可写
        if (!$this->checkPath($this->config['path'])) {
            throw new MigrateException("The current directory is not writable");
        }
        $path = realpath($this->config['path']);
        $flag = FilesystemIterator::KEY_AS_FILENAME;
        $glob = new FilesystemIterator($path, $flag);
        $list = array();
        foreach ($glob as $name => $file) {
            if (preg_match('/^\\d{8,8}-\\d{6,6}-\\d+\\.sql(?:\\.gz)?$/', $name)) {
                $info['filename'] = $name;
                $name = sscanf($name, '%4s%2s%2s-%2s%2s%2s-%d');
                $date = "{$name[0]}-{$name[1]}-{$name[2]}";
                $time = "{$name[3]}:{$name[4]}:{$name[5]}";
                $part = $name[6];
                if (isset($list["{$date} {$time}"])) {
                    $info = $list["{$date} {$time}"];
                    $info['part'] = max($info['part'], $part);
                    $info['size'] = $info['size'] + $file->getSize();
                } else {
                    $info['part'] = $part;
                    $info['size'] = $file->getSize();
                }
                $extension = strtoupper(pathinfo($file->getFilename(), PATHINFO_EXTENSION));
                $info['compress'] = $extension === 'SQL' ? '-' : $extension;
                $info['time'] = strtotime("{$date} {$time}");
                $list["{$date} {$time}"] = $info;
            }
        }
        return $list;
    }

    /**
     * 获取备份的数据库文件信息
     * 
     * @param string $type  操作类型
     * @param integer $time 时间戳
     * @throws MigrateException
     * @return array|false|string
     */
    public function getFile($type = '', $time = 0)
    {
        if (!is_numeric($time)) {
            throw new MigrateException("{$time} Illegal data type");
        }
        switch ($type) {
            case 'time':
                $name = date('Ymd-His', $time) . '-*.sql*';
                $path = realpath($this->config['path']) . DIRECTORY_SEPARATOR . $name;
                return glob($path);
            case 'timeverif':
                $name = date('Ymd-His', $time) . '-*.sql*';
                $path = realpath($this->config['path']) . DIRECTORY_SEPARATOR . $name;
                $files = glob($path);
                $list = array();
                foreach ($files as $name) {
                    $basename = basename($name);
                    $match = sscanf($basename, '%4s%2s%2s-%2s%2s%2s-%d');
                    $gz = preg_match('/^\\d{8,8}-\\d{6,6}-\\d+\\.sql.gz$/', $basename);
                    $list[$match[6]] = array($match[6], $name, $gz);
                }
                $last = end($list);
                if (count($list) === $last[0]) {
                    return $list;
                } else {
                    throw new MigrateException("File {$files['0']} may be damaged, please check again");
                }
            case 'pathname':
                return "{$this->config['path']}{$this->file['name']}-{$this->file['part']}.sql";
            case 'filename':
                return "{$this->file['name']}-{$this->file['part']}.sql";
            case 'filepath':
                return $this->config['path'];
            default:
                $arr = array('pathname' => "{$this->config['path']}{$this->file['name']}-{$this->file['part']}.sql", 'filename' => "{$this->file['name']}-{$this->file['part']}.sql", 'filepath' => $this->config['path'], 'file' => $this->file);
                return $arr;
        }
    }

    /**
     * 删除备份文件
     *
     * @param integer $time 时间戳
     * @throws MigrateException
     * @return mixed
     */
    public function del($time)
    {
        if ($time) {
            $filePathArr = $this->getFile('time', $time);
            array_map("unlink", $filePathArr);
            if (count($this->getFile('time', $time))) {
                throw new MigrateException("File {$time} deleted failed");
            } else {
                return $time;
            }
        } else {
            throw new MigrateException("{$time} Time parameter is incorrect");
        }
    }

    /**
     * 下载备份
     *
     * @param integer $time 文件名时间戳
     * @param integer $part
     * @throws MigrateException
     * @return void
     */
    public function download($time, $part = 0)
    {
        $file = $this->getFile('time', $time);
        $fileName = $file[$part];
        if (file_exists($fileName)) {
            ob_end_clean();
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($fileName));
            header('Content-Disposition: attachment; filename=' . basename($fileName));
            readfile($fileName);
        } else {
            throw new MigrateException("{$time} File is abnormal");
        }
    }

    /**
     * 导入SQL，还原数据
     *
     * @param mixed $start
     * @return mixed
     */
    public function import($start)
    {
        if ($this->config['compress']) {
            $gz = gzopen($this->file[1], 'r');
            $size = 0;
        } else {
            $size = filesize($this->file[1]);
            $gz = fopen($this->file[1], 'r');
        }
        $sql = '';
        if ($start) {
            $this->config['compress'] ? gzseek($gz, $start) : fseek($gz, $start);
        }
        for ($i = 0; $i < 1000; $i++) {
            $sql .= $this->config['compress'] ? gzgets($gz) : fgets($gz);
            if (preg_match('/.*;$/', trim($sql))) {
                if (false !== $this->connect()->execute($sql)) {
                    $start += strlen($sql);
                } else {
                    return false;
                }
                $sql = '';
            } elseif ($this->config['compress'] ? gzeof($gz) : feof($gz)) {
                return 0;
            }
        }
        return [$start, $size];
    }

    /**
     * 备份表结构
     *
     * @param string $table 表名
     * @param integer $start 起始位
     * @return boolean|integer
     */
    public function backup($table, $start = 0)
    {
        // 备份表结构
        if (0 == $start) {
            $result = $this->connect()->query("SHOW CREATE TABLE `{$table}`");
            $sql = "\n";
            $sql .= "-- -----------------------------\n";
            $sql .= "-- Table structure for `{$table}`\n";
            $sql .= "-- -----------------------------\n";
            $sql .= "DROP TABLE IF EXISTS `{$table}`;\n";
            $sql .= trim($result[0]['Create Table']) . ";\n\n";
            if (false === $this->write($sql)) {
                return false;
            }
        }
        // 数据总数
        $count = $this->connect()->table($table)->count();
        // 备份表数据
        if ($count) {
            // 写入数据注释
            if (0 == $start) {
                $sql = "-- -----------------------------\n";
                $sql .= "-- Records of `{$table}`\n";
                $sql .= "-- -----------------------------\n";
                $this->write($sql);
            }
            // 备份数据记录
            $result = $this->connect()->query("SELECT * FROM `{$table}` LIMIT :min, 1000", ['min' => intval($start)], true);
            foreach ($result as $row) {
                $row = array_map('addslashes', $row);
                $sql = "INSERT INTO `{$table}` VALUES ('" . str_replace(array("\r", "\n"), array('\\r', '\\n'), implode("', '", $row)) . "');\n";
                if (false === $this->write($sql)) {
                    return false;
                }
            }
            // 还有更多数据
            if ($count > $start + 1000) {
                return $this->backup($table, $start + 1000);
            }
        }
        // 备份下一表
        return 0;
    }

    /**
     * 优化表
     *
     * @param array|string $tables
     * @throws MigrateException
     * @return void
     */
    public function optimize($tables)
    {
        if ($tables) {
            if (is_array($tables)) {
                $tables = implode('`,`', $tables);
                $list = $this->connect()->query("OPTIMIZE TABLE `{$tables}`");
            } else {
                $list = $this->connect()->query("OPTIMIZE TABLE `{$tables}`");
            }
            if (!$list) {
                throw new MigrateException("data sheet'{$tables}'Repair mistakes please try again!");
            }
        } else {
            throw new MigrateException("Please specify the table to be repaired!");
        }
    }

    /**
     * 修复表
     *
     * @param string|null $tables 
     * @throws MigrateException
     * @return array
     */
    public function repair($tables)
    {
        if ($tables) {
            if (is_array($tables)) {
                $tables = implode('`,`', $tables);
                $list = $this->connect()->query("REPAIR TABLE `{$tables}`");
            } else {
                $list = $this->connect()->query("REPAIR TABLE `{$tables}`");
            }
            if ($list) {
                return $list;
            } else {
                throw new MigrateException("data sheet'{$tables}'Repair mistakes please try again!");
            }
        } else {
            throw new MigrateException("Please specify the table to be repaired!");
        }
    }

    /**
     * 写入SQL语句
     *
     * @param string $sql 要写入的SQL语句
     * @return boolean     true - 写入成功，false - 写入失败！
     */
    protected function write($sql)
    {
        $size = strlen($sql);
        // 由于压缩原因，无法计算出压缩后的长度，这里假设压缩率为50%，
        // 一般情况压缩率都会高于50%；
        $size = $this->config['compress'] ? $size / 2 : $size;
        $this->open($size);
        return $this->config['compress'] ? @gzwrite($this->fp, $sql) : @fwrite($this->fp, $sql);
    }

    /**
     * 打开一个文件，用于写入数据
     *
     * @param integer $size 写入数据的大小
     * @return void
     */
    protected function open($size)
    {
        if ($this->fp) {
            $this->size += $size;
            if ($this->size > $this->config['part']) {
                $this->config['compress'] ? @gzclose($this->fp) : @fclose($this->fp);
                $this->fp = null;
                $this->file['part']++;
                $this->backupInit();
            }
        } else {
            $backuppath = $this->config['path'];
            $filename = "{$backuppath}{$this->file['name']}-{$this->file['part']}.sql";
            if ($this->config['compress']) {
                $filename = "{$filename}.gz";
                $this->fp = @gzopen($filename, "a{$this->config['level']}");
            } else {
                $this->fp = @fopen($filename, 'a');
            }
            $this->size = filesize($filename) + $size;
        }
    }

    /**
     * 写入初始数据
     *
     * @return boolean true - 写入成功，false - 写入失败
     */
    protected function backupInit()
    {
        $sql = "-- -----------------------------\n";
        $sql .= "-- Mon MySQL Data Transfer \n";
        $sql .= "-- \n";
        $sql .= "-- Host     : " . $this->dbconfig['host'] . "\n";
        $sql .= "-- Port     : " . $this->dbconfig['port'] . "\n";
        $sql .= "-- Database : " . $this->dbconfig['database'] . "\n";
        $sql .= "-- \n";
        $sql .= "-- Part : #{$this->file['part']}\n";
        $sql .= "-- Date : " . date("Y-m-d H:i:s") . "\n";
        $sql .= "-- -----------------------------\n\n";
        $sql .= "SET FOREIGN_KEY_CHECKS = 0;\n\n";
        return $this->write($sql);
    }

    /**
     * 检查目录是否存在
     *
     * @param string $path 文件目录路径
     * @return boolean
     */
    protected function checkPath($path)
    {
        if (is_dir($path)) {
            return true;
        }
        if (mkdir($path, 0755, true)) {
            return true;
        }
        return false;
    }
}
