<?php

namespace app\service;

use mon\orm\Db;
use mon\util\Instance;

/**
 * 省市区服务
 * 
 * @author Mon <985558837@qq.com>
 * @version 1.0.0
 */
class LocationService
{
    use Instance;

    /**
     * 省表
     *
     * @var string
     */
    protected $province = 'region_provinces';

    /**
     * 市表
     *
     * @var string
     */
    protected $city = 'region_cities';

    /**
     * 区表
     * 
     * @var string
     */
    protected $area = 'region_areas';

    /**
     * 获取省份信息
     *
     * @param integer $province_code 省份代码
     * @return array
     */
    public function getProvince($province_code = '')
    {
        $where = [];
        if ($province_code) {
            $where['province_code'] = $province_code;
        }
        $data = Db::table($this->province)->where($where)->field('name, province_code')->select();
        return $data;
    }

    /**
     * 获取城市信息
     *
     * @param integer $province_code 省份代码
     * @param integer $city_code 城市代码
     * @return array
     */
    public function getCity($province_code = '', $city_code = '')
    {
        $where = [];
        if ($province_code) {
            $where['province_code'] = $province_code;
        }
        if ($city_code) {
            $where['city_code'] = $city_code;
        }
        $data = Db::table($this->city)->where($where)->field('name, province_code, city_code')->select();
        return $data;
    }

    /**
     * 获取县区信息
     *
     * @param integer $city_code 城市代码
     * @param integer $area_code 县区代码
     * @return array
     */
    public function getArea($city_code = '', $area_code = '')
    {
        $where = [];
        if ($city_code) {
            $where['city_code'] = $city_code;
        }
        if ($area_code) {
            $where['area_code'] = $area_code;
        }
        $data = Db::table($this->area)->where($where)->field('name, area_code, city_code')->select();
        return $data;
    }
}
