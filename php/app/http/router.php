<?php
/*
|--------------------------------------------------------------------------
| 定义应用请求路由
|--------------------------------------------------------------------------
| 使用$router可直接定义路由，或通过Route类进行注册
|
*/

use app\http\middleware\befor\AdminAuth;
use app\http\middleware\befor\ChatLogin;
use app\http\middleware\befor\AdminLogin;

// 首页
$router->any('/', 'app\http\controller\IndexController@index');

// 软件安装
$router->any('/install', 'app\http\controller\InstallController@run');

// 即时通信聊天模块接口路由
$router->group(['path' => '/chat', 'namespace' => 'app\http\controller\chat\\'], function ($router) {
    // 获取聊天记录
    $router->get('/pull/{waiter_id}/{app}/{visitid}', 'ClientController@pull');
    // 获取JS
    $router->get('/js/{app}', 'ClientController@js');
    // 客户留言验证码
    $router->get('/client/captcha', 'ClientController@captcha');
    // 客户端
    $router->map(['get', 'post'], '/client/{app}', 'ClientController@index');
    // 客服登录
    $router->map(['get', 'post'], '/waiter/login', 'WaiterController@login');
    // 获取客服登录验证码
    $router->get('/waiter/captcha', 'WaiterController@captcha');
    // 上传图片
    $router->post('/upload', 'ClientController@upload');
    // 客服相关接口
    $router->group(['path' => '/waiter', 'befor' => ChatLogin::class], function ($router) {
        // 主页
        $router->get('', 'WaiterController@index');
        $router->get('/index', 'WaiterController@index');
        // 获取客户信息
        $router->get('/getClientInfo/{visitid}', 'WaiterController@getClientInfo');
        // 保存客服信息
        $router->post('/saveClientInfo/{visitid}', 'WaiterController@saveClientInfo');
        // 获取历史记录
        $router->post('/history', 'WaiterController@history');
        // 修改密码
        $router->post('/modifypwd', 'WaiterController@modifyPassword');
        // 退出登录
        $router->any('/logout', 'WaiterController@logout');
        // 获取允许转接的客服
        $router->post('/transfer', 'WaiterController@transfer');
        // 获取图文信息
        $router->post('/news', 'WaiterController@news');
    });
});


// 管理端相关路由配置
$router->group(['path' => '/admin', 'namespace' => 'app\http\controller\admin\\'], function ($router) {
    // 首页
    $router->get('', 'PassportController@identify');
    $router->get('/', 'PassportController@identify');
    $router->get('/index', 'PassportController@identify');
    // 用户登陆
    $router->map(['get', 'post'], '/login', 'PassportController@login');
    // 获取验证码
    $router->get('/captcha', 'PassportController@captcha');
    // 退出登陆
    $router->map(['get', 'post'], '/logout', 'PassportController@logout');

    // 基础接口
    $router->group(['path' => '/console', 'befor' => AdminLogin::class], function ($router) {
        // 管理端桌面
        $router->get('', 'ConsoleController@index');
        // 桌面
        $router->get('/desktop', 'ConsoleController@desktop');
        // 文件上传
        $router->post('/upload', 'ConsoleController@upload');
        // 修改当前用户信息
        $router->map(['get', 'post'], '/edit', 'ConsoleController@editUserInfo');
        // 获取七牛云token
        $router->post('/qiniu', 'ConsoleController@qiniu');
    });

    // 系统管理
    $router->group(['path' => '/sys', 'befor' => [AdminAuth::class], 'namespace' => 'app\http\controller\admin\sys\\'], function ($router) {
        // 权限相关
        $router->group(['path' => '/auth', 'namespace' => 'app\http\controller\admin\sys\auth\\'], function ($router) {
            // 用户管理
            $router->group(['path' => '/admin'], function ($router) {
                // 查看管理员
                $router->get('', 'AdminController@index');
                // 新增管理员
                $router->map(['get', 'post'], '/add', 'AdminController@add');
                // 编辑管理员信息
                $router->map(['get', 'post'], '/edit', 'AdminController@edit');
                // 重置管理员密码
                $router->map(['get', 'post'], '/password', 'AdminController@password');
                // 封解冻管理员
                $router->post('/toggle', 'AdminController@toggle');
            });

            // 角色组别
            $router->group(['path' => '/group'], function ($router) {
                // 查看角色组别
                $router->get('', 'GroupController@index');
                // 新增角色组别
                $router->map(['get', 'post'], '/add', 'GroupController@add');
                // 编辑角色组别
                $router->map(['get', 'post'], '/edit', 'GroupController@edit');
                // 角色组别权限规则树
                $router->post('/role', 'GroupController@role');
                // 角色组别关联用户
                $router->get('/user', 'GroupController@user');
                // 角色组别绑定用户
                $router->map(['get', 'post'], '/bind', 'GroupController@bindUser');
                // 角色组别用户解绑
                $router->post('/unbind', 'GroupController@unbindUser');
            });

            // 权限规则
            $router->group(['path' => '/rule'], function ($router) {
                // 查看权限规则
                $router->get('', 'RuleController@index');
                // 新增权限规则
                $router->map(['get', 'post'], '/add', 'RuleController@add');
                // 编辑权限规则
                $router->map(['get', 'post'], '/edit', 'RuleController@edit');
            });
        });

        // 菜单管理
        $router->group(['path' => '/menu'], function ($router) {
            // 查看菜单
            $router->get('', 'MenuController@index');
            // 新增菜单
            $router->map(['get', 'post'], '/add', 'MenuController@add');
            // 编辑菜单
            $router->map(['get', 'post'], '/edit', 'MenuController@edit');
        });

        // 配置管理
        $router->group(['path' => '/setting'], function ($router) {
            // 查看配置信息
            $router->get('', 'SettingController@index');
            // 新增配置信息
            $router->map(['get', 'post'], '/add', 'SettingController@add');
            // 编辑配置信息
            $router->map(['get', 'post'], '/edit', 'SettingController@edit');
        });

        // 数据备份
        $router->group(['path' => '/backup'], function ($router) {
            // 查看备份数据
            $router->get('', 'MigrateController@index');
            // 查看表列表
            $router->get('/table', 'MigrateController@table');
            // 查看表结构
            $router->get('/field', 'MigrateController@field');
            // 优化表
            $router->post('/optimize', 'MigrateController@optimize');
            // 修复表
            $router->post('/repair', 'MigrateController@repair');
            // 备份表
            $router->post('/save', 'MigrateController@save');
            // 删除备份
            $router->post('/del', 'MigrateController@del');
            // 下载备份
            $router->get('/download', 'MigrateController@download');
            // 导入备份
            $router->post('/import', 'MigrateController@import');
            // 数据字典
            $router->get('/dictionary', 'MigrateController@dictionary');
        });

        // 文件管理
        $router->group(['path' => '/filemanage'], function ($router) {
            // 查看页面，获取目录内容
            $router->map(['get', 'post'], '', 'FileManageController@index');
            // 创建目录
            $router->post('/mkdir', 'FileManageController@mkdir');
            // 文件上传
            $router->post('/upload', 'FileManageController@upload');
            // 重命名
            $router->post('/rename', 'FileManageController@rename');
            // 删除
            $router->post('/del', 'FileManageController@del');
        });

        // 清理数据
        $router->group(['path' => '/clear'], function ($router) {
            // 查看页面
            $router->get('', 'ClearController@index');
            // 清除文件缓存
            $router->post('/cache', 'ClearController@cache');
        });
    });

    // 常规管理
    $router->group(['path' => '/general', 'befor' => [AdminAuth::class], 'namespace' => 'app\http\controller\admin\general\\'], function ($router) {
        // 资源管理
        $router->group(['path' => '/assets'], function ($router) {
            // 查看资源信息
            $router->get('', 'AssetsController@index');
            // 新增资源信息
            $router->map(['get', 'post'], '/add', 'AssetsController@add');
            // 编辑资源信息
            $router->map(['get', 'post'], '/edit', 'AssetsController@edit');
            // 选择资源
            $router->map(['get', 'post'], '/select', 'AssetsController@select');
        });

        // 日志流水
        $router->group(['path' => '/log'], function ($router) {
            // 管理员日志
            $router->get('/admin', 'LogController@admin');
        });
    });

    // 客服通信
    $router->group(['path' => '/chat', 'befor' => [AdminAuth::class], 'namespace' => 'app\http\controller\admin\chat\\'], function ($router) {
        // 应用相关路由
        $router->group(['path' => '/app'], function ($router) {
            // 应用管理
            $router->get('', 'AppController@index');
            // 新增应用
            $router->map(['get', 'post'], '/add', 'AppController@add');
            // 修改应用配置
            $router->map(['get', 'post'], '/edit', 'AppController@edit');
            // 获取应用信息
            $router->get('/info', 'AppController@info');
            // 应该状态
            $router->get('/status', 'AppController@status');
            // 查看游客信息
            $router->map(['get', 'post'], '/visit', 'AppController@visit');
        });

        // 历史记录相关路由
        $router->group(['path' => '/history'], function ($router) {
            // 通信记录
            $router->get('', 'HistoryController@index');
            // 查看通信内容
            $router->get('/info', 'HistoryController@info');
        });

        // 查看应用留言
        $router->get('/stay', 'StayController@index');

        // 客服相关路由
        $router->group(['path' => '/waiter'], function ($router) {
            // 客服列表
            $router->get('', 'WaiterController@index');
            // 新增客服
            $router->map(['get', 'post'], '/add', 'WaiterController@add');
            // 修改客服信息
            $router->map(['get', 'post'], '/edit', 'WaiterController@edit');
            // 关联APP
            $router->map(['get', 'post'], '/bind', 'WaiterController@bind');
        });

        // 图文信息
        $router->group(['path' => '/news'], function ($router) {
            // 列表
            $router->get('', 'NewsController@index');
            // 新增
            $router->map(['get', 'post'], '/add', 'NewsController@add');
            // 修改
            $router->map(['get', 'post'], '/edit', 'NewsController@edit');
            // 关联APP
            $router->map(['get', 'post'], '/bind', 'NewsController@bind');
        });
    });
});
