<?php

namespace app\http\middleware\befor;

use Laf\Session;
use mon\env\Config;

/**
 * ChatLogin 前置中间件，验证客服端登录态
 *
 * Class ChatLogin
 * @copyright 2021-03-24 mon-console
 * @version 1.0.0
 */
class ChatLogin
{
	/**
	 * 回调方法
	 *
	 * @param mixed $val 依赖参数
	 * @param \FApi\App $app APP实例
	 * @return boolean 返回true执行后续操作
	 */
	public function handler($val, $app)
	{
		return true;
		$userInfo = Session::instance()->get(Config::instance()->get('chat.userInfo'));
		if (!$userInfo) {
			$loginURL = $app->url->build('/chat/waiter/login');
			return $app->url->redirect($loginURL);
		}

		return true;
	}
}
