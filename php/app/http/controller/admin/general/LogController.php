<?php

namespace app\http\controller\admin\general;

use app\model\AdminLogModel;
use app\http\controller\admin\Controller;

/**
 * 日志控制器
 */
class LogController extends Controller
{
    /**
     * 查看
     *
     * @return string
     */
    public function admin()
    {
        if ($this->request->isAjax()) {
            $option = $this->request->get();
            $result = AdminLogModel::instance()->queryList($option);
            return $this->success('ok', $result['list'], ['count' => $result['total']]);
        }

        return $this->fetch('general/log/admin');
    }
}
