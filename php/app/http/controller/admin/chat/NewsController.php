<?php

namespace app\http\controller\admin\chat;

use app\model\ChatAppModel;
use app\model\ChatNewsModel;
use app\model\ChatNewsAccessModel;
use app\http\controller\admin\Controller;

/**
 * 客服图文管理
 */
class NewsController extends Controller
{
    /**
     * 加载首页
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $data = ChatNewsModel::instance()->queryList($this->request->get());
            return $this->success('ok', $data['list'], ['count' => $data['total']]);
        }
        $apps = ChatAppModel::instance()->field(['id', 'title'])->select();
        array_unshift($apps, ['id' => '', 'title' => '']);
        return $this->fetch('chat/news/index', ['app' => $apps]);    }

    /**
     * 新增应用
     *
     * @return string
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $save = ChatNewsModel::instance()->add($this->request->post());
            if (!$save) {
                return $this->error(ChatNewsModel::instance()->getError());
            }

            return $this->success('ok');
        }
        return $this->fetch('chat/news/add');
    }

    /**
     * 编辑应用
     *
     * @return void
     */
    public function edit()
    {
        $idx = $this->request->get('idx', 0);
        $data = ChatNewsModel::instance()->getInfo(['id' => $idx]);
        if (!$data) {
            return $this->error(ChatNewsModel::instance()->getError());
        }

        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $idx;
            $edit = ChatNewsModel::instance()->edit($option);
            if (!$edit) {
                return $this->error(ChatNewsModel::instance()->getError());
            }

            return $this->success('ok');
        }

        return $this->fetch('chat/news/edit', ['info' => $data]);
    }

    /**
     * 关联角色组别用户
     *
     * @param ChatNewsModel $model
     * @param ChatAccessModel $access
     * @return string
     */
    public function bind(ChatNewsModel $model, ChatNewsAccessModel $access)
    {
        $idx = $this->request->get('idx');
        $info = $model->getInfo(['id' => $idx]);
        if (!$info) {
            return $this->error($model->getError());
        }

        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $idx;
            $save = $access->bind($option);
            if (!$save) {
                return $this->error($access->getError());
            }

            return $this->success('ok');
        }

        // 获取所有可选用户id, username
        $field = 'id as value, title';
        $apps = ChatAppModel::instance()->field($field)->select();
        // 获取所有已绑定关联的用户id
        $selects = $access->field('app_id')->where('news_id', $idx)->select();
        $selectUsers = [];
        foreach ($selects as $item) {
            $selectUsers[] = $item['app_id'];
        }
        $data = [
            'idx'       => $idx,
            'apps'     => $apps,
            'selects'   => $selectUsers
        ];
        $this->assign('data', json_encode($data, JSON_UNESCAPED_UNICODE));
        return $this->fetch('chat/waiter/bind');
    }
}
