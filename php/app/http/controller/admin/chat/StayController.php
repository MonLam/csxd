<?php

namespace app\http\controller\admin\chat;

use app\model\ChatAppModel;
use app\model\ChatStayModel;
use app\http\controller\admin\Controller;

class StayController extends Controller
{
    /**
     * 查看留言
     *
     * @param ChatStayModel $model
     * @return void
     */
    public function index(ChatStayModel $model)
    {
        if ($this->request->isAjax()) {
            $data = $model->queryList($this->request->get());
            return $this->success('ok', $data['list'], ['count' => $data['total']]);
        }

        $apps = ChatAppModel::instance()->field(['id', 'title'])->select();
        array_unshift($apps, ['id' => '', 'title' => '']);
        return $this->fetch('chat/stay/index', ['app' => $apps]);
    }
}
