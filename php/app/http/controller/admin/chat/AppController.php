<?php

namespace app\http\controller\admin\chat;

use mon\env\Config;
use GatewayClient\Gateway;
use app\model\ChatAppModel;
use app\model\ChatVisitorModel;
use app\http\controller\admin\Controller;

/**
 * 客服应用管理
 */
class AppController extends Controller
{
    /**
     * 加载首页
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $data = ChatAppModel::instance()->queryList($this->request->get());
            return $this->success('ok', $data['list'], ['count' => $data['total']]);
        }
        return $this->fetch('chat/app/index');
    }

    /**
     * 新增应用
     *
     * @return string
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $save = ChatAppModel::instance()->add($this->request->post());
            if (!$save) {
                return $this->error(ChatAppModel::instance()->getError());
            }

            return $this->success('ok');
        }
        return $this->fetch('chat/app/add');
    }

    /**
     * 编辑应用
     *
     * @param ChatAppModel $model
     * @return void
     */
    public function edit(ChatAppModel $model)
    {
        $idx = $this->request->get('idx', 0);
        $data = $model->getInfo(['id' => $idx]);
        if (!$data) {
            return $this->error($model->getError());
        }

        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $idx;
            $edit = $model->edit($option);
            if (!$edit) {
                return $this->error($model->getError());
            }

            return $this->success('ok');
        }

        return $this->fetch('chat/app/edit', ['info' => $data]);
    }

    /**
     * 应用状态
     *
     * @return string
     */
    public function status()
    {
        $app = $this->request->get('app');
        if (!$app) {
            return $this->error('query faild');
        }

        try {
            Gateway::$registerAddress = Config::instance()->get('socket.gateway.registerAddress');
            // 获取在线客服
            $onLineKF = Gateway::getClientSessionsByGroup("kf_" . $app);
            // 获取在线访客
            $onLineChat = Gateway::getClientSessionsByGroup("chat_" . $app);
            // 获取在线等待访客
            $onLineWait = Gateway::getClientSessionsByGroup("wait_" . $app);
        } catch (\Exception $e) {
            $onLineKF = [];
            $onLineChat = [];
            $onLineWait = [];
        }

        return $this->fetch('chat/app/status', [
            'app' => $app,
            'onLineKF' => $onLineKF,
            'onLineChat' => $onLineChat,
            'onLineWait' => $onLineWait
        ]);
    }

    /**
     * 应用信息
     *
     * @param ChatAppModel $model
     * @return void
     */
    public function info(ChatAppModel $model)
    {
        $idx = $this->request->get('idx', 0);
        $data = $model->getInfo(['id' => $idx]);

        return $this->fetch('chat/app/info', ['info' => $data]);
    }

    /**
     * 查看游客信息
     *
     * @param ChatVisitorModel $model
     * @return void
     */
    public function visit(ChatVisitorModel $model)
    {
        $visitID = $this->request->get('visitid');
        $info = $model->getInfo(['visitid' => $visitID]);
        if (!$info) {
            return $this->error($model->getError(), 200);
        }
        if ($this->request->isPost()) {
            $option = $this->request->post();
            // 移除空参数
            foreach ($option as $k => $v) {
                if ($v === '') {
                    unset($option[$k]);
                }
            }
            $save = $model->modify($option, $visitID);
            if (!$save) {
                return $this->error($model->getError());
            }

            return $this->success('ok');
        }

        return $this->fetch('chat/app/visit', ['info' => $info]);
    }
}
