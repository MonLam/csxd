<?php

namespace app\http\controller\admin\chat;

use app\model\ChatAppModel;
use app\model\ChatWaiterModel;
use app\model\ChatAccessModel;
use app\http\controller\admin\Controller;

class WaiterController extends Controller
{
    /**
     * 应用客服账号
     *
     * @param ChatWaiterModel $model
     * @return void
     */
    public function index(ChatWaiterModel $model)
    {
        if ($this->request->isAjax()) {
            $query = $this->request->get();
            $data = $model->queryList($query);
            return $this->success('ok', $data['list'], ['count' => $data['total']]);
        }

        $apps = ChatAppModel::instance()->field(['id', 'title'])->select();
        array_unshift($apps, ['id' => '', 'title' => '']);
        return $this->fetch('chat/waiter/index', ['app' => $apps]);
    }

    /**
     * 编辑客服账号
     *
     * @param ChatWaiterModel $model
     * @return void
     */
    public function edit(ChatWaiterModel $model)
    {
        $idx = $this->request->get('idx', 0);
        $data = $model->getInfo(['id' => $idx]);
        if (!$data) {
            return $this->error($model->getError());
        }

        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $idx;
            $edit = $model->edit($option);
            if (!$edit) {
                return $this->error($model->getError());
            }

            return $this->success('ok');
        }
        return $this->fetch('chat/waiter/edit', ['info' => $data]);
    }

    /**
     * 新增客服
     *
     * @param ChatWaiterModel $model
     * @return void
     */
    public function add(ChatWaiterModel $model)
    {
        if ($this->request->isPost()) {
            $save = $model->add($this->request->post(), $this->userInfo['id']);
            if (!$save) {
                return $this->error($model->getError());
            }

            return $this->success('ok');
        }
        return $this->fetch('chat/waiter/add');
    }

    /**
     * 关联角色组别用户
     *
     * @param ChatWaiterModel $model
     * @param ChatAccessModel $access
     * @return string
     */
    public function bind(ChatWaiterModel $model, ChatAccessModel $access)
    {
        $idx = $this->request->get('idx');
        $info = $model->getInfo(['id' => $idx]);
        if (!$info) {
            return $this->error($model->getError());
        }

        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $idx;
            $save = $access->bind($option);
            if (!$save) {
                return $this->error($access->getError());
            }

            return $this->success('ok');
        }

        // 获取所有可选用户id, username
        $field = 'id as value, title';
        $apps = ChatAppModel::instance()->field($field)->select();
        // 获取所有已绑定关联的用户id
        $selects = $access->field('app_id')->where('waiter_id', $idx)->select();
        $selectUsers = [];
        foreach ($selects as $item) {
            $selectUsers[] = $item['app_id'];
        }
        $data = [
            'idx'       => $idx,
            'apps'     => $apps,
            'selects'   => $selectUsers
        ];
        $this->assign('data', json_encode($data, JSON_UNESCAPED_UNICODE));
        return $this->fetch('chat/waiter/bind');
    }
}
