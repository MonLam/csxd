<?php

namespace app\http\controller\admin\sys;

use Laf\libs\Cache;
use app\http\controller\admin\Controller;

/**
 * 配置管理接口控制器
 */
class ClearController extends Controller
{
    /**
     * 查看页面
     *
     * @return string
     */
    public function index()
    {
        $cache = $this->auth->check('/admin/sys/clear/cache', $this->userInfo['id']);
        return $this->fetch('sys/clear', [
            'cache' => $cache
        ]);
    }

    /**
     * 清空缓存
     *
     * @return void
     */
    public function cache()
    {
        Cache::instance()->getService()->clear();
        return $this->success('清除缓存成功');
    }
}
