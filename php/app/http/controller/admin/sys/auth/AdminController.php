<?php

namespace app\http\controller\admin\sys\auth;

use mon\env\Config;
use app\model\AdminModel;
use app\http\controller\admin\Controller;

/**
 * 管理员管理
 */
class AdminController extends Controller
{
    /**
     * 管理员列表
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $option = $this->request->get();
            $result = AdminModel::instance()->queryList($option);
            return $this->success('ok', $result['list'], ['count' => $result['total']]);
        };
        $this->assign('superAdmin', json_encode(Config::instance()->get('admin.supper_admin'), JSON_UNESCAPED_UNICODE));
        return $this->fetch('sys/auth/admin/index');
    }

    /**
     * 新增管理员
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $option = $this->request->post();
            $save = AdminModel::instance()->add($option);
            if (!$save) {
                return $this->error(AdminModel::instance()->getError());
            }
            return $this->success('操作成功');
        }

        return $this->fetch('sys/auth/admin/add');
    }

    /**
     * 修改密码
     *
     * @return void
     */
    public function password(AdminModel $adminModel)
    {
        $id = $this->request->get('idx');
        if (!$id || !is_numeric($id) || !is_int($id + 0) || $id < 1) {
            return $this->error('参数错误');
        }
        if (in_array($id, Config::instance()->get('admin.supper_admin', []))) {
            return $this->error('不允许操作超级管理员用户');
        }
        $info = $adminModel->getInfo(['id' => $id]);
        if (!$info) {
            return $this->error($adminModel->getError());
        }

        // ajax，修改密码
        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $id;
            $save = $adminModel->reset_password($option);
            if (!$save) {
                return $this->error($adminModel->getError());
            }
            return $this->success('操作成功');
        }

        $this->assign('data', $info);
        return $this->fetch('sys/auth/admin/password');
    }

    /**
     * 修改信息
     *
     * @return void
     */
    public function edit(AdminModel $adminModel)
    {
        $id = $this->request->get('idx');
        if (!$id || !is_numeric($id) || !is_int($id + 0) || $id < 1) {
            return $this->error('参数错误');
        }
        if (in_array($id, Config::instance()->get('admin.supper_admin', []))) {
            return $this->error('不允许操作超级管理员用户');
        }
        $info = $adminModel->getInfo(['id' => $id]);
        if (!$info) {
            return $this->error($adminModel->getError());
        }

        // 修改信息
        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $id;
            $save = $adminModel->edit($option, $this->userInfo['id']);
            if (!$save) {
                return $this->error($adminModel->getError());
            }
            return $this->success('操作成功');
        }

        $this->assign('data', $info);
        return $this->fetch('sys/auth/admin/edit');
    }

    /**
     * 停启用用户
     *
     * @return void
     */
    public function toggle(AdminModel $adminModel)
    {
        $id = $this->request->get('idx');
        if (!$id || !is_numeric($id) || !is_int($id + 0) || $id < 1) {
            return $this->error('参数错误');
        }
        if (in_array($id, Config::instance()->get('admin.supper_admin', []))) {
            return $this->error('不允许操作超级管理员用户');
        }

        $option = $this->request->post();
        $option['idx'] = $id;
        $save = $adminModel->reset_status($option);
        if (!$save) {
            return $this->error($adminModel->getError());
        }
        return $this->success('操作成功');
    }
}
