<?php

namespace app\http\controller\admin\sys;

use app\model\MenuModel;
use app\http\controller\admin\Controller;

/**
 * 菜单控制器
 */
class MenuController extends Controller
{
    /**
     * 菜单管理
     *
     * @return string
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            return $this->success('ok', MenuModel::instance()->getTreeList('*', 'title', []));
        }
        return $this->fetch('sys/menu/index');
    }

    /**
     * 新增
     *
     * @param MenuModel $model
     * @return string
     */
    public function add(MenuModel $model)
    {
        if ($this->request->isPost()) {
            $option = $this->request->post();
            $edit = $model->add($option);
            if (!$edit) {
                return $this->error($model->getError());
            }

            return $this->success('操作成功');
        }
        $selected = $this->request->get('idx', 0);
        // 输出规则树select
        $rule = $model->getTreeList();
        array_unshift($rule, ['id' => 0, 'title' => '无', 'pid' => 0]);
        $this->assign('rule', $rule);
        $this->assign('selected', [intval($selected)]);
        return $this->fetch('sys/menu/add');
    }

    /**
     * 编辑
     *
     * @return string
     */
    public function edit(MenuModel $model)
    {
        $id = $this->request->get('idx');
        if (!$id) {
            return $this->error('参数错误');
        }
        // 查询规则
        $data = $model->getInfo(['id' => $id]);
        if (!$data) {
            return $this->error($model->getError());
        }
        // post更新操作
        if ($this->request->isPost()) {
            $option = $this->request->post();
            $option['idx'] = $id;
            $edit = $model->edit($option);
            if (!$edit) {
                return $this->error($model->getError());
            }

            return $this->success('操作成功');
        }

        $rule = $model->getTreeList();
        array_unshift($rule, ['id' => 0, 'title' => '无', 'pid' => 0]);
        $this->assign('rule', $rule);
        $this->assign('data', $data);
        return $this->fetch('sys/menu/edit');
    }
}
