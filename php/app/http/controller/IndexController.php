<?php

namespace app\http\controller;

use app\libs\View;
use Laf\Controller;

/**
 * 首页控制器
 */
class IndexController extends Controller
{
    /**
     * 首页
     *
     * @return string
     */
    public function index()
    {
        $template = new View();
        $customerJS = $this->url->build('/chat/js/919bd32e3ddf7b1af05e07215b204c17');
        return $template->fetch('index', ['customerJS' => $customerJS]);
    }
}
