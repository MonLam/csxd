<?php

namespace app\console\command;

use mon\console\Input;
use mon\console\Output;
use mon\console\Command;
use app\model\ChatStayModel;
use app\model\ChatMessageRelationModel;

/**
 * 测试指令
 */
class Test extends Command
{
    /**
     * 执行指令
     *
     * @param  Input  $in  输入实例
     * @param  Output $out 输出实例
     * @return integer exit状态码
     */
    public function execute($in, $out)
    {
        return $out->write('save rand stay data success!');
    }

    /**
     * 随机创建通信关系数据
     *
     * @return void
     */
    protected function createRelationData()
    {
        $tmp = [
            'app_name'  => '919bd32e3ddf7b1af05e07215b204c17',
        ];

        $count = 5000;
        $limit = 100;
        $result = [];
        for ($i = 0; $i < $count; $i++) {
            $item = $tmp;
            $item['waiter_id'] = rand(1, 999);
            $item['visitid']   = randString(20);
            $item['near_time'] = mt_rand(1617006271, 1619798400);
            $item['create_time'] = mt_rand(1617006271, 1619798400);
            $result[] = $item;
            if (count($result) >= $limit) {
                ChatMessageRelationModel::instance()->insertAll($result);
                $result = [];
            }
        }
    }

    /**
     * 随机创建留言数据
     *
     * @return void
     */
    protected function createStayData()
    {
        $tmp = [
            'app_name'  => '919bd32e3ddf7b1af05e07215b204c17',
            'username'  => randString(12),
            'content'   => randString(20),
            'ip'        => mt_rand(0, 255) . '.' . mt_rand(0, 255) . '.' . mt_rand(0, 255) . '.' . mt_rand(0, 255),
        ];

        $count = 10000;
        $limit = 100;
        $result = [];
        for ($i = 0; $i < $count; $i++) {
            $item = $tmp;
            $item['create_time'] = mt_rand(1617006271, 1619798400);
            $result[] = $item;
            if (count($result) >= $limit) {
                ChatStayModel::instance()->insertAll($result);
                $result = [];
            }
        }
    }
}
