<?php

namespace app\libs\exception;

use Exception;

/**
 * 数据库备份异常类
 */
class MigrateException extends Exception
{
}
