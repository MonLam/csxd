<?php

namespace app\model;

use Laf\Log;
use mon\orm\db\Raw;
use mon\orm\Model;
use mon\util\Instance;
use mon\orm\exception\DbException;

/**
 * 图文信息模型
 */
class ChatNewsModel extends Model
{
    use Instance;

    /**
     * 操作表
     *
     * @var string
     */
    protected $table = 'chat_news';

    /**
     * 新增自动写入字段
     *
     * @var array
     */
    protected $insert = ['create_time', 'update_time'];

    /**
     * 更新自动写入字段
     *
     * @var array
     */
    protected $update = ['update_time'];

    /**
     * 验证器
     *
     * @var string
     */
    protected $validate = \app\validate\Chat::class;

    /**
     * 查询信息
     *
     * @param array $where where条件
     * @param mixed $field 查询字段
     * @return mixed
     */
    public function getInfo(array $where, $field = '*')
    {
        $info = $this->where($where)->field($field)->find();
        if (!$info) {
            $this->error = '图文信息不存在';
            return false;
        }

        return $info;
    }

    /**
     * 新增
     *
     * @param array $option
     * @return boolean
     */
    public function add(array $option)
    {
        $check = $this->validate()->data($option)->scope('add_news')->check();
        if ($check !== true) {
            $this->error = $this->validate()->getError();
            return false;
        }

        $add = $this->allowField(['title', 'img', 'desc', 'link', 'sort', 'status'])->save($option);
        if (!$add) {
            $this->error = '添加图文信息失败';
            return false;
        }

        return true;
    }

    /**
     * 编辑
     *
     * @param array $option
     * @param mixed $adminID
     * @return boolean
     */
    public function edit(array $option, $adminID = null)
    {
        $check = $this->validate()->data($option)->scope('edit_news')->check();
        if ($check !== true) {
            $this->error = $this->validate()->getError();
            return false;
        }

        $this->startTrans();
        try {
            $save = $this->allowField(['title', 'img', 'desc', 'link', 'sort', 'status'])->save($option, ['id' => $option['idx']]);
            if (!$save) {
                $this->rollback();
                $this->error = '编辑图文失败';
                return false;
            }

            // 管理员ID不为null，写入管理员日志
            if (!is_null($adminID)) {
                $record = AdminLogModel::instance()->record(['uid' => $adminID, 'action' => '编辑图文信息', 'content' => '管理员编辑图文信息：' . $option['title']]);
                if (!$record) {
                    $this->rollback();
                    $this->error = '记录日志失败';
                    return false;
                }
            }

            $this->commit();
            return true;
        } catch (DbException $e) {
            $this->rollback();
            $this->error = '编辑图文信息异常';
            Log::instance()->oss(__FILE__, __LINE__, 'edit chat news exception, msg => ' . $e->getMessage(), 'error');
            return false;
        }
    }

    /**
     * 查询列表
     *
     * @param array $option 请求参数
     * @return array
     */
    public function queryList(array $option)
    {
        $limit = isset($option['limit']) ? intval($option['limit']) : 10;
        $page = isset($option['page']) && is_numeric($option['page']) ? intval($option['page']) : 1;

        // 查询
        $list = $this->scope('list', $option)->page($page, $limit)->select();
        $total = $this->scope('list', $option)->count();

        return [
            'list'      => $list,
            'total'     => $total,
            'pageSize'  => $limit,
            'page'      => $page
        ];
    }

    /**
     * 查询列表场景
     *
     * @param [type] $query
     * @param [type] $args
     * @return $this
     */
    protected function scopeList($query, $args)
    {
        // ID搜索
        if (isset($args['idx']) && $args['idx'] != '' && is_numeric($args['idx']) && is_int($args['idx'] + 0)) {
            $query->where('id', intval($args['idx']));
        }
        // 应用ID
        if (isset($args['app_id']) && $args['app_id'] != '' && is_numeric($args['app_id']) && is_int($args['app_id'] + 0)) {
            $accessTable = ChatNewsAccessModel::instance()->getTable();
            $query->where(new Raw("id IN (SELECT news_id FROM {$accessTable} WHERE app_id = {$args['app_id']})"));
        }
        // status搜索
        if (isset($args['status']) && $args['status'] != '' && is_numeric($args['status']) && is_int($args['status'] + 0)) {
            $query->where('status', intval($args['status']));
        }
        // 按用户名
        if (isset($args['title']) && is_string($args['title']) && !empty($args['title'])) {
            $query->whereLike('title', trim($args['title']) . '%');
        }
        // 创建时间搜索
        if (isset($args['start_time']) && $args['start_time'] != '' && is_numeric($args['start_time']) && is_int($args['start_time'] + 0)) {
            $query->where('create_time', '>=', intval($args['start_time']));
        }
        if (isset($args['end_time']) && $args['end_time'] != '' && is_numeric($args['end_time']) && is_int($args['end_time'] + 0)) {
            $query->where('create_time', '<=', intval($args['end_time']));
        }
        // 排序
        if (isset($args['order']) && isset($args['sort']) && in_array($args['order'], ['create_time']) && in_array($args['sort'], ['asc', 'desc'])) {
            $query->order($args['order'], $args['sort']);
        } else {
            $query->order('sort', 'desc');
        }

        return $query;
    }

    /**
     * 自动完成create_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return integer
     */
    protected function setCreateTimeAttr($val)
    {
        return time();
    }

    /**
     * 自动完成update_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return integer
     */
    protected function setUpdateTimeAttr($val)
    {
        return time();
    }
}
