<?php

namespace app\model;

use Laf\Log;
use mon\orm\Model;
use mon\util\Instance;
use mon\orm\exception\DbException;

/**
 * 客服应用模型
 *
 * Class ChatApp
 * @copyright 2021-03-25 mon-console
 * @version 1.0.0
 */
class ChatAppModel extends Model
{
    use Instance;

    /**
     * 操作表
     *
     * @var string
     */
    protected $table = 'chat_app';

    /**
     * 新增自动写入字段
     *
     * @var array
     */
    protected $insert = ['name', 'create_time', 'update_time'];

    /**
     * 更新自动写入字段
     *
     * @var array
     */
    protected $update = ['update_time'];

    /**
     * 验证器
     *
     * @var string
     */
    protected $validate = \app\validate\Chat::class;

    /**
     * 用于生成APP标识的混淆盐
     *
     * @var string
     */
    protected $salt = 'CHATAPP';

    /**
     * 工具栏列表
     *
     * @var array
     */
    public $tools = [
        'emoji' => '表情包', 'link' => '链接', 'file' => '文件'
    ];

    /**
     * JS渲染列表
     *
     * @var array
     */
    public $display = [
        'home' => '主页', 'kefu' => '联系客服', 'moble' => '联系电话', 'qq' => '客服QQ', 'wechat' => '客服微信', 'qrcode' => '二维码', 'top' => '回顶'
    ];

    /**
     * 查询信息
     *
     * @param array $where where条件
     * @param mixed $field 查询字段
     * @return mixed
     */
    public function getInfo(array $where, $field = '*')
    {
        $info = $this->where($where)->field($field)->find();
        if (!$info) {
            $this->error = '客服应用信息不存在';
            return false;
        }

        return $info;
    }

    /**
     * 新增
     *
     * @param array $option
     * @param mixed $adminID 管理员ID，不为null则写入管理员日志
     * @return boolean
     */
    public function add(array $option, $adminID = null)
    {
        $check = $this->validate()->data($option)->scope('add_app')->check();
        if ($check !== true) {
            $this->error = $this->validate()->getError();
            return false;
        }

        // 判断名称是否已存在
        $userInfo = $this->where(['title' => $option['title']])->find();
        if ($userInfo) {
            $this->error = '名称已存在';
            return false;
        }
        // 整理数据
        $option['tools'] = implode(',', $option['tools']);
        $option['display'] = implode(',', (isset($option['display']) ? $option['display'] : []));

        $this->startTrans();
        try {
            $save = $this->allowField(['title', 'seatnum', 'receptnum', 'logo', 'home', 'qq', 'moble', 'wechat', 'qrcode', 'wellcome', 'tools', 'display', 'status'])->save($option);
            if (!$save) {
                $this->rollback();
                $this->error = '新增应用失败';
                return false;
            }

            // 管理员ID不为null，写入管理员日志
            if (!is_null($adminID)) {
                $record = AdminLogModel::instance()->record(['uid' => $adminID, 'action' => '新增客服应用', 'content' => '管理员新增客服应用：' . $option['title']]);
                if (!$record) {
                    $this->rollback();
                    $this->error = '记录日志失败';
                    return false;
                }
            }

            $this->commit();
            return true;
        } catch (DbException $e) {
            $this->rollback();
            $this->error = '新增应用异常';
            Log::instance()->oss(__FILE__, __LINE__, 'add chat app exception, msg => ' . $e->getMessage(), 'error');
            return false;
        }
    }

    /**
     * 修改
     *
     * @param array $option
     * @param mixed $adminID 管理员ID，不为null则写入管理员日志
     * @return boolean
     */
    public function edit(array $option, $adminID = null)
    {
        $check = $this->validate()->data($option)->scope('edit_app')->check();
        if ($check !== true) {
            $this->error = $this->validate()->getError();
            return false;
        }

        // 判断名称是否已存在
        $userInfo = $this->where(['title' => $option['title']])->where('id', '<>', $option['idx'])->find();
        if ($userInfo) {
            $this->error = '名称已存在';
            return false;
        }
        // 整理数据
        $option['tools'] = implode(',', $option['tools']);
        $option['display'] = implode(',', (isset($option['display']) ? $option['display'] : []));

        $this->startTrans();
        try {
            $save = $this->allowField(['title', 'seatnum', 'receptnum', 'logo', 'qq', 'moble', 'home', 'wechat', 'qrcode', 'wellcome', 'tools', 'display', 'status'])->save($option, ['id' => $option['idx']]);
            if (!$save) {
                $this->rollback();
                $this->error = '编辑应用失败';
                return false;
            }

            // 管理员ID不为null，写入管理员日志
            if (!is_null($adminID)) {
                $record = AdminLogModel::instance()->record(['uid' => $adminID, 'action' => '编辑客服应用', 'content' => '管理员编辑客服应用：' . $option['title']]);
                if (!$record) {
                    $this->rollback();
                    $this->error = '记录日志失败';
                    return false;
                }
            }

            $this->commit();
            return true;
        } catch (DbException $e) {
            $this->rollback();
            $this->error = '编辑应用异常';
            Log::instance()->oss(__FILE__, __LINE__, 'edit chat app exception, msg => ' . $e->getMessage(), 'error');
            return false;
        }
    }

    /**
     * 查询列表
     *
     * @param array $option 请求参数
     * @return array
     */
    public function queryList(array $option)
    {
        $limit = isset($option['limit']) ? intval($option['limit']) : 10;
        $page = isset($option['page']) && is_numeric($option['page']) ? intval($option['page']) : 1;

        // 查询
        $list = $this->scope('list', $option)->page($page, $limit)->select();
        $total = $this->scope('list', $option)->count();

        return [
            'list'      => $list,
            'total'     => $total,
            'pageSize'  => $limit,
            'page'      => $page
        ];
    }

    /**
     * 查询列表场景
     *
     * @param [type] $query
     * @param [type] $args
     * @return $this
     */
    protected function scopeList($query, $args)
    {
        // ID搜索
        if (isset($args['idx']) && $args['idx'] != '' && is_numeric($args['idx']) && is_int($args['idx'] + 0)) {
            $query->where('id', intval($args['idx']));
        }
        // status搜索
        if (isset($args['status']) && $args['status'] != '' && is_numeric($args['status']) && is_int($args['status'] + 0)) {
            $query->where('status', intval($args['status']));
        }
        // 按用户名
        if (isset($args['title']) && is_string($args['title']) && !empty($args['title'])) {
            $query->whereLike('title', trim($args['title']) . '%');
        }
        // 创建时间搜索
        if (isset($args['start_time']) && $args['start_time'] != '' && is_numeric($args['start_time']) && is_int($args['start_time'] + 0)) {
            $query->where('create_time', '>=', intval($args['start_time']));
        }
        if (isset($args['end_time']) && $args['end_time'] != '' && is_numeric($args['end_time']) && is_int($args['end_time'] + 0)) {
            $query->where('create_time', '<=', intval($args['end_time']));
        }
        // 排序
        if (isset($args['order']) && isset($args['sort']) && in_array($args['order'], ['create_time']) && in_array($args['sort'], ['asc', 'desc'])) {
            $query->order($args['order'], $args['sort']);
        }

        return $query;
    }

    /**
     * 自动完成create_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setCreateTimeAttr($val)
    {
        return time();
    }

    /**
     * 自动完成update_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setUpdateTimeAttr($val)
    {
        return time();
    }

    /**
     * 生成APP标识
     *
     * @param null $val
     * @param array $data
     * @return string
     */
    protected function setNameAttr($val, $data)
    {
        return md5($data['title'] . time() . $this->salt);
    }
}
