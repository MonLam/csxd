<?php

namespace app\model;

use Laf\Log;
use mon\orm\Model;
use mon\util\Instance;
use mon\orm\exception\DbException;

/**
 * 客服绑定App表模型
 *
 * Class ChatAccessModel
 * @copyright 2021-04-18 mon-console
 * @version 1.0.0
 */
class ChatNewsAccessModel extends Model
{
    use Instance;

    /**
     * 操作表
     *
     * @var string
     */
    protected $table = 'chat_news_access';

    /**
     * 新增自动写入字段
     *
     * @var array
     */
    protected $insert = ['create_time', 'update_time'];

    /**
     * 更新自动写入字段
     *
     * @var array
     */
    protected $update = ['update_time'];

    /**
     * 验证器
     *
     * @var string
     */
    protected $validate = \app\validate\Chat::class;

    /**
     * 获取图文信息绑定的APP列表
     *
     * @param string $app_name
     * @return array
     */
    public function getBindApp($app_name)
    {
        $result = $this->alias('a')->join(ChatNewsModel::instance()->getTable() . ' b', 'a.news_id = b.id')
            ->join(ChatAppModel::instance()->getTable() . ' c', 'c.id = a.app_id')
            ->where('c.name', $app_name)->where('b.status', 1)->where('c.status', 1)->order('b.sort', 'desc')
            ->field('a.news_id, a.app_id, b.title, b.img, b.desc, b.link')->select();
        return $result;
    }

    /**
     * 绑定APP
     *
     * @param array $data
     * @return boolean
     */
    public function bind($data)
    {
        $check = $this->validate()->data($data)->scope('bind')->check();
        if ($check !== true) {
            $this->error = $this->validate()->getError();
            return false;
        }
        $this->startTrans();;
        try {
            // 移除旧绑定数据
            $del = $this->where('news_id', $data['idx'])->delete();
            if ($del === false) {
                $this->rollback();
                $this->error = '更新关联数据失败';
                return false;
            }
            // 重新绑定
            $apps = explode(',', $data['apps']);
            $saveData = [];
            $now = time();
            foreach ($apps as $app_id) {
                $saveData[] = [
                    'news_id' => $data['idx'],
                    'app_id' => $app_id,
                    'update_time' => $now,
                    'create_time' => $now,
                ];
            }

            $save = $this->insertAll($saveData);
            if (!$save) {
                $this->rollback();
                $this->error = '保存关联数据失败';
                return false;
            }

            $this->commit();
            return true;
        } catch (DbException $e) {
            $this->rollback();
            $this->error = '关联APP异常！';
            Log::instance()->error($this->error . $e->getMessage());
            return false;
        }
    }

    /**
     * 自动完成create_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setCreateTimeAttr($val)
    {
        return time();
    }

    /**
     * 自动完成update_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setUpdateTimeAttr($val)
    {
        return time();
    }
}
