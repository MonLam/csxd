<?php

namespace app\model;

use mon\orm\Model;
use mon\util\Instance;

/**
 * 通信信息记录模型
 *
 * Class ChatMessage
 * @copyright 2021-03-25 mon-console
 * @version 1.0.0
 */
class ChatMessageModel extends Model
{
    use Instance;

    /**
     * 操作表
     *
     * @var string
     */
    protected $table = 'chat_message';

    /**
     * 新增自动写入字段
     *
     * @var array
     */
    protected $insert = ['create_time', 'ip'];

    /**
     * 查询信息
     *
     * @param array $where where条件
     * @param mixed $field 查询字段
     * @return mixed
     */
    public function getInfo(array $where, $field = '*')
    {
        $info = $this->where($where)->field($field)->find();
        if (!$info) {
            $this->error = '记录信息不存在';
            return false;
        }

        return $info;
    }

    /**
     * 记录通信信息
     *
     * @param string $chat_id 通信信息关系表ID
     * @param string $messgae 通信内容
     * @return boolean
     */
    public function record($chat_id, $messgae)
    {
        $save = $this->save([
            'chat_id'   => $chat_id,
            'content'   => $messgae
        ], null, true);

        return $save;
    }

    /**
     * 获取通话记录
     *
     * @param integer $chat_id  通话编号
     * @param array $field  查询的字段
     * @return array
     */
    public function getMessage($chat_id, $field = [])
    {
        return $this->where('chat_id', $chat_id)->order('id')->field($field)->select();
    }

    /**
     * 自动完成create_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setCreateTimeAttr($val)
    {
        return time();
    }

    /**
     * 设置IP
     *
     * @return string
     */
    protected function setIpAttr()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}
