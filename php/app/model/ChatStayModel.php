<?php

namespace app\model;

use mon\orm\Model;
use mon\util\Instance;

/**
 * 游客留言模型
 *
 * Class ChatStay
 * @copyright 2021-03-25 mon-console
 * @version 1.0.0
 */
class ChatStayModel extends Model
{
    use Instance;

    /**
     * 操作表
     *
     * @var string
     */
    protected $table = 'chat_stay';

    /**
     * 新增自动写入字段
     *
     * @var array
     */
    protected $insert = ['create_time', 'ip'];

    /**
     * 验证器
     *
     * @var string
     */
    protected $validate = \app\validate\Chat::class;

    /**
     * 查询信息
     *
     * @param array $where where条件
     * @param mixed $field 查询字段
     * @return mixed
     */
    public function getInfo(array $where, $field = '*')
    {
        $info = $this->where($where)->field($field)->find();
        if (!$info) {
            $this->error = '留言信息不存在';
            return false;
        }

        return $info;
    }

    /**
     * 记录留言
     *
     * @param array $option
     * @return boolean
     */
    public function record(array $option)
    {
        if (!isset($option['username']) || empty($option['username'])) {
            $this->error = '请输入姓名';
            return false;
        }
        $check = $this->validate()->scope('stay')->data($option)->check();
        if ($check !== true) {
            $this->error = $this->validate()->getError();
            return false;
        }

        $save = $this->allowField(['username', 'email', 'moble', 'content', 'app_name'])->save($option);
        if (!$save) {
            $this->error = '保存留言信息失败';
            return false;
        }

        return true;
    }

    /**
     * 查询列表
     *
     * @param array $option 请求参数
     * @return array
     */
    public function queryList(array $option)
    {
        $limit = isset($option['limit']) ? intval($option['limit']) : 10;
        $page = isset($option['page']) && is_numeric($option['page']) ? intval($option['page']) : 1;

        // 查询
        $list = $this->scope('list', $option)->page($page, $limit)->select();
        $total = $this->scope('list', $option)->count();

        return [
            'list'      => $list,
            'total'     => $total,
            'pageSize'  => $limit,
            'page'      => $page
        ];
    }

    /**
     * 查询列表场景
     *
     * @param [type] $query
     * @param [type] $args
     * @return $this
     */
    protected function scopeList($query, $args)
    {
        $query->alias('a')->join(ChatAppModel::instance()->getTable() . ' b', 'a.app_name = b.name')
            ->field('a.*, b.title')->order('a.id', 'desc');
        // ID搜索
        if (isset($args['idx']) && $args['idx'] != '' && is_numeric($args['idx']) && is_int($args['idx'] + 0)) {
            $query->where('a.id', intval($args['idx']));
        }
        // 按app_name
        if (isset($args['name']) && is_string($args['name']) && !empty($args['name'])) {
            $query->where('a.app_name', trim($args['name']));
        }
        // 按app_id
        if (isset($args['app_id']) && $args['app_id'] != '' && is_numeric($args['app_id']) && is_int($args['app_id'] + 0)) {
            $query->where('b.id', intval($args['app_id']));
        }
        if (isset($args['app_title']) && is_string($args['app_title']) && !empty($args['app_title'])) {
            $query->where('b.title', trim($args['app_title']));
        }
        // 按app名称
        if (isset($args['title']) && is_string($args['title']) && !empty($args['title'])) {
            $query->where('b.title', trim($args['title']));
        }
        // 创建时间搜索
        if (isset($args['start_time']) && $args['start_time'] != '' && is_numeric($args['start_time']) && is_int($args['start_time'] + 0)) {
            $query->where('a.create_time', '>=', intval($args['start_time']));
        }
        if (isset($args['end_time']) && $args['end_time'] != '' && is_numeric($args['end_time']) && is_int($args['end_time'] + 0)) {
            $query->where('a.create_time', '<=', intval($args['end_time']));
        }

        return $query;
    }

    /**
     * 自动完成create_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setCreateTimeAttr($val)
    {
        return time();
    }

    /**
     * 自动完成ip字段
     *
     * @param mixed $val
     * @return string
     */
    protected function setIpAttr($val)
    {
        return ip();
    }
}
