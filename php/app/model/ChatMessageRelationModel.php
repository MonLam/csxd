<?php

namespace app\model;

use mon\orm\Model;
use mon\util\Instance;

/**
 * 通信关系模型
 *
 * Class ChatMessageRelation
 * @copyright 2021-03-25 mon-console
 * @version 1.0.0
 */
class ChatMessageRelationModel extends Model
{
    use Instance;

    /**
     * 操作表
     *
     * @var string
     */
    protected $table = 'chat_message_relation';

    /**
     * 新增自动写入字段
     *
     * @var array
     */
    protected $insert = ['create_time', 'near_time'];

    /**
     * 补充字段
     *
     * @var array
     */
    protected $append = ['code'];

    /**
     * 查询信息
     *
     * @param array $where where条件
     * @param mixed $field 查询字段
     * @return mixed
     */
    public function getInfo(array $where, $field = '*')
    {
        $info = $this->where($where)->field($field)->find();
        if (!$info) {
            $this->error = '通信关系不存在';
            return false;
        }

        return $info;
    }

    /**
     * 记录通话关系
     *
     * @param integer $app_id   应用标志ID
     * @param integer $uid      应用客服ID
     * @param string $visitid   访客ID
     * @return integer 通话关系ID
     */
    public function record($app_id, $uid, $visitid)
    {
        $info = $this->getInfo(['app_name' => $app_id, 'waiter_id' => $uid, 'visitid' => $visitid]);
        if (!$info) {
            $save = $this->save(['app_name' => $app_id, 'waiter_id' => $uid, 'visitid' => $visitid], null, true);
            if (!$save) {
                return false;
            }

            return $save;
        }

        return $info['id'];
    }

    /**
     * 获取对话列表
     *
     * @param array $option 请求参数
     * @return array
     */
    public function getList(array $option)
    {
        $limit = isset($option['limit']) ? intval($option['limit']) : 10;
        $page = isset($option['page']) && is_numeric($option['page']) ? intval($option['page']) : 1;
        // 查询
        $list = $this->scope('list', $option)->page($page, $limit)->all();
        $total = $this->scope('list', $option)->count();

        return [
            'list'      => $list ? $list->toArray() : [],
            'total'     => $total,
            'pageSize'  => $limit,
            'page'      => $page
        ];
    }

    /**
     * 场景查询列表
     *
     * @param [type] $query
     * @param [type] $args
     * @return void
     */
    protected function scopeList($query, $args)
    {
        $query->alias('a')->join(ChatVisitorModel::instance()->getTable() . ' b', 'a.visitid = b.visitid')
            ->join(ChatAppModel::instance()->getTable() . ' c', 'a.app_name = c.name')
            ->join(ChatWaiterModel::instance()->getTable() . ' d', 'a.waiter_id = d.id')
            ->field('a.id, a.app_name, a.waiter_id, a.visitid, a.near_time, a.create_time, a.remark, b.id AS visitor_id, b.ip, c.title, d.account')
            ->order('a.near_time', 'desc');
        // 按APP名
        if (isset($args['app']) && is_string($args['app']) && !empty($args['app'])) {
            $query->where('a.app_name', trim($args['app']));
        }
        if (isset($args['app_title']) && is_string($args['app_title']) && !empty($args['app_title'])) {
            $query->where('c.title', trim($args['app_title']));
        }
        // app_id搜索
        if (isset($args['app_id']) && $args['app_id'] != '' && is_numeric($args['app_id']) && is_int($args['app_id'] + 0)) {
            $query->where('c.id', intval($args['app_id']));
        }
        // waiter_id搜索
        if (isset($args['waiter_id']) && $args['waiter_id'] != '' && is_numeric($args['waiter_id']) && is_int($args['waiter_id'] + 0)) {
            $query->where('a.waiter_id', intval($args['waiter_id']));
        }
        // 访客ID搜索
        if (isset($args['visitor_id']) && $args['visitor_id'] != '' && is_numeric($args['visitor_id']) && is_int($args['visitor_id'] + 0)) {
            $query->where('b.id', intval($args['visitor_id']));
        }
        // 最近会话日期搜索
        if (isset($args['near']) && $args['near'] != '' && check('date', $args['near'])) {
            $startTime = strtotime($args['near']);
            $endTime = strtotime(date('Y-m-d 23:59:59', $startTime));
            $query->where('a.near_time', '>=', $startTime);
            $query->where('a.near_time', '<=', $endTime);
        }
        // 最后会话时间区间搜索
        if (isset($args['start_near']) && $args['start_near'] != '' && is_numeric($args['start_near']) && is_int($args['start_near'] + 0)) {
            $query->where('a.near_time', '>=', intval($args['start_near']));
        }
        if (isset($args['end_near']) && $args['end_near'] != '' && is_numeric($args['end_near']) && is_int($args['end_near'] + 0)) {
            $query->where('a.near_time', '<=', intval($args['end_near']));
        }
        // 创建时间搜索
        if (isset($args['start_time']) && $args['start_time'] != '' && is_numeric($args['start_time']) && is_int($args['start_time'] + 0)) {
            $query->where('a.create_time', '>=', intval($args['start_time']));
        }
        if (isset($args['end_time']) && $args['end_time'] != '' && is_numeric($args['end_time']) && is_int($args['end_time'] + 0)) {
            $query->where('a.create_time', '<=', intval($args['end_time']));
        }
        // 按用户名搜索
        if (isset($args['username']) && is_string($args['username']) && !empty($args['username'])) {
            $query->whereLike('b.username', trim($args['username']) . '%');
        }
        // 按visitid搜索
        if (isset($args['visitid']) && is_string($args['visitid']) && !empty($args['visitid'])) {
            $query->where('a.visitid', 'like', trim($args['visitid']) . '%');
        }
        if (isset($args['account']) && is_string($args['account']) && !empty($args['account'])) {
            $query->where('d.account', trim($args['account']));
        }

        return $query;
    }

    /**
     * 自动完成create_time字段
     * 
     * @param mixed $val 默认值
     * @param array  $row 列值
     * @return string
     */
    protected function setCreateTimeAttr($val)
    {
        return time();
    }

    /**
     * 自动完成near_time字段
     * 
     * @param [type] $val 默认值
     * @param array  $row 列值
     * @return integer
     */
    protected function setNearTimeAttr($val)
    {
        return time();
    }

    /**
     * 生成code字段
     *
     * @return string
     */
    public function getCodeAttr($val, $data)
    {
        return id2code($data['waiter_id']);
    }
}
