<?php

namespace app\validate;

use mon\util\Instance;
use mon\util\Validate;

/**
 * Menu 验证器
 *
 * Class Menu
 * @copyright 2021-03-24 mon-console
 * @version 1.0.0
 */
class Menu extends Validate
{
    use Instance;

    /**
     * 验证规则
     *
     * @var array
     */
    public $rule = [
        'idx'           => ['required', 'int', 'min:1'],
        'icon'          => ['required'],
        'name'          => ['required'],
        'pid'           => ['required', 'num'],
        'status'        => ['required', 'in:1,2'],
        'title'         => ['required'],
        'sort'          => ['required', 'num', 'min:0', 'max:1000'],
        'remark'        => ['str'],
    ];

    /**
     * 错误提示信息
     *
     * @var array
     */
    public $message = [
        'idx'           => '参数异常',
        'icon'          => '请选择对应icon',
        'name'          => '请输入正确的规则',
        'pid'           => '请选择对应的父级',
        'status'        => '请选择正确的状态',
        'title'         => '请输入对应的标题',
        'sort'          => '请输入正确的权重值(0 <= x < 1000)',
        'remark'        => '请输入合法的备注',
    ];

    /**
     * 验证场景
     *
     * @var array
     */
    public $scope = [
        'add'      => ['icon', 'pid', 'status', 'title', 'sort', 'name', 'remark'],
        'edit'     => ['icon', 'pid', 'status', 'title', 'sort', 'name', 'remark', 'idx'],
    ];
}
