<?php

namespace app\validate;

use mon\util\Instance;
use mon\util\Validate;

/**
 * Chat 验证器
 *
 * Class Chat
 * @copyright 2021-03-24 mon-console
 * @version 1.0.0
 */
class Chat extends Validate
{
    use Instance;

    /**
     * 验证规则
     *
     * @var array
     */
    public $rule = [
        'idx'       => ['required', 'int', 'min:1'],
        'app_id'    => ['required', 'int', 'min:1'],
        'user'      => ['required', 'str', 'maxLength:12', 'minLength:3'],
        'password'  => ['required', 'str', 'maxLength:16', 'minLength:6'],
        'status'    => ['required', 'in:1,2'],
        'nickname'  => ['required', 'str', 'maxLength:24'],
        'username'  => ['str', 'maxLength:48'],
        'email'     => ['email'],
        'moble'     => ['moble'],
        'qq'        => ['int', 'min:10000'],
        'address'   => ['str'],
        'remark'    => ['str'],
        'content'   => ['required', 'str'],
        'title'     => ['required', 'str'],
        'seatnum'   => ['required', 'int', 'min:1'],
        'receptnum' => ['required', 'int', 'min:1'],
        'logo'      => ['str'],
        'home'      => ['url'],
        'wechat'    => ['str'],
        'qrcode'    => ['str'],
        'wellcome'  => ['str'],
        'tools'     => ['required', 'arr'],
        'display'   => ['arr'],
        'apps'      => ['required', 'apps'],
        'img'       => ['required', 'str'],
        'desc'      => ['str'],
        'link'      => ['required', 'str'],
        'sort'      => ['required', 'int', 'min:0'],
    ];

    /**
     * 错误提示信息
     *
     * @var array
     */
    public $message = [
        'idx'           => '参数异常',
        'user'          => [
            'required'  => '用户名必须',
            'maxLength' => '用户名长度不能超过12',
            'minLength' => '用户名长度不能小于3',
        ],
        'password'      => [
            'required'  => '密码必须',
            'maxLength' => '密码长度不能超过16',
            'minLength' => '密码长度不能小于6',
        ],
        'status'        => '请选择正确的状态',
        'username'      => '请输入合法的姓名',
        'email'         => '请输入合法的邮箱地址',
        'moble'         => '请输入合法的手机号',
        'qq'            => '请输入合法的QQ号',
        'address'       => '请输入合法的地址',
        'remark'        => '请输入合法的备注信息',
        'content'       => '请输入留言信息',
        'title'         => '请输入合法的名称',
        'seatnum'       => '请输入客服数',
        'receptnum'     => '请输入客服人员最大接待数',
        'logo'          => '请上传Logo图片',
        'home'          => '请输入合法的主站地址',
        'wechat'        => '请上传微信二维码图片',
        'qrcode'        => '请上传二维码图片',
        'wellcome'      => '请输入欢迎词',
        'tools'         => '请选择工具栏权限',
        'display'       => '请选择生成JS渲染内容',
        'apps'          => '遍历APP参数异常',
        'img'           => '请上传图片',
        'desc'          => '请输入合法的描述信息',
        'link'          => '请输入合法的跳转地址',
        'sort'          => '请输入合法的排序信息'
    ];

    /**
     * 验证场景
     *
     * @var array
     */
    public $scope = [
        'login'         => ['user', 'password'],
        'reset_pwd'     => ['idx', 'password'],
        'reset_status'  => ['idx', 'status'],
        'password'      => ['password'],
        'clent_info'    => ['username', 'email', 'moble', 'qq', 'address', 'remark'],
        'stay'          => ['username', 'email', 'moble', 'content'],
        'add_app'       => ['title', 'seatnum', 'receptnum', 'logo', 'home', 'wechat', 'qrcode', 'wellcome', 'tools', 'display', 'status'],
        'edit_app'      => ['idx', 'title', 'seatnum', 'receptnum', 'logo', 'home', 'wechat', 'qrcode', 'wellcome', 'tools', 'display', 'status'],
        'add_waiter'    => ['user', 'password', 'nickname', 'status'],
        'edit_waiter'   => ['idx', 'user', 'nickname', 'status'],
        'bind'          => ['idx', 'apps'],
        'add_news'      => ['title', 'img', 'desc', 'link', 'sort'],
        'edit_news'     => ['idx', 'title', 'img', 'desc', 'link', 'sort'],
    ];

    /**
     * 验证规则路由
     *
     * @param  string $value ","号分割的数字字符串
     * @return boolean
     */
    public function apps($value)
    {
        $rules = explode(",", $value);
        foreach ($rules as $v) {
            if (!$this->int($v)) {
                return false;
            }
        }

        return true;
    }
}
