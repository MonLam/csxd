; (function (window) {
    var chatMessage = function (path) {
        this.setPath(path)
    }
    chatMessage.prototype = {
        constructor: this,
        // 解析内容
        parse: function (type, data, date, send) {
            switch (type) {
                case 'tips':
                    return this._parseTips(data.content);
                case 'text':
                    return this._parseText(data.content, date, send);
                case 'img':
                    return this._parseIMG(data.content, date);
                case 'news':
                    return this._parseCard(data.content, date);
                default:
                    return '';
            }
        },
        // 设置emoji路径
        setPath: function(path) {
            this.emojiPath = path || ''
            return this
        },
        // 发送图片信息
        _parseIMG: function (url, date) {
            return '<div class="message-tips">' + date + '</div><img src="' + url + '" class="file-img" alt="图片加载失败" />';
        },
        // 写入tips信息
        _parseTips(str) {
            return '<div class="tips">' + str + "</div>";
        },
        // 图文卡片信息
        _parseCard(data, date) {
            return '<div class="message-tips">' + date + '</div>\
                        <a href="'+ data.link + '" target="_blank">\
                        <div class="message-card" title="'+ data.title + '">\
                            <div class="message-card-img"><img src="' + data.img + '" class="card-img" alt="图片加载失败" /></div>\
                            <div class="message-card-msg">\
                                <div class="message-card-msg-title">'+ data.title + '</div>\
                                <div class="message-card-msg-desc">'+ data.desc + '</div>\
                            </div>\
                        </a>\
                    </div>';
        },
        // 写入文字信息
        _parseText(str, date, send) {
            // 替换a链接
            str = this._parseLink(str);
            // 替换所有的换行符
            str = str.replace(/\r\n/g, " <br> ");
            str = str.replace(/\n/g, " <br> ");
            // 替换所有的空格（中文空格、英文空格都会被替换）
            str = str.replace(/\s/g, "&nbsp;");
            // 替换a链接转义后特殊的空格符号，解决多个链接正则混淆匹配
            str = str.replace(/&my_nbsp;/ig, ' ');
            // 转换表情图片
            str = this._parseEmoji(str);

            var postion = send == 'ask' ? 'right' : 'left';
            return '<div class="' + send + '">\
                        <div class="message-tips">'+ date + '</div>\
                        <div class="message">\
                            <span class="arrow-' + postion + '-out"></span>\
                            <span class="arrow-' + postion + '-in"></span>\
                            ' + str + '\
                        </div>\
                    </div>';
        },
        // 解析表情包
        _parseEmoji: function (str) {
            var reg = /\[mon:([a-zA-Z0-9_\u4e00-\u9fa5]+):mon\]/g;
            var _self = this;

            return str.replace(reg, function (str, target) {
                var tempArr = target.split("_");
                var title = tempArr.shift();
                var type = tempArr.pop();
                var name = tempArr.join("_");
                var path = _self.emojiPath;
                var url = name + "." + type;

                return '<img src="' + path + url + '" alt="' + title + '" />';
            });
        },
        // 解析生成A链接
        _parseLink(str) {
            var reg = /\[monlink:([a-zA-z]+:\/\/[^\s]*=>?[^\s]*):monlink\]/g;
            return str.replace(reg, function (str, target) {
                var tempArr = target.split("=>");
                var url = tempArr.shift();
                var title = tempArr.pop();
                title = title == "" ? url : title;
                // &my_nbsp;为特殊的空格表示位，需要再重新转换多一次空格，该设置解决多个链接的情况下，正则匹配为1个链接
                return (
                    '<a&my_nbsp;href="' + url + '"&my_nbsp;target="_blank">' + title + "</a>"
                );
            });
        },
    }

    if (typeof module != "undefined" && module.exports) {
        module.exports = new chatMessage;
    } else if (typeof define == "function" && define.amd) {
        define(function () { return new chatMessage; });
    } else {
        !('chatMessage' in window) && (window.chatMessage = new chatMessage);
    }

})(window)