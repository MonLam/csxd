/**
 * 数据表单
 */
define(['jquery', 'layui', 'xmSelect'], function ($, Layui, xmSelect) {
    var Form = {
        // layui
        Layui: Layui,
        // 获取实例, 执行回调
        callback: function (callback) {
            Layui.use('form', function (form) {
                callback.call(this, form)
            })
        },
        val: function (filter) {
            var value
            Layui.use('form', function (form) {
                value = form.val(filter)
            })
            return value
        },
        // 加载表单模块，渲染页面
        render: function (type, filter) {
            Layui.use('form', function (form) {
                // 初始化样式
                Form.init();
                // 注册验证器
                form.verify(Form.verify)
                // 样式渲染
                form.render(type || null, filter || null);
            })
        },
        // 表单提交
        submit: function (filter, success, error, option) {
            Layui.use('form', function (form) {
                form.on('submit(' + filter + ')', function (data) {
                    // 按钮防刷
                    $(data.elem).addClass("disabled")
                    $(data.elem).attr("disabled", '')

                    // 表单数据
                    var formElement = data.form
                    var field = data.field
                    // 请求方式
                    var type = formElement.method || 'GET'
                    var url = formElement.action || location.href
                    // 合并ajax请求参数，option如果为函数则回调函数，获取参数实现提交时操作
                    var extendData = (typeof option == 'object') ? option : (typeof option == 'function') ? option.call(this, data) : {}
                    var dataOption = $.extend(field, extendData || {});
                    //调用Ajax请求方法
                    Mon.api.ajax({
                        type: type,
                        url: url,
                        data: dataOption,
                        dataType: 'json',
                        beforeSend: function (xhr, setting) {
                            // 请求携带防跨域token
                            var token = sessionStorage.getItem('x-form-token') || ''
                            if (token) {
                                xhr.setRequestHeader('x-form-token', token)
                            }
                        },
                        complete: function (xhr) {
                            $(data.elem).removeClass("disabled")
                            $(data.elem).removeAttr('disabled')
                            // 获取响应的防跨域token, 存储到sessionStorage中
                            var token = xhr.getResponseHeader('x-form-token');
                            if (token) {
                                sessionStorage.setItem('x-form-token', token)
                            }
                        }
                    }, function (res, ret) {
                        if (res && typeof res === 'object') {
                            //刷新客户端防跨域token
                            if (typeof res.token !== 'undefined') {
                                sessionStorage.setItem('x-form-token', res.token)
                            }
                            // 存在回调，执行回调
                            if (typeof success === 'function' && success.call(this, form, data, res, ret) === false) {
                                return false;
                            }

                            //提示及关闭当前窗口
                            var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : '操作成功';
                            parent.Toastr.success(msg);
                            // 存在刷新按钮，且已绑定事件，则刷新上级table
                            parent.$(".btn-refresh").trigger("click");
                            var index = parent.Layer.getFrameIndex(window.name);
                            parent.Layer.close(index);
                            return false;
                        }
                    }, function (res, ret) {
                        if (typeof error === 'function' && error.call(this, form, data, res, ret) === false) {
                            return false;
                        }
                    });

                    return false;
                });
            })
        },
        // 内置的验证条件
        verify: {
            // 用户名
            username: function (value, item) {
                if (!new RegExp('^[a-zA-Z0-9_-]{3,16}$').test(value)) {
                    return '用户名必须为合法的3-16位';
                }
            },
            // 密码
            password: function (value, item) {
                if (!new RegExp('^^[a-zA-Z0-9_\.]{6,16}$').test(value)) {
                    return '密码必须6-16位';
                }
            },
            // 验证输入一致
            consistent: function (value) {
                if (value !== $('#consistent_value').val()) {
                    return '输入内容不一致';
                }
            },
        },
        // 初始化样式
        init: function () {
            // 日期区间
            if ($('.date-range').length > 0) {
                Layui.use('laydate', function (LayDate) {
                    $('.date-range').each(function (index, e) {
                        LayDate.render({
                            elem: e,
                            // type: 'datetime',
                            range: true,
                            // format: 'yyyy-MM-dd HH:mm:ss',
                            value: $(e).val(),
                        })
                    })
                })
            }
            // 日期
            if ($('.date').length > 0) {
                Layui.use('laydate', function (LayDate) {
                    $('.date').each(function (index, e) {
                        LayDate.render({
                            elem: e,
                            // type: 'datetime',
                            // format: 'yyyy-MM-dd HH:mm:ss',
                            value: $(e).val(),
                        })
                    })
                })
            }
            // 多选
            if ($('.xm-select[lay-ignore]').length > 0) {
                $('.xm-select[lay-ignore]').each(function (index, e) {
                    var name = $(e).attr('name');
                    var toolbar = $(e).attr('toolbar') ? ($(e).attr('toolbar') == '1') : true
                    if (name) {
                        var data = []
                        $(e).find('option').each(function (i, el) {
                            var selected = Boolean($(el).attr('selected'))
                            var val = $(el).attr('value')
                            var item = {
                                name: $(el).text(),
                                value: val,
                                selected: selected,
                                disabled: Boolean($(el).attr('disabled')),
                            }
                            data.push(item)
                        })


                        // 创建新元素，删除旧元素
                        var div = document.createElement('div');
                        var simple = $(e).attr('simple')
                        $(e).after(div).remove()
                        // 是否选中高级模式
                        if (simple === undefined) {
                            xmSelect.render({
                                el: div,
                                data: data,
                                name: name,
                                filterable: true,
                                autoRow: true,
                                paging: true,
                                pageSize: 5,
                                toolbar: {
                                    show: toolbar,
                                },
                            })
                        }else{
                            xmSelect.render({
                                el: div,
                                data: data,
                                name: name,
                            })
                        }
                    }
                })
            }
        }
    }

    return Form;
})
