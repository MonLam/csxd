define(['jquery', 'layui'], function ($, Layui) {
    // 模板
    var tpl = ' <div id="chooseimg">\
                    <div class="row">\
                        <div class="col-xs-6" style="margin-left: 4px">\
                            <div class="form-group">\
                                <div class="input-group">\
                                    <input type="text" id="img-select-search-input" class="form-control" placeholder="搜索"autocomplete="off" />\
                                    <a href="javascript:;" id="img-select-search" class="input-group-addon" lay-event="search"><i class="fa fa-search"></i></a>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="clearfix"/>\
                    <div class="col-xs-12"><div class="row"><ul id="img-select-list" class="img-select-list"></ul></div></div>\
                    <div class="col-xs-12"><div class="row"><div id="img-select-page"></div></div></div>\
                </div>';

    var ImgSelect = {
        // leyer弹窗ID
        LayerID: null,
        // 选中后回调
        onSelect: null,
        // 当前页
        page: 1,
        // 每页显示数据
        limit: 18,
        // 获取数据接口API
        api: '/admin/general/assets/select',
        // 搜索的内容
        search: '',
        // 标志位数据
        mark: '',
        // img选择
        show: function (api, mark) {
            if (api) {
                ImgSelect.api = api
            }
            if (mark) {
                ImgSelect.mark = mark
            } else {
                ImgSelect.mark = ''
            }
            var url = ImgSelect.replaceurl(ImgSelect.api, ImgSelect.page);
            ImgSelect.LayerID = Layer.open({
                type: 1,
                area: ['720px', '480px'],
                scrollbar: false,
                content: tpl,
                success: function (layero, index) {
                    $.get(url, function (ret) {
                        if (ret.code == '1') {
                            // 渲染表格
                            ImgSelect.render(ret.data, ret.count)
                            // 绑定事件
                            ImgSelect.bindEvent();
                        } else {
                            Layer.msg('获取图片资源失败', { icon: 2 });
                        }
                    });
                }
            })
        },
        // 获取处理数据
        getData(url) {
            $.get(url, function (ret) {
                if (ret.code == '1') {
                    ImgSelect.render(ret.data)
                } else {
                    Layer.msg('获取图片资源失败', { icon: 2 });
                }
            });
        },
        // 添加参数
        replaceurl: function (url, page, search) {
            var query = 'limit=' + ImgSelect.limit;
            if (page) {
                query = query + '&page=' + page
            }
            if (search) {
                query = query + '&title=' + search
            }
            return url + ((url.match(/(\?|&)+/) ? "&" : "?") + query)
        },
        // 选中的图片后的回调绑定
        callback: function (callback) {
            if (typeof callback == 'function') {
                ImgSelect.onSelect = callback
            } else {
                console.error('mon-ImgSelect: icon selected callback not is function!');
            }
        },
        // 渲染
        render: function (data, count) {
            // <li title="1779719795ff7f325a1159.png">
            //     <img src="http://demo.test/upload/202101/1779719795ff7f325a1159.png" draggable="false" alt="">
            //     <div class="img-select-name">1779719795ff7f325a1159.png</div>
            // </li>
            if (data.length > 0) {
                var html = [];
                for (var i = 0, l = data.length; i < l; i++) {
                    var item = data[i]
                    var temp = '<li class="img-select-item" data-mark="' + ImgSelect.mark + '" data-url="' + item.url + '" data-id="' + item.id + '" title="' + item.title + '">\
                                    <img src="'+ item.url + '" draggable="false" alt="' + item.title + '">\
                                    <div class="img-select-name">' + item.title + '</div>\
                                </li>'
                    html.push(temp)
                }
                $('#img-select-list').html(html.join(''))
            } else {
                $('#img-select-list').html('<p class="text-center">暂无数据</p>')
            }

            if (count > 0) {
                Layui.use('laypage', function (page) {
                    page.render({
                        elem: 'img-select-page',
                        count: count,
                        limit: ImgSelect.limit,
                        curr: ImgSelect.page,
                        jump: function (obj, first) {
                            //obj包含了当前分页的所有参数，比如：
                            var curr = obj.curr
                            var url = ImgSelect.replaceurl(ImgSelect.api, curr, ImgSelect.search)
                            $.get(url, function (ret) {
                                if (ret.code == '1') {
                                    ImgSelect.render(ret.data)
                                } else {
                                    Layer.msg('获取图片资源失败', { icon: 2 });
                                }
                            });
                        }
                    });
                })
            }
        },
        // 绑定事件
        bindEvent: function () {
            // 搜索
            $('#img-select-search').on('click', function () {
                var search = $('#img-select-search-input').val()
                ImgSelect.page = 1
                var url = ImgSelect.replaceurl(ImgSelect.api, ImgSelect.page, search);
                ImgSelect.getData(url)
            })
            // 选择
            $('#chooseimg').on('click', '.img-select-item', function () {
                if (typeof ImgSelect.onSelect == 'function') {
                    var data = $(this).data()
                    ImgSelect.onSelect.call(this, data, ImgSelect.LayerID, Layui)
                }
            })
        }
    }

    return ImgSelect
});