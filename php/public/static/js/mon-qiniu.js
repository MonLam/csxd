/**
 * 七牛云文件上传
 * 
 * @see 结合upload插件，实现上传，render方法，参数同upload插件一致
 */
define(['jquery', 'upload'], function ($, Upload) {
    // 获取服务器token的API地址
    var getTokenApi = '/admin/console/qiniu'
    // 七牛云上传API地址
    var qiniuApi = {
        // 华东
        z0: 'http://upload.qiniup.com',
        // 华北
        z1: 'http://upload-z1.qiniup.com',
        // 华南
        z2: 'http://upload-z2.qiniup.com',
        // 北美
        na0: 'http://upload-na0.qiniup.com',
        // 东南亚
        as0: 'http://upload-as0.qiniup.com',
    }

    var qiniu = {
        // 获取七牛云上传的URL
        getUrl: function (type) {
            return qiniuApi[type] ? qiniuApi[type] : console.error('mon-qiniu hint: 未知的存储空间')
        },
        // 获取token
        getToken: function () {
            var token;
            $.ajax({
                // ajax 非异步获取taken
                async: false,
                type: 'post',
                url: getTokenApi,
                success: function (res) {
                    token = res.data.token;
                }
            });
            return token;
        },
        // 使用upload插件渲染，参数同upload，但不支持data参数，data参数将被覆盖
        render: function (option) {
            var config = $.extend({}, option, {
                data: {
                    token: function () {
                        return qiniu.getToken();
                    }
                }
            })
            Upload.render(config)
        }
    }

    return qiniu;
});