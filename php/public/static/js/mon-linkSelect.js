/**
 * 无限分级select
 */
define(['jquery', 'layui', 'mon'], function ($, Layui, Mon) {
    var linkSelect = {
        // 当前组件获取的各种值
        data: {
            // 当前选中数据值名数据
            selected: [],
            // 当前选中的值
            values: [],
            // 当前选中的名
            names: [],
            // 当前选中最后一个值
            lastValue: '',
            // 当前选中最后一个值
            lastName: '',
            // 是否已选
            isSelected: false,
        },
        // 配置信息
        config: {
            // 渲染的dom节点
            elm: '',
            // layui-form的filter
            formFilter: null,
            // 渲染的select的filter
            selectFilter: 'link-select',
            // 渲染select的name值
            selectName: 'link-select',
            // 首次渲染是否重新绘制dom节点内容
            append: false,
            // 初始化数据
            data: [],
            // 默认选中值
            selected: [],
            // 空值项提示，可设置为数组['请选择省','请选择市','请选择县']
            tips: '请选择',
            // 是否允许搜索，可设置为数组[true,true,true]
            search: false,
            // 选择项宽度，可设置为数组['80','90','100']
            width: null,
            // 为真只取最后一个值
            last: false,
            // 值验证，与lay-verify一致
            verify: '',
            // 验证提示方式
            vertype: 'msg',
            // 必填提示
            reqText: '',
            // 数据分隔符
            delimiter: ',',
            // 数据的键名 status=0为禁用状态
            field: {
                idName: 'id',
                titleName: 'title',
                statusName: 'status',
                childName: 'children'
            },
            // 选项修改后的回调方法
            onChange: null,
            // 异步获取数据配置
            queryConfig: {
                // 数据获取的URL
                url: null,
                // 请求方式
                method: 'get',
                // 请求数据
                request: {

                },
            }
        },
        // 写入数据的索引
        appendIndex: 0,

        // 初始化配置
        init: function (config) {
            this.config = $.extend(this.config, config);
        },

        // 渲染
        render: function (config) {
            this.init(config);
            // 判断容器
            var elm = $(this.config.elm)
            if (elm.length == 0) {
                console.error('link-select hint：找不到容器 ' + this.config.elm);
                return false;
            }
            // 判断数据
            if (this.config.data.length == 0) {
                // 不存在数据，异步获取数据渲染
                this.getDataInit();
            } else {
                // 存在数据，直接渲染
                this.initSelect()
            }
            //  绑定修改时间
            Layui.use('form', function (form) {
                //监听下拉事件
                form.on('select(' + linkSelect.config.selectFilter + ')', function (data) {
                    linkSelect.change(data);
                });
            })
        },

        // 异步获取数据, 渲染select
        getDataInit: function () {
            if (this.config.queryConfig.url == null || this.config.queryConfig.url == undefined) {
                console.error('lins-select hint：未定义ajax请求URL ');
                return false
            }

            var queryData = this.config.queryConfig.request || {}
            Mon.api.ajax({
                url: this.config.queryConfig.url,
                method: this.config.queryConfig.method,
                data: queryData
            }, function (res) {
                linkSelect.config.data = res
                linkSelect.initSelect()

                return false
            }, function (err) {
                console.error('lins-select hint：候选数据ajax请求错误 ');
            })
        },

        // 初始化渲染
        initSelect: function (selected) {
            var elm = $(this.config.elm)
            selected = typeof selected == 'undefined' ? this.config.selected : selected;
            var verify = this.config.verify == '' ? '' : 'lay-verify="' + this.config.verify + '" ';
            var html = '<div style="height:0px;width:0px;overflow:hidden"><input value="' + selected.join(this.config.delimiter) + '" type="hidden" lay-reqText="' + this.config.reqText + '" lay-vertype="' + this.config.vertype + '" ' + verify + 'name="' + this.config.selectName + '"></div>';
            html += this.createSelect(this.config.data);
            if (!this.config.append && this.appendIndex < 1) {
                // 替换重新写入html
                elm.html(html);
            } else {
                // 追加
                elm.append(html);
            }
            this.appendIndex++

            var index = [];
            for (var i = 0; i < selected.length; i++) {
                // 设置最后一个select的选中值
                elm.find('select:last').val(selected[i]);
                // 获取该选中值的索引
                var lastIndex = elm.find('select:last').get(0).selectedIndex - 1;
                index.push(lastIndex);
                // 取出下级的选项值
                var childItem = this.getOptionData(this.config.data, index);
                // 下级选项值存在则创建select
                if (childItem) {
                    var html = this.createSelect(childItem);
                    elm.append(html);
                }
            }

            // 重绘select
            Layui.use('form', function (form) {
                form.render('select', linkSelect.formFilter);
            })
        },

        // 创建select
        createSelect: function (optionData) {
            var c = this.config;
            var f = c.field;
            var html = '';
            html += '<div class="layui-input-inline mon-lins-select-item" ' + this.setWidth() + '>';
            html += ' <select ' + this.setSearch() + 'lay-filter="' + c.selectFilter + '">';
            html += '  <option value="">' + this.setTips() + '</option>';
            for (var i = 0; i < optionData.length; i++) {
                var disabled = optionData[i][f.statusName] == 0 ? 'disabled="" ' : '';
                html += '  <option ' + disabled + 'value="' + optionData[i][f.idName] + '">' + optionData[i][f.titleName] + '</option>';
            }
            html += ' </select>';
            html += '</div>';
            return html;
        },

        // 设置select宽度
        setWidth: function () {
            var c = this.config;
            if (Object.prototype.toString.call(c.width) != '[object Array]') {
                return /^\d+$/.test(c.width) ? 'style="width:' + c.width + 'px;" ' : ' ';
            } else {
                var i = $(c.elm).find('select').length;
                if (c.width.hasOwnProperty(i)) {
                    return /^\d+$/.test(c.width[i]) ? 'style="width:' + c.width[i] + 'px;" ' : ' ';
                }
            }
        },

        // 设置搜索
        setSearch: function () {
            var c = this.config;
            if (Object.prototype.toString.call(c.search) != '[object Array]') {
                return c.search === true ? 'lay-search ' : ' ';
            } else {
                var i = $(c.elm).find('select').length;
                if (c.search.hasOwnProperty(i)) {
                    return c.search[i] === true ? 'lay-search ' : ' ';
                }
            }
            return ' ';
        },

        // “请选择”文字
        setTips: function () {
            var c = this.config;
            if (Object.prototype.toString.call(c.tips) != '[object Array]') {
                return c.tips;
            } else {
                var i = $(c.elm).find('select').length;
                return c.tips.hasOwnProperty(i) ? c.tips[i] : '请选择';
            }
        },

        // 获取当前option的数据
        getOptionData: function (catData, optionIndex) {
            var f = this.config.field;
            var item = catData;
            for (var i = 0; i < optionIndex.length; i++) {
                if ('undefined' == typeof item[optionIndex[i]]) {
                    item = null;
                    break;
                } else if ('undefined' == typeof item[optionIndex[i]][f.childName]) {
                    item = null;
                    break;
                } else {
                    item = item[optionIndex[i]][f.childName];
                }
            }
            return item;
        },

        // 获取所有值-数组 每次选择后执行
        getSelected: function () {
            var c = this.config;
            var values = [];
            var names = [];
            var selected = [];
            elm = $(c.elm);
            elm.find('select').each(function () {
                var item = {};
                var v = $(this).val()
                var n = $(this).find('option:selected').text();
                item.value = v;
                item.name = n;
                values.push(v);
                names.push(n);
                selected.push(item);
            });
            this.data.selected = selected;
            this.data.values = values;
            this.data.names = names;
            this.data.lastValue = elm.find('select:last').val();
            this.data.lastName = elm.find('option:selected:last').text();

            this.data.isSelected = this.data.lastValue == '' ? false : true;
            var inputVal = c.last === true ? this.data.lastValue : this.data.values.join(c.delimiter);
            elm.find('input[name="' + c.selectName + '"]').val(inputVal);
        },

        // 切换选项
        change: function (obj) {
            var elm = $(obj.elem)
            var $thisItem = elm.parent();
            // 移除后面的select
            $thisItem.nextAll('div.layui-input-inline').remove();
            var index = [];
            // 获取所有select，取出选中项的值和索引
            $thisItem.parent().find('select').each(function () {
                index.push($(this).get(0).selectedIndex - 1);
            });

            var childItem = this.getOptionData(this.config.data, index);
            if (childItem) {
                var html = this.createSelect(childItem);
                $thisItem.after(html);
                // 重绘select
                Layui.use('form', function (form) {
                    form.render('select', linkSelect.formFilter);
                })
            }
            this.getSelected();
            // 执行回调
            if (typeof this.config.onChange == 'function') {
                // 回传当前选择的数据
                this.config.onChange.call(this, this.data)
            }
        },

        // 获取数据
        getData() {
            return this.data;
        }
    };

    return linkSelect;
});