define(['jquery', 'tinymce'], function ($, Tinymce) {
    var editor = {
        // 图片上传URL，因为要在config的上传回调方法中使用URL，而URL需要重载，所以上传的URL外置出来做属性，特异化处理
        images_upload_url: '',
        // 默认配置信息
        config: {
            // 是否自动保存到textarea中
            saveTextarea: false,
            // 资源路径
            base_url: '/static/libs/tinymce',
            // 扩展文件加载后缀名
            suffix: '.min',
            // 使用语言
            language: 'zh_CN',
            // 格式字体从pt改为px
            fontsize_formats: '8px 10px 12px 14px 16px 18px 24px 36px',
            // 自动获取焦点
            auto_focus: false,
            // 显示右下角官方信息
            branding: false,
            // 最小高度
            min_height: 480,
            // 允许修改大小
            resize: true,
            // 皮肤，样式存放于skins/ui目录
            skin: 'oxide',
            // 加载插件列表
            plugins: ['lists', 'advlist', 'code', 'table', 'link', 'image', 'print', 'preview', 'searchreplace', 'fullscreen', 'media', 'imagetools', 'anchor', 'autolink', 'autosave', 'hr', 'pagebreak', 'paste', 'wordcount', 'bdmap'],
            // 工具栏列表
            toolbar: 'undo redo | styleselect | bold italic forecolor backcolor | bullist numlist | alignleft aligncenter alignright alignjustify | outdent indent | hr pagebreak | bdmap searchreplace print fullscreen',
            // 开启图片高级选项
            image_advtab: true,
            // 自定义图片上传
            images_upload_handler: function (blobInfo, success, fail) {
                var xhr, formData;
                var file = blobInfo.blob(); // 转化为易于理解的file对象
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', editor.images_upload_url);
                xhr.onload = function () {
                    var json;
                    if (xhr.status != 200) {
                        fail('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);
                    if (!json) {
                        fail('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    if (json.code != '1') {
                        fail('upload faild: ' + json.msg);
                        return;
                    }
                    success(json.data.url);
                };
                formData = new FormData();
                formData.append('file', file, file.name);
                xhr.send(formData);
            },
            // 初始化
            setup: function (ed) {
                // 自动保存内容到textarea
                if (editor.config.saveTextarea) {
                    ed.on('change', function () {
                        Tinymce.triggerSave()
                    })
                }
            },
            // 创建实例后
            init_instance_callback: function (ed) {

            }
        },
        // 全局初始化
        init: function (config) {
            this.config = $.extend(this.config, config)
            // 特异化设置图片上传URL
            if (this.config.images_upload_url) {
                this.images_upload_url = setting.images_upload_url
            }
            return this
        },
        // 渲染生成tinymce
        render: function (id, config) {
            if (typeof id != 'string' || id == '') {
                console.error('mon-tinymce hint：请输入容器ID');
                return false
            }
            // 获取配置
            var setting = {}
            if (typeof config == 'object') {
                setting = $.extend(setting, this.config, config)
                // 特异化设置图片上传URL
                if (setting.images_upload_url) {
                    this.images_upload_url = setting.images_upload_url
                }
            } else {
                setting = this.config
            }
            setting.selector = '#' + id
            // 渲染Tinymce
            Tinymce.init(setting)
        },
        // 获取tinymce
        getEditor: function () {
            return Tinymce
        },
        // 获取内容
        getContent(id) {
            return Tinymce.editors[id].getContent();
        },
        // 设置内容
        setContent(id, content) {
            Tinymce.editors[id].setContent(content);
        }
    }

    return editor
});