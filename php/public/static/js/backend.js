define(['mon'], function (Mon) {
    var Backend = {
        api: {
            // 刷新导航菜单
            refreshmenu: function () {
                top.window.$(".sidebar-menu").trigger("refresh");
            },
            addtabs: function (url, title, icon) {
                var dom = "a[url='{url}']"
                var leftlink = top.window.$(dom.replace(/\{url\}/, url));
                if (leftlink.length > 0) {
                    leftlink.trigger("click");
                } else {
                    url = Mon.api.fixurl(url);
                    leftlink = top.window.$(dom.replace(/\{url\}/, url));
                    if (leftlink.length > 0) {
                        var event = leftlink.parent().hasClass("active") ? "dblclick" : "click";
                        leftlink.trigger(event);
                    } else {
                        var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
                        leftlink = top.window.$(dom.replace(/\{url\}/, baseurl));
                        if (leftlink.length > 0) {
                            icon = typeof icon !== 'undefined' ? icon : leftlink.find("i").attr("class");
                            title = typeof title !== 'undefined' ? title : leftlink.find("span:first").text();
                            leftlink.trigger("fa.event.toggleitem");
                        }
                        var navnode = top.window.$(".nav-tabs ul li a[node-url='" + url + "']");
                        if (navnode.length > 0) {
                            navnode.trigger("click");
                        } else {
                            // 追加新的tab
                            var id = Math.floor(new Date().valueOf() * Math.random());
                            icon = typeof icon !== 'undefined' ? icon : 'fa fa-circle-o';
                            title = typeof title !== 'undefined' ? title : '';
                            top.window.$("<a />").append('<i class="' + icon + '"></i> <span>' + title + '</span>').prop("href", url).attr({
                                url: url,
                                addtabs: id
                            }).addClass("hide").appendTo(top.window.document.body).trigger("click");
                        }
                    }
                }
            },
            closetabs: function (url) {
                if (typeof url === 'undefined') {
                    top.window.$("ul.nav-addtabs li.active .close-tab").trigger("click");
                } else {
                    var dom = "a[url='{url}']"
                    var navlink = top.window.$(dom.replace(/\{url\}/, url));
                    if (navlink.length === 0) {
                        url = Mon.api.fixurl(url);
                        navlink = top.window.$(dom.replace(/\{url\}/, url));
                        if (navlink.length === 0) {
                        } else {
                            var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
                            navlink = top.window.$(dom.replace(/\{url\}/, baseurl));
                            if (navlink.length === 0) {
                                navlink = top.window.$(".nav-tabs ul li a[node-url='" + url + "']");
                            }
                        }
                    }
                    if (navlink.length > 0 && navlink.attr('addtabs')) {
                        top.window.$("ul.nav-addtabs li#tab_" + navlink.attr('addtabs') + " .close-tab").trigger("click");
                    }
                }
            },
        },
        init: function () {
            // 添加ios-fix兼容iOS下的iframe
            if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                $("html").addClass("ios-fix");
            }
            // 配置Toastr的参数
            Toastr.options.positionClass = "toast-top-right";
            // 点击包含.btn-dialog的元素时弹出dialog，存在confirm则先弹出提示框
            $(document).on('click', '.btn-dialog', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                var url = $(that).attr("href") || $(that).data('href')
                var title = $(that).attr("title") || $(that).data("title");
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Mon.api.open(url, title, options);
                        Layer.close(index);
                    });
                } else {
                    Mon.api.open(url, title, options);
                }
                return false;
            });
            // 点击包含.btn-addtabs的元素时新增选项卡
            $(document).on('click', '.btn-addtabs', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                var url = $(that).attr('href') || $(that).data("href")
                var title = $(that).attr("title") || $(that).data("title") || $(that).data('original-title');
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.addtabs(url, title);
                        Layer.close(index);
                    });
                } else {
                    Backend.api.addtabs(url, title);
                }
                return false;
            });
            // 修复含有fixed-footer类的body边距
            if ($(".fixed-footer").length > 0) {
                $(document.body).css("padding-bottom", $(".fixed-footer").outerHeight());
            }
            // 修复不在iframe时layer-footer隐藏的问题
            if ($(".layer-footer").length > 0 && self === top) {
                $(".layer-footer").show();
            }

            // 将Backend渲染至全局,以便于在子框架中调用
            window.Backend = Backend;
        }
    };

    Backend.init();
    return Backend;
});