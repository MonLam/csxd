/**
 * 数据表格
 */
define(['jquery', 'layui'], function ($, Layui) {
    var Table = {
        // layui
        Layui: Layui,
        // 默认配置
        default: {
            // 默认Layui-table配置
            config: {
                id: 'table',
                // 异步获取数据的URL
                url: '',
                // 请求方式
                method: 'get',
                // 请求参数
                where: {},
                // 不显示loading
                // loading: false,
                // 开启分页
                page: true,
                limit: 10,
                limits: [10, 20, 50],
                // 禁用自动排序
                autoSort: false,
                // 转换数据
                // parseData: function (res) {
                //     return {
                //         "code": res.code == '1' ? 0 : 1,
                //         "msg": res.msg,
                //         "count": res.count,
                //         "data": res.data
                //     };
                // },
                // 定义接收数据格式
                response: {
                    statusName: 'code',
                    statusCode: 1,
                    msgName: 'msg',
                    countName: 'count',
                    dataName: 'data'
                },
                // 重新定义请求参数名
                request: {
                    // pageName: 'offset'
                    // limitName: 'nums' //每页数据量的参数名，默认：limit
                },
                // toolbar: '#toolbar',
                // 表格外观，line （行边框风格）row （列边框风格）nob （无边框风格）
                skin: 'line'
            },
            // 操作按钮URL
            operURL: {
                add: '',
                edit: '',
                del: '',
            },
            // 格式化默认数据
            format: {
                baseImg: requirejs.s.contexts._.config.config.site.cdnurl + '/static/img/blank.gif',
                baseImgClass: 'img-sm img-center',
            }
        },
        // 表格配置信息
        config: {},
        // 获取实例, 执行回调
        callback: function (callback) {
            Layui.use('table', function (table) {
                callback.call(this, table)
            })
        },
        // js渲染
        render(option) {
            Layui.use(['table'], function (table) {
                Table.config = $.extend(Table.default.config, option)
                table.render(Table.config);
            })
        },
        // html渲染
        init(element, option) {
            Layui.use(['table'], function (table) {
                Table.config = $.extend(Table.default.config, option)
                table.init(element, Table.config);
            })
        },
        // 刷新表格
        reload(filter, option) {
            Layui.use(['table'], function (table) {
                table.reload(filter, option || {})
            })
        },
        // 搜索
        search(filter, where) {
            Table.reload(filter, {
                page: {
                    // 重新返回第一页
                    curr: 1,
                },
                where: where || {}
            })
        },
        // 绑定事件
        bindEvent(filter, option) {
            filter = typeof filter == 'string' ? filter : 'table';
            Layui.use(['table'], function (table) {
                var eventConfig = $.extend(Table.default.operURL, option || {})

                // 绑定tool事件
                Table.event.bindTool(filter, eventConfig, table)
                // 绑定sort事件
                Table.event.bindSort(filter, eventConfig, table)
                // 绑定刷新事件
                Table.event.bindRefresh(filter, eventConfig, table)
            })
        },
        // 绑定事件
        event: {
            // tool事件
            bindTool: function (filter, option, table) {
                table.on('tool(' + filter + ')', function (obj) {
                    var data = obj.data;
                    var event = obj.event;
                    // 打开url弹窗
                    if (option[event]) {
                        var title = '操作'
                        switch (event) {
                            case 'edit':
                                title = '编辑'
                                Mon.api.open(Table.format.replaceurl(option[event], data.id), title);
                                break;
                            case 'route':
                                title = '定义路由'
                                Mon.api.open(Table.format.replaceurl(option[event], data.id), title);
                                break;
                            case 'del':
                                Layer.confirm('确定执行该删除操作么？', {
                                    icon: 3
                                }, function (index) {
                                    var query = {
                                        url: Table.format.replaceurl(option[event], data.id),
                                    }
                                    // 发起请求
                                    Mon.api.ajax(query, function () {
                                        // 成功则刷新表格，
                                        Layer.close(index);
                                        Table.reload(Table.config.id)
                                    });
                                })
                                break;
                            case 'add':
                                title = '新增'
                                Mon.api.open(Table.format.replaceurl(option[event], data.id), title);
                                break;
                            case 'toggle':
                                var queryData = $(this).data();
                                Layer.confirm(queryData.title || '确定执行该操作么？', {
                                    icon: 3
                                }, function (index) {
                                    var query = {
                                        url: Table.format.replaceurl(option[event], data.id),
                                        data: queryData
                                    }
                                    // 发起请求
                                    Mon.api.ajax(query, function () {
                                        // 成功则刷新表格，
                                        Layer.close(index);
                                        Table.reload(Table.config.id)
                                    });
                                })
                                break;
                            default:
                                // 判断是否存在自定义扩展事件配置，存在则调用对应的回调方法
                                // if (option['custom'] && option['custom'][event] && typeof option['custom'][event] == 'function') {
                                //     var callback = option['custom'][event]
                                //     callback.call(this, table, data)
                                // }

                                // 不存在对应的事件处理，直接打开页面
                                Mon.api.open(Table.format.replaceurl(option[event], data.id), title);
                                break;
                        }
                    } else {
                        // 不存在URL，直接判断是否存在自定义扩展事件配置，存在则调用对应的回调方法
                        if (option['custom'] && option['custom'][event] && typeof option['custom'][event] == 'function') {
                            var callback = option['custom'][event]
                            callback.call(this, table, data)
                        }
                    }
                });
            },
            // 排序事件
            bindSort: function (filter, option, table) {
                table.on('sort(' + filter + ')', function (obj) {
                    var field = obj.field;
                    table.reload(filter, {
                        initSort: obj,
                        where: {
                            order: field,
                            sort: obj.type
                        }
                    });

                })
            },
            // 刷新事件
            bindRefresh: function (filter, option, table) {
                $('#toolbar').on('click', '.btn-refresh', function () {
                    table.reload(filter)
                })
            },
            // 行点击
            bindClick: function (filter, callback) {
                Layui.use('table', function (table) {
                    table.on('row(' + filter + ')', function (obj) {
                        // console.log(obj.tr) //得到当前行元素对象
                        // console.log(obj.data) //得到当前行数据
                        //obj.del(); //删除当前行
                        //obj.update(fields) //修改当前行数据
                        callback.call(this, table, obj)
                    });
                })
            },
            // 行双击
            bindDBClick: function (filter, callback) {
                Layui.use('table', function (table) {
                    table.on('rowDouble(' + filter + ')', function (obj) {
                        // console.log(obj.tr) //得到当前行元素对象
                        // console.log(obj.data) //得到当前行数据
                        //obj.del(); //删除当前行
                        //obj.update(fields) //修改当前行数据
                        callback.call(this, table, obj)
                    });
                })
            },
        },
        // 格式化数据
        format: {
            // 图标
            icon: function (value) {
                value = typeof value != 'object' ? value : value.icon
                return '<i class="' + value + '"></i>';
            },
            avatar: function (value, className) {
                value = typeof value != 'object' ? value : value.img
                value = value ? value : Table.default.format.baseImg;
                var classAttr = className ? className : Table.default.format.baseImgClass;
                return '<a href="javascript:void(0)"><img class="img-circle + ' + classAttr + '" src="' + Mon.api.cdnurl(value) + '" /></a>';
            },
            // 图片
            image: function (value, className) {
                value = typeof value != 'object' ? value : value.img
                value = value ? value : Table.default.format.baseImg;
                var classAttr = className ? className : Table.default.format.baseImgClass;
                return '<a href="' + Mon.api.cdnurl(value) + '" target="_blank"><img class="' + classAttr + '" src="' + Mon.api.cdnurl(value) + '" /></a>';
            },
            // 多张图片
            images: function (value, className) {
                value = typeof value != 'object' ? value : value.img
                value = value === null ? '' : value.toString();
                var classAttr = className ? className : Table.default.format.baseImgClass;
                var arr = value.split(',');
                var html = [];
                $.each(arr, function (i, value) {
                    value = value ? value : Table.default.format.baseImg;
                    html.push('<a href="' + Mon.api.cdnurl(value) + '" target="_blank"><img class="' + classAttr + '" src="' + Mon.api.cdnurl(value) + '" /></a>');
                });
                return html.join(' ');
            },
            // 状态
            status: function (value) {
                value = typeof value != 'object' ? value : value.status
                var display = value == 1 ? '有效' : value == 2 ? '无效' : '未知';
                return value == '1' ? '<i class="fa fa-check text-success" style="font-size: 14px"> ' + display + '</i>' : '<i class="fa fa-close text-danger" style="font-size: 14px"> ' + display + '</i>'
            },
            // URL
            url: function (value) {
                // 过滤空链接
                if (value == '' || value == undefined) {
                    return ''
                }
                value = typeof value != 'object' ? value : value.url
                return '<div class="input-group input-group-sm" style="width:200px;margin:0 auto;"><input type="text" class="form-control input-sm" readonly value="' + value + '"><span class="input-group-btn input-group-sm"><a href="' + value + '" target="_blank" class="btn btn-default btn-sm"><i class="fa fa-link"></i></a></span></div>';
            },
            // 日期时间
            dateTime: function (value) {
                value = typeof value != 'object' ? value : value.timestamp
                return value ? Mon.api.dateFormat(value, 'Y-m-d-H-i-s') : '';
            },
            // 日期
            date: function (value) {
                value = typeof value != 'object' ? value : value.timestamp
                return value ? Mon.api.dateFormat(value, 'Y-m-d') : '';
            },
            // 自动添加idx参数
            replaceurl: function (url, idx) {
                return !url.match(/\{idx\}/i) ? url + (url.match(/(\?|&)+/) ? "&idx=" : "?idx=") + idx : url;
            },
        }

    }
    return Table
});