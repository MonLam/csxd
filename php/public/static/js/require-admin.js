require.config({
    baseUrl: requirejs.s.contexts._.config.assets.baseUrl,
    // 不使用require自带的版本控制参数，用配置文件逐一配置，优化版本更新的影响
    // urlArgs: "dev=" + requirejs.s.contexts._.config.config.site.version,
    waitSeconds: 30,
    charset: 'utf-8',
    map: requirejs.s.contexts._.config.assets.map,
    paths: requirejs.s.contexts._.config.assets.paths,
    shim: requirejs.s.contexts._.config.assets.shim,
});

require(['jquery'], function ($) {
    // 初始配置
    var Config = requirejs.s.contexts._.config.config;
    // 将Config渲染到全局
    window.Config = Config;
    // 初始化
    $(function () {
        require(['backend'], function (Backend) {
            //加载相应模块
            if (Config.jsname) {
                var script = requirejs.s.contexts._.config.assets.baseUrl + Config.jsname + '.js?v=' + requirejs.s.contexts._.config.config.site.version
                require([script], function (Controller) {
                    if (Controller.hasOwnProperty(Config.actionname)) {
                        Controller[Config.actionname]();
                    }
                }, function (e) {
                    console.error(e);
                    // 这里可捕获模块加载的错误
                });
            }
        });
    });
});
