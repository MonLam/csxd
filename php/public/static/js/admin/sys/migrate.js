define(['jquery', 'backend', 'table'], function ($, Backend, Table) {
    var api = {
        index: '/admin/sys/backup',
        table: '/admin/sys/backup/table',
        field: '/admin/sys/backup/field',
        download: '/admin/sys/backup/download',
        del: '/admin/sys/backup/del',
        import: '/admin/sys/backup/import',
        backup: '/admin/sys/backup/save',
        optimize: '/admin/sys/backup/optimize',
        repair: '/admin/sys/backup/repair',
        dictionary: '/admin/sys/backup/dictionary'
    }
    var Controller = {
        index: function () {
            // 备份数据表格
            Table.render({
                id: 'backupTable',
                elem: '#backupTable',
                url: api.index,
                page: false,
                cols: [[
                    { field: 'filename', title: '备份名称', minWidth: 240 },
                    { field: 'part', title: 'part', width: 80, align: 'center' },
                    { field: 'size', title: '大小', width: 140, align: 'center', templet: function (d) { return Controller.format.parseByte(d.size) } },
                    { field: 'compress', title: 'compress', width: 100, align: 'center' },
                    { field: 'time', title: '备份时间', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.time) }, sort: true },
                    { title: '操作', templet: '#operate', minWidth: 200 }
                ]]
            })
            // 表数据
            if (window.tableAuth) {
                var defaultToolbar = ['exports', 'print']
                if (window.dictionaryAuth) {
                    defaultToolbar.push({
                        title: '数据字典',
                        layEvent: 'dictionary',
                        icon: 'layui-icon-read'
                    })
                }
                Table.render({
                    id: 'table',
                    elem: '#table',
                    url: api.table,
                    page: false,
                    toolbar: '#table-toolbar',
                    defaultToolbar: defaultToolbar,
                    cols: [[
                        { type: 'checkbox' },
                        { field: 'name', title: '表名', width: 240 },
                        { field: 'comment', title: '备注', minWidth: 200 },
                        { field: 'engine', title: '类型', width: 120, align: 'center' },
                        { field: 'data_length', title: '大小', width: 180, align: 'center', templet: function (d) { return Controller.format.parseByte(d.data_length) } },
                        { field: 'rows', title: '行数', width: 160, align: 'center' },
                        { field: 'update_time', title: '更新时间', width: 168, align: 'center' },
                        { title: '操作', templet: '#table-operate', width: 120 }
                    ]]
                })
            }

            // 绑定表格事件
            Table.callback(function (table) {
                // 备份记录
                table.on('tool(backupTable)', function (obj) {
                    var data = obj.data
                    var event = obj.event
                    switch (event) {
                        case 'download':
                            window.open(api.download + '?filename=' + data.time)
                            break;
                        case 'import':
                            Layer.alert('导入功能暂未开放，尽请期待后续更新开放功能');
                            break
                        case 'del':
                            Layer.confirm('确定要删除该备份记录么？', { icon: 3 }, function (index) {
                                var query = {
                                    url: api.del,
                                    data: {
                                        filename: data.time
                                    }
                                }
                                // 发起请求
                                Mon.api.ajax(query, function () {
                                    // 成功则刷新表格，
                                    Layer.close(index);
                                    Table.reload('backupTable')
                                });
                            })
                            break
                    }
                })
                if (window.tableAuth) {
                    // 表数据
                    table.on('tool(table)', function (obj) {
                        var data = obj.data
                        var event = obj.event
                        switch (event) {
                            case 'field':
                                Mon.api.open(api.field + '?table=' + data.name, data.name + '：' + data.comment);
                                break;
                        }
                    })
                    table.on('toolbar(table)', function (obj) {
                        // 获取选中的记录
                        var checkStatus = table.checkStatus(obj.config.id);
                        var dataList = checkStatus.data
                        var data = [];
                        for (var i = 0, l = dataList.length; i < l; i++) {
                            data.push(dataList[i].name)
                        }
                        var event = obj.event
                        switch (event) {
                            case 'backup':
                                if (!data.length) {
                                    return Toastr.error('请选择需要备份的库表');
                                }
                                Mon.api.ajax({ url: api.backup, data: { tables: data } }, function () {
                                    // 成功则刷新备份记录表格，
                                    Table.reload('backupTable')
                                });
                                break;
                            case 'optimize':
                                if (!data.length) {
                                    return Toastr.error('请选择需要优化的库表');
                                }
                                Mon.api.ajax({ url: api.optimize, data: { tables: data } });
                                break;
                            case 'repair':
                                if (!data.length) {
                                    return Toastr.error('请选择需要修复的库表');
                                }
                                Mon.api.ajax({ url: api.repair, data: { tables: data } });
                                break;
                            case 'dictionary':
                                window.open(api.dictionary)
                                break;
                        }
                    })
                }
            })
        },
        format: {
            // 格式化文件大小
            parseByte: function (size) {
                if (size == 0) {
                    return '-';
                }
                var type = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
                var pos = 0;
                while (size >= 1024) {
                    size /= 1024;
                    pos++;
                }

                return (Math.round(size * 100) / 100) + ' ' + type[pos]
            }
        }
    };
    return Controller;
});
