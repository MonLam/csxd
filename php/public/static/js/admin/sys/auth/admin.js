define(['jquery', 'backend', 'table', 'form'], function ($, Backend, Table, Form) {
    var api = {
        index: '/admin/sys/auth/admin',
        add: '/admin/sys/auth/admin/add',
        edit: '/admin/sys/auth/admin/edit',
        toggle: '/admin/sys/auth/admin/toggle',
        password: '/admin/sys/auth/admin/password',
        custom: {
            password: function (table, data) {
                Mon.api.open(Table.format.replaceurl(api.password, data.id), '重置密码');
            }
        }
    }
    var Controller = {
        index: function () {
            Table.render({
                elem: '#table',
                url: api.index,
                cols: [[
                    { field: 'id', title: 'ID', width: 80, align: 'center' },
                    { field: 'username', title: '用户名', minWidth: 120 },
                    { field: 'avatar', title: '头像', width: 80, align: 'center', templet: function (d) { return Table.format.avatar(d.avatar) } },
                    { field: 'status', title: '状态', width: 100, templet: function (d) { return Table.format.status(d.status) }, align: 'center' },
                    { field: 'login_ip', title: '登录IP', width: 140, align: 'center' },
                    { field: 'login_time', title: '最后登录时间', width: 168, align: 'center', templet: function (d) { return d.login_time ? Table.format.dateTime(d.login_time) : ''; }, sort: true },
                    { field: 'deadline', title: '过期时间', width: 168, align: 'center', templet: function (d) { return Table.format.date(d.deadline) }, sort: true },
                    // { field: 'update_time', title: '修改时间', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.update_time) }, sort: true },
                    { field: 'create_time', title: '添加时间', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.create_time) }, sort: true },
                    { field: 'operate', title: '操作', templet: '#operate', minWidth: 260 }
                ]]
            })
            // 绑定表格事件
            Table.bindEvent('table', api);
            // 初始化搜索表单
            Form.render();
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                Mon.api.open(api.add, '新增', $(this).data() || {});
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.login_time) {
                    var iLogin_time = data.login_time.split(' - ')
                    data.login_start_time = Mon.api.unixFormat(iLogin_time[0] + ' 00:00:00')
                    data.login_end_time = Mon.api.unixFormat(iLogin_time[1] + ' 23:59:59')
                } else {
                    data.login_start_time = ''
                    data.login_end_time = ''
                }
                Table.search('table', data)
            })
        },
        add: function () {
            Form.render();
            Form.submit('submit')
        },
        edit: function () {
            Form.render();
            Form.submit('submit')
        },
        password: function () {
            Form.render();
            Form.submit('submit')
        }
    };
    return Controller;
});
