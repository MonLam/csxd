define(['jquery', 'backend', 'table', 'form'], function ($, Backend, Table, Form) {

    // 参数配置
    var api = {
        "index": "/admin/sys/auth/rule",
        "add": "/admin/sys/auth/rule/add",
        "edit": "/admin/sys/auth/rule/edit",
        "publish": "/admin/sys/auth/rule/publish",
    }
    var Controller = {
        index: function () {
            Table.render({
                elem: '#table',
                url: api.index,
                page: false,
                cols: [[
                    { field: 'id', title: 'ID', width: 80, align: 'center' },
                    { field: 'title', title: '名称', minWidth: 240 },
                    { field: 'name', title: '路由规则', minWidth: 240 },
                    { field: 'status', title: '状态', width: 80, align: 'center', templet: Controller.api.formatter.status },
                    { field: 'id', align: 'center', title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle"><i class="fa fa-chevron-up"></i></a>', width: 60, templet: Controller.api.formatter.subnode },
                    { field: 'oper', title: '操作', templet: '#oper', minWidth: 240 }
                ]],
                done: function () {
                    // 隐藏二级节点
                    $(".btn-node-sub.disabled").closest("tr").hide();

                    //显示隐藏子节点
                    $(".btn-node-sub").off("click").on("click", function (e) {
                        var status = $(this).data("shown") ? true : false;
                        $("a.btn[data-pid='" + $(this).data("id") + "']").each(function () {
                            $(this).closest("tr").toggle(!status);
                        });
                        $(this).data("shown", !status);
                        return false;
                    });
                }
            });
            // 绑定表格事件
            Table.bindEvent('table', api);

            //展开隐藏一级
            $(document.body).on("click", ".btn-toggle", function (e) {
                $("a.btn[data-id][data-pid][data-pid!=0].disabled").closest("tr").hide();
                var that = this;
                var show = $("i", that).hasClass("fa-chevron-down");
                $("i", that).toggleClass("fa-chevron-down", !show);
                $("i", that).toggleClass("fa-chevron-up", show);
                $("a.btn[data-id][data-pid][data-pid!=0]").not('.disabled').closest("tr").toggle(show);
                $(".btn-node-sub[data-pid=0]").data("shown", show);
            });

            // 展开隐藏全部列表
            $(document.body).on("click", ".btn-toggle-all", function (e) {
                var that = this;
                var show = $("i", that).hasClass("fa-plus");
                $("i", that).toggleClass("fa-plus", !show);
                $("i", that).toggleClass("fa-minus", show);
                $(".btn-node-sub.disabled").closest("tr").toggle(show);
                $(".btn-node-sub").data("shown", show);
            });

            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                Mon.api.open(api.add, '新增', $(this).data() || {});
            })
            // 发布按钮
            $('#toolbar').on('click', '.btn-publish', function (e) {
                Layer.confirm('确认要发布路由规则么？', { icon: 3, title: '提示' }, function (index) {
                    Mon.api.ajax(api.publish, function () {
                        Layer.close(index)
                    }, function () {
                        Layer.close(index)
                    })
                })
            })
        },
        add: function () {
            // 渲染表单
            Form.render();
            // 提交表单
            Form.submit('add')
        },
        edit: function () {
            // 渲染表单
            Form.render();
            // 提交表单
            Form.submit('edit')
        },
        api: {
            formatter: {
                subnode: function (row) {
                    return '<a href="javascript:;" data-toggle="tooltip" title="切换菜单" data-id="' + row.id + '" data-pid="' + row.pid + '" class="btn btn-xs '
                        + (row.haschild == 1 || row.ismenu == 1 ? 'btn-success' : 'btn-default disabled') + ' btn-node-sub"><i class="fa fa-sitemap"></i></a>';
                },
                status: function (row) {
                    return Table.format.status(row.status)
                },
            }
        }
    };
    return Controller;
});