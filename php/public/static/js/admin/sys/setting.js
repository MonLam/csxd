define(['jquery', 'backend', 'table', 'layui', 'form'], function ($, Backend, Table, Layui, Form) {

    var api = {
        index: '/admin/sys/setting',
        add: '/admin/sys/setting/add',
        edit: '/admin/sys/setting/edit'
    }

    var Controller = {
        index: function () {
            // 渲染layui-tabs
            Layui.use('element', function (el) {
                // 切换
                el.on('tab(group)', function (data) {
                    var group = data.elem.context.dataset.group
                    Controller.api.renderTable(group)
                });
            });

            // 初始化渲染表格
            Controller.api.renderTable($('#groups li.layui-this').data('group'))
            // 绑定表格事件
            Table.bindEvent('table');
            // 表格单元格修改
            Table.callback(function (table) {
                table.on('edit(table)', function (obj) {
                    var data = obj.data
                    Mon.api.ajax({ url: api.edit, data: data }, function () {
                        table.reload('table');
                    })
                })
            })
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                var groupItem = $('#groups li.layui-this').data('group') || ''
                Mon.api.open(api.add + '?group=' + groupItem, '新增配置', {});
            })
        },
        add: function () {
            Form.render()
            Form.submit('submit', function (from, data, res, ret) {
                // 判断点击新增按钮所在的组别和实际新增的组别是否相同，不相同则提示是否立即刷新
                var groupItem = Mon.api.get('group');
                var sendGroup = data.field.group
                if (groupItem != sendGroup) {
                    Layer.confirm('当前新增的组别与选择的组别不同，是否立即刷新页面？', { icon: 3 }, function () {
                        // 确定，刷新页面
                        parent.window.location.reload();
                    }, function () {
                        // 取消，提示及关闭当前窗口
                        var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : '操作成功';
                        parent.Toastr.success(msg);
                        // 存在刷新按钮，且已绑定事件，则刷新上级table
                        parent.$(".btn-refresh").trigger("click");
                        var index = parent.Layer.getFrameIndex(window.name);
                        parent.Layer.close(index);
                    })
                    return false;
                }
            })
        },
        api: {
            // 渲染表格
            renderTable: function (group) {
                // 判断是否可编辑
                var edit = $('#table').data('edit') ? 'text' : false;
                Table.render({
                    elem: '#table',
                    url: api.index + '?group=' + group,
                    page: false,
                    cols: [[
                        { field: 'id', title: 'ID', width: 80, align: 'center' },
                        { field: 'group', title: '组别索引', width: 120, align: 'center' },
                        { field: 'index', title: '配置索引', width: 170, align: 'center' },
                        { field: 'value', title: '配置值', minWidth: 160, edit: edit, align: 'center' },
                        { field: 'remark', title: '备注', minWidth: 160, edit: edit },
                        { field: 'update_time', title: '修改时间', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.update_time) } },
                        { field: 'create_time', title: '添加时间', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.create_time) } },
                    ]],
                });
            }
        }
    };
    return Controller;
});