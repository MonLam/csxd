define(['jquery', 'backend'], function ($, Backend) {
    var api = {
        cache: '/admin/sys/clear/cache',
    }

    var Controller = {
        index: function () {
            // 清除文件缓存
            $(document).on('click', '.btn-clear-cache', function () {
                Layer.confirm('确定要清除文件缓存记录么？', { icon: 3 }, function (index) {
                    // 发起请求
                    Mon.api.ajax({url: api.cache}, function () {
                        // 成功则刷新表格，
                        Layer.close(index);
                    });
                })
            })
        },
    };
    return Controller;
});