define(['jquery', 'backend', 'table', 'form', 'iconSelect'], function ($, Backend, Table, Form, iconSelect) {

    // 参数配置
    var api = {
        "index": "/admin/sys/menu",
        "add": "/admin/sys/menu/add",
        "edit": "/admin/sys/menu/edit",
    }
    var Controller = {
        index: function () {
            Table.render({
                elem: '#table',
                url: api.index,
                page: false,
                cols: [[
                    {
                        field: 'id',
                        title: 'ID',
                        width: 80,
                        align: 'center'
                    },
                    {
                        field: 'title',
                        title: '菜单名称',
                        minWidth: 200,
                        templet: Controller.api.formatter.title
                    },
                    {
                        field: 'name',
                        title: '链接地址',
                        minWidth: 240
                    },
                    {
                        field: 'icon',
                        title: '图标',
                        width: 60,
                        align: 'center',
                        templet: Controller.api.formatter.icon
                    },
                    {
                        field: 'status',
                        title: '状态',
                        width: 80,
                        align: 'center',
                        templet: Controller.api.formatter.status
                    },
                    {
                        field: 'sort',
                        title: '权重',
                        width: 60,
                        align: 'center'
                    },
                    {
                        field: 'id',
                        align: 'center',
                        title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle"><i class="fa fa-chevron-up"></i></a>',
                        width: 60,
                        templet: Controller.api.formatter.subnode
                    },
                    {
                        field: 'oper',
                        title: '操作',
                        templet: '#oper',
                        minWidth: 200
                    }
                ]],
                done: function () {
                    // 隐藏二级节点
                    // $(".btn-node-sub.disabled").closest("tr").hide();

                    //显示隐藏子节点
                    // $(".btn-node-sub").off("click").on("click", function (e) {
                    //     var status = $(this).data("shown") ? true : false;
                    //     $("a.btn[data-pid='" + $(this).data("id") + "']").each(function () {
                    //         $(this).closest("tr").toggle(!status);
                    //     });
                    //     $(this).data("shown", !status);
                    //     return false;
                    // });
                }
            });
            // 绑定表格事件
            Table.bindEvent('table', api);

            // 绑定表格行双击，打开隐藏的后代菜单
            // Table.event.bindDBClick('table', function (table, obj) {
            //     if(obj.data.pid == '0'){
            //         $(obj.tr).find('.btn-node-sub').trigger('click')
            //     }
            // })

            //展开隐藏一级
            $(document.body).on("click", ".btn-toggle", function (e) {
                $("a.btn[data-id][data-pid][data-pid!=0].disabled").closest("tr").hide();
                var that = this;
                var show = $("i", that).hasClass("fa-chevron-down");
                $("i", that).toggleClass("fa-chevron-down", !show);
                $("i", that).toggleClass("fa-chevron-up", show);
                $("a.btn[data-id][data-pid][data-pid!=0]").not('.disabled').closest("tr").toggle(show);
                $(".btn-node-sub[data-pid=0]").data("shown", show);
            });

            // 展开隐藏全部列表
            $(document.body).on("click", ".btn-toggle-all", function (e) {
                var that = this;
                var show = $("i", that).hasClass("fa-plus");
                $("i", that).toggleClass("fa-plus", !show);
                $("i", that).toggleClass("fa-minus", show);
                $(".btn-node-sub.disabled").closest("tr").toggle(show);
                $(".btn-node-sub").data("shown", show);
            });

            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                Mon.api.open(api.add, '新增', $(this).data() || {});
            })
        },
        add: function () {
            Controller.api.bindevent();
            // 提交表单
            Form.submit('add', function (form, data, res, ret) {
                Backend.api.refreshmenu()
                //提示及关闭当前窗口
                var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : '操作成功';
                parent.Toastr.success(msg);
                var index = parent.Layer.getFrameIndex(window.name);
                parent.Layer.close(index);
                return false;
            })
        },
        edit: function () {
            Controller.api.bindevent();
            // 提交表单
            Form.submit('edit', function (form, data, res, ret) {
                Backend.api.refreshmenu()
                //提示及关闭当前窗口
                var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : '操作成功';
                parent.Toastr.success(msg);
                var index = parent.Layer.getFrameIndex(window.name);
                parent.Layer.close(index);
                return false;
            })
        },
        api: {
            formatter: {
                title: function (row) {
                    return row.title
                    // return (row.pid == '0' ? '<i class="fa fa-folder text-info"></i> ' : '') + row.title
                },
                icon: function (row) {
                    return Table.format.icon(row.icon)
                },
                subnode: function (row) {
                    return '<a href="javascript:;" data-toggle="tooltip" title="切换菜单" data-id="' + row.id + '" data-pid="' + row.pid + '" class="btn btn-xs ' +
                        (row.haschild == 1 ? 'btn-success' : 'btn-default disabled') + ' btn-node-sub"><i class="fa fa-sitemap"></i></a>';
                },
                status: function (row) {
                    return Table.format.status(row.status)
                },
            },
            bindevent: function () {
                // 绑定图标选择
                $(document).on('click', ".btn-search-icon", function () {
                    iconSelect.show();
                });
                // 绑定iconSelect选中回调事件
                iconSelect.callback(function (value, layid, layui) {
                    $("#icon").val(value);
                    Layer.close(layid);
                })
                // 渲染表单
                Form.render();
            },
        }
    };
    return Controller;
});