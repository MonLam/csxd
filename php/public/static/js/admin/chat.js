define(['jquery', 'backend', 'table', 'form', 'imgSelect', 'upload'], function ($, Backend, Table, Form, ImgSelect, Upload) {
    var api = {
        index: '/admin/chat/app',
        add: '/admin/chat/app/add',
        edit: '/admin/chat/app/edit',
        upload: '/admin/console/upload',
        custom: {
            // 客户留言
            stay: function (table, data) {
                var url = '/admin/chat/app/stay?app=' + data.name
                Mon.api.open(url, '访客留言');
            },
            // 应用客服
            waiter: function (table, data) {
                var url = '/admin/chat/waiter?app=' + data.id
                window.location.href = url
            },
            // 应用代码
            info: function (table, data) {
                var url = '/admin/chat/app/info?idx=' + data.id
                Mon.api.open(url, '应用代码')
            },
            // 历史信息
            history: function (table, data) {
                var url = '/admin/chat/history?app=' + data.name
                window.location.href = url
            },
            // 应用状态
            status: function (table, data) {
                var url = '/admin/chat/app/status?app=' + data.name
                Mon.api.open(url, '应用状态')
            }
        }
    }
    var Controller = {
        // 应用管理
        app: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: api.index,
                cols: [[
                    { title: 'ID', field: 'id', width: 80, align: 'center' },
                    { title: 'App标识', field: 'name', align: 'center', width: 300 },
                    { title: '名称', field: 'title', width: 180 },
                    { title: '客服席位数', field: 'seatnum', width: 120, align: 'center' },
                    { title: '席位接待数', field: 'receptnum', width: 120, align: 'center' },
                    { title: '状态', field: 'status', width: 100, templet: function (d) { return Table.format.status(d.status) }, align: 'center' },
                    { title: '更新时间', field: 'update_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.update_time) } },
                    { title: '操作', field: 'operate', templet: '#operate', minWidth: 520 }
                ]]
            })
            // 绑定表格事件
            Table.bindEvent('table', api);
            // 初始化搜索表单
            Form.render();
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                Mon.api.open(api.add, '新增', $(this).data() || {});
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.search_time) {
                    var iCreate_time = data.search_time.split(' - ')
                    data.start_time = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_time = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_time = ''
                    data.end_time = ''
                }
                Table.search('table', data)
            })
        },
        // 新增应用
        add: function () {
            Form.render();
            // 绑定事件
            Controller.event.app();
            Form.submit('submit')
        },
        // 编辑应用
        edit: function () {
            Form.render();
            // 绑定事件
            Controller.event.app();
            Form.submit('submit')
        },
        // 访客留言
        stay: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: window.location.href,
                cols: [[
                    { title: '留言时间', field: 'create_time', width: 120, align: 'center', templet: function (d) { return Table.format.date(d.create_time) } },
                    { title: '姓名', field: 'username', width: 88 },
                    { title: '手机号', field: 'moble', align: 'center', width: 120 },
                    { title: '邮箱', field: 'email', width: 180 },
                    { title: '留言信息', field: 'content' },

                ]]
            })
            // 初始化搜索表单
            Form.render();
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Table.reload('table');
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.search_time) {
                    var iCreate_time = data.search_time.split(' - ')
                    data.start_time = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_time = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_time = ''
                    data.end_time = ''
                }
                Table.search('table', data)
            })
        },
        // 应用客服
        waiter: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: window.location.href,
                cols: [[
                    { title: 'ID', field: 'id', width: 80, align: 'center' },
                    { title: '账号', field: 'account', align: 'center', width: 140 },
                    { title: '昵称', field: 'nickname', width: 140, align: 'center' },
                    { title: '状态', field: 'status', width: 100, templet: function (d) { return Table.format.status(d.status) }, align: 'center' },
                    { title: '最后登录IP', field: 'last_login_ip', width: 168, align: 'center' },
                    { title: '最后登录时间', field: 'last_login_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.last_login_time) } },
                    { title: '创建时间', field: 'create_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.update_time) } },
                    { title: '操作', field: 'operate', templet: '#operate', minWidth: 360 }
                ]]
            })

            // 初始化搜索表单
            Form.render();
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Table.reload('table');
            })
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                var url = '/admin/chat/waiter/add?app=' + $('#app_id').val()
                Mon.api.open(url, '新增');
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                Table.search('table', data)
            })
            // 返回
            $('#toolbar').on('click', '.btn-return', function () {
                window.location.href = api.index
            })
            // 编辑
            $(document).on('click', '.operate-edit', function () {
                var idx = $(this).data('idx');
                var url = '/admin/chat/waiter/edit?idx=' + idx
                Mon.api.open(url, '编辑');
            })
            // 历史记录
            $(document).on('click', '.operate-history', function () {
                var user_id = $(this).data('idx')
                var app_id = $('#app_id').val();
                var url = '/admin/chat/history?user_id=' + user_id + '&app_id=' + app_id
                window.location.href = url
            })
        },
        // 新增应用客服
        waiterAdd: function () {
            Form.render();
            Form.submit('submit')
        },
        // 修改应用客服
        waiterEdit: function () {
            Form.render();
            Form.submit('submit')
        },
        // 历史记录
        history: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: window.location.href,
                cols: [[
                    { title: '游客编号', field: 'visitid', width: 300 },
                    { title: '最后通话时间', field: 'near_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.near_time) } },
                    { title: '开始通话时间', field: 'create_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.create_time) } },
                    { title: '操作', field: 'operate', templet: '#operate', minWidth: 280 }
                ]]
            })
            // 初始化搜索表单
            Form.render();
            // 返回
            $('#toolbar').on('click', '.btn-return', function () {
                Mon.api.rollback();
            })
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Table.reload('table');
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.search_time) {
                    var iCreate_time = data.search_time.split(' - ')
                    data.start_near = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_near = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_near = ''
                    data.end_near = ''
                }
                Table.search('table', data)
            })
            // 查看游客信息
            $(document).on('click', '.operate-visit', function () {
                var visitid = $(this).data('idx')
                var url = '/admin/chat/visit?visitid=' + visitid
                Mon.api.open(url, '游客信息')
            })
            $(document).on('click', '.operate-info', function () {
                var idx = $(this).data('idx')
                var url = '/admin/chat/history/info?idx=' + idx
                Mon.api.open(url, '通话记录', { area: ['560px', '480px'] })
            })
        },
        // 通信记录
        historyInfo: function () {
            require(['emoji'], function (Emoji) {
                // 创建表情包库
                var parseEmoji = new Emoji({
                    wrapper: "#emoji",
                    path: "/static/libs/monEmoji/img/",
                })
                // 解析生成A链接
                var _parseLink = function (str) {
                    var reg = /\[monlink:([a-zA-z]+:\/\/[^\s]*=>?[^\s]*):monlink\]/g;
                    return str.replace(reg, function (str, target) {
                        var tempArr = target.split("=>");
                        var url = tempArr.shift();
                        var title = tempArr.pop();
                        title = title == "" ? url : title;
                        // &my_nbsp;为特殊的空格表示位，需要再重新转换多一次空格，该设置解决多个链接的情况下，正则匹配为1个链接
                        return '<a&my_nbsp;href="' + url + '"&my_nbsp;target="_blank">' + title + "</a>"
                    });
                }
                // 处理图片内容
                var _chatIMG = function (url, date, send) {
                    return '<div class="' + send + '"><div class="message-tips">' + date + '</div><img src="' + url + '" class="file-img" alt="图片加载失败"></div>';
                }
                // 处理文字信息
                var _chatTXT = function (str, date, send) {
                    // 替换a链接
                    str = _parseLink(str);
                    // 替换所有的换行符
                    str = str.replace(/\r\n/g, " <br> ");
                    str = str.replace(/\n/g, " <br> ");
                    // 替换所有的空格（中文空格、英文空格都会被替换）
                    str = str.replace(/\s/g, "&nbsp;");
                    // 替换a链接转义后特殊的空格符号，解决多个链接正则混淆匹配
                    str = str.replace(/&my_nbsp;/ig, ' ');
                    // 转换表情图片
                    str = parseEmoji.parse(str);
                    // 处理定位
                    var postion = send == 'ask' ? 'right' : 'left';
                    return '<div class="' + send + '">\
                                <div class="message-tips">'+ date + '</div>\
                                <div class="message">\
                                    <span class="arrow-' + postion + '-out"></span>\
                                    <span class="arrow-' + postion + '-in"></span>\
                                    ' + str + '\
                                </div>\
                            </div>';
                }
                var result = []
                var list = window.PageSetting.list
                for (var i in list) {
                    var item = list[i]
                    var date = Mon.api.dateFormat(item.create_time, 'm-d-H-i')
                    var content = JSON.parse(item.content)
                    let className = (content.from == window.PageSetting.userCode) ? "ask" : "reply";
                    switch (content.type) {
                        case "text":
                            result.push(_chatTXT(content.content, date, className));
                            break;
                        case "img":
                            result.push(_chatIMG(content.content, date, className));
                            break;
                    }
                }
                if (result.length > 0) {
                    result.push('<div class="clearfix"></div>')
                } else {
                    result.push('<h4 class="text-center">无通信记录</h4>')
                }
                // 写入Dom
                $('#chatMessage').html(result.join(''))
            })
        },
        // 游客信息
        visit: function () {
            Form.render();
            Form.submit('submit')
        },
        event: {
            app: function () {
                // 绑定图片选择
                $(document).on('click', ".btn-search-img", function () {
                    var mark = $(this).siblings('input').attr('id')
                    ImgSelect.show(null, mark);
                });
                // 绑定ImgSelect选中回调事件
                ImgSelect.callback(function (value, layid, layui) {
                    $('#' + value.mark).val(value.url)
                    Layer.close(layid);
                })
                // 图片上传
                Upload.render({
                    elem: '#img_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#img').val(url);
                    },
                });
                Upload.render({
                    elem: '#wechat_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#wechat').val(url);
                    },
                });
                Upload.render({
                    elem: '#qrcode_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#qrcode').val(url);
                    },
                });
            }
        }
    };
    return Controller;
});