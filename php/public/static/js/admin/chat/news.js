define(['jquery', 'backend', 'table', 'form', 'layui', 'imgSelect', 'upload'], function ($, Backend, Table, Form, Layui, ImgSelect, Upload) {
    var api = {
        list: '/admin/chat/news',
        add: '/admin/chat/news/add',
        edit: '/admin/chat/news/edit',
        bind: '/admin/chat/news/bind',
        upload: '/admin/console/upload',
    }
    var Controller = {
        // 应用客服
        index: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: api.list,
                cols: [[
                    { title: 'ID', field: 'id', width: 60, align: 'center' },
                    { title: '图片', field: 'img', width: 200, align: 'center', templet: function (d) { return Table.format.image(d.img, ' img-center') } },
                    { title: '标题', field: 'title', width: 200 },
                    { title: '跳转URL', field: 'link', width: 220, align: 'center', templet: function (d) { return Table.format.url(d.link) } },
                    { title: '描述', field: 'desc', width: 300 },
                    { title: '状态', field: 'status', width: 80, templet: function (d) { return Table.format.status(d.status) }, align: 'center' },
                    { title: '更新时间', field: 'update_time', width: 128, align: 'center', templet: function (d) { return Table.format.dateTime(d.update_time) } },
                    { title: '操作', field: 'operate', templet: '#operate', minWidth: 200 }
                ]]
            })

            // 初始化搜索表单
            Form.render();
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Table.reload('table');
            })
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                var url = api.add + '?app=' + $('#app_id').val()
                Mon.api.open(url, '新增');
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                Table.search('table', data)
            })
            // 返回
            $('#toolbar').on('click', '.btn-return', function () {
                window.location.href = api.index
            })
            // 编辑
            $(document).on('click', '.operate-edit', function () {
                var idx = $(this).data('idx');
                var url = api.edit + '?idx=' + idx
                Mon.api.open(url, '编辑');
            })
            // 关联APP
            $(document).on('click', '.operate-bind', function () {
                var idx = $(this).data('idx');
                var url = api.bind + '?idx=' + idx
                var area = [$(window).width() > 560 ? '542px' : '90%', $(window).height() > 540 ? '500px' : '90%'];
                Mon.api.open(url, '关联APP', { area: area });
            })
        },
        // 新增应用客服
        add: function () {
            Form.render();
            Controller.event.upload()
            Form.submit('submit')
        },
        // 修改应用客服
        edit: function () {
            Form.render();
            Controller.event.upload()
            Form.submit('submit')
        },
        // 关联APP
        bind: function () {
            // 渲染穿梭框
            Layui.use(['transfer'], function (transfer) {
                transfer.render({
                    elem: '#transfer',
                    id: 'transfer',
                    title: ['APP列表', '已关联'],
                    showSearch: true,
                    searchPlaceholder: '名称搜索',
                    data: window.transfer_data.apps,
                    value: window.transfer_data.selects,
                    text: {
                        none: '无数据',
                        searchNone: '无匹配数据'
                    }
                })
                // 提交，获取右侧数据
                $('#transfer-submit').on('click', function () {
                    var data = transfer.getData('transfer'); //获取右侧数据
                    var apps = []
                    for (var i = 0, l = data.length; i < l; i++) {
                        var item = data[i]
                        apps.push(item['value'])
                    }
                    apps = apps.join(',')
                    Mon.api.ajax({
                        url: api.bind + '?idx=' + window.transfer_data.idx,
                        data: {
                            apps: apps
                        }
                    }, function (res, ret) {
                        //提示及关闭当前窗口
                        var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : '操作成功';
                        parent.Toastr.success(msg);
                        // 存在刷新按钮，且已绑定事件，则刷新上级table
                        parent.$(".btn-refresh").trigger("click");
                        var index = parent.Layer.getFrameIndex(window.name);
                        parent.Layer.close(index);
                        return false;
                    })
                })
                // 重置
                $('#transfer-reset').on('click', function () {
                    transfer.reload('transfer', {
                        value: window.transfer_data.selects
                    });
                })
            })
        },
        event: {
            upload: function () {
                // 绑定图片选择
                $(document).on('click', ".btn-search-img", function () {
                    var mark = $(this).siblings('input').attr('id')
                    ImgSelect.show(null, mark);
                });
                // 绑定ImgSelect选中回调事件
                ImgSelect.callback(function (value, layid, layui) {
                    $('#' + value.mark).val(value.url)
                    Layer.close(layid);
                })
                // 图片上传
                Upload.render({
                    elem: '#img_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#img').val(url);
                    },
                });
            }
        }
    };
    return Controller;
});