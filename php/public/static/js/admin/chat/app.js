define(['jquery', 'backend', 'table', 'form', 'imgSelect', 'upload'], function ($, Backend, Table, Form, ImgSelect, Upload) {
    var api = {
        index: '/admin/chat/app',
        add: '/admin/chat/app/add',
        edit: '/admin/chat/app/edit',
        upload: '/admin/console/upload',
        custom: {
            // 应用代码
            info: function (table, data) {
                var url = '/admin/chat/app/info?idx=' + data.id
                Mon.api.open(url, '应用代码')
            },
            // 历史信息
            history: function (table, data) {
                var url = '/admin/chat/history?app=' + data.name
                window.location.href = url
            },
            // 应用状态
            status: function (table, data) {
                var url = '/admin/chat/app/status?app=' + data.name
                Mon.api.open(url, '应用状态')
            }
        }
    }
    var Controller = {
        // 应用管理
        index: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: api.index,
                cols: [[
                    { title: 'ID', field: 'id', width: 60, align: 'center' },
                    { title: 'App标识', field: 'name', align: 'center', width: 280 },
                    { title: 'Logo', field: 'logo', width: 90, align: 'center', templet: function (d) { return Table.format.image(d.logo, 'img-md img-center') } },
                    { title: 'APP名称', field: 'title', width: 180 },
                    { title: '官网URL', field: 'home', width: 220, align: 'center', templet: function (d) { return Table.format.url(d.home) } },
                    { title: '状态', field: 'status', width: 80, templet: function (d) { return Table.format.status(d.status) }, align: 'center' },
                    { title: '更新时间', field: 'update_time', width: 128, align: 'center', templet: function (d) { return Table.format.dateTime(d.update_time) } },
                    { title: '操作', field: 'operate', templet: '#operate', minWidth: 340 }
                ]]
            })
            // 绑定表格事件
            Table.bindEvent('table', api);
            // 初始化搜索表单
            Form.render();
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                Mon.api.open(api.add, '新增', $(this).data() || {});
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.search_time) {
                    var iCreate_time = data.search_time.split(' - ')
                    data.start_time = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_time = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_time = ''
                    data.end_time = ''
                }
                Table.search('table', data)
            })
        },
        // 新增应用
        add: function () {
            Form.render();
            // 绑定事件
            Controller.event.app();
            Form.submit('submit')
        },
        // 编辑应用
        edit: function () {
            Form.render();
            // 绑定事件
            Controller.event.app();
            Form.submit('submit')
        },
        event: {
            app: function () {
                // 绑定图片选择
                $(document).on('click', ".btn-search-img", function () {
                    var mark = $(this).siblings('input').attr('id')
                    ImgSelect.show(null, mark);
                });
                // 绑定ImgSelect选中回调事件
                ImgSelect.callback(function (value, layid, layui) {
                    $('#' + value.mark).val(value.url)
                    Layer.close(layid);
                })
                // 图片上传
                Upload.render({
                    elem: '#img_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#img').val(url);
                    },
                });
                Upload.render({
                    elem: '#wechat_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#wechat').val(url);
                    },
                });
                Upload.render({
                    elem: '#qrcode_upload',
                    url: api.upload,
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#qrcode').val(url);
                    },
                });
            }
        }
    };
    return Controller;
});