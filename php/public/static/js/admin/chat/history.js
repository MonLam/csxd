define(['jquery', 'backend', 'table', 'form'], function ($, Backend, Table, Form) {
    var api = {
        list: '/admin/chat/history',
        info: '/admin/chat/history/info',
        visit: '/admin/chat/app/visit'
    }
    var Controller = {
        // 历史记录
        index: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: window.location.href,
                cols: [[
                    { title: 'APP名称', field: 'title', width: 180 },
                    { title: '开始通话时间', field: 'create_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.create_time) } },
                    { title: '最后通话时间', field: 'near_time', width: 168, align: 'center', templet: function (d) { return Table.format.dateTime(d.near_time) } },
                    { title: '访客IP', field: 'ip', align: 'center', width: 148 },
                    { title: '访客编号', field: 'visitor_id', width: 100, align: 'center' },
                    { title: '接待客服', field: 'account', align: 'center', width: 148 },
                    { title: '备注信息', field: 'remark', width: 200 },
                    { title: '操作', field: 'operate', templet: '#operate', minWidth: 240 }
                ]]
            })
            // 初始化搜索表单
            Form.render();
            // 返回
            $('#toolbar').on('click', '.btn-return', function () {
                Mon.api.rollback();
            })
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Table.reload('table');
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.search_time) {
                    var iCreate_time = data.search_time.split(' - ')
                    data.start_near = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_near = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_near = ''
                    data.end_near = ''
                }
                Table.search('table', data)
            })
            // 查看游客信息
            $(document).on('click', '.operate-visit', function () {
                var visitid = $(this).data('idx')
                var url = api.visit + '?visitid=' + visitid
                Mon.api.open(url, '游客信息')
            })
            $(document).on('click', '.operate-info', function () {
                var idx = $(this).data('idx')
                var url = api.info + '?idx=' + idx
                Mon.api.open(url, '通话记录', { area: ['560px', '480px'] })
            })
        },
        // 聊天记录
        info: function () {
            require(['chatMessage'], function (ChatMessage) {
                var sdk = ChatMessage.setPath('/static/libs/monEmoji/img/')
                var result = []
                var list = window.PageSetting.list
                for (var i in list) {
                    var item = list[i]
                    var date = Mon.api.dateFormat(item.create_time, 'm-d-H-i')
                    var message = JSON.parse(item.content)
                    var className = (message.from == window.PageSetting.userCode) ? "ask" : "reply";
                    result.push(sdk.parse(message.type, message, date, className));
                }
                if (result.length > 0) {
                    result.push('<div class="clearfix"></div>')
                } else {
                    result.push('<h4 class="text-center">无通信记录</h4>')
                }
                // 写入Dom
                $('#chatMessage').html(result.join(''))
            })
        },
    };
    return Controller;
});