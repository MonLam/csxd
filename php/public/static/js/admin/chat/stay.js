define(['jquery', 'backend', 'table', 'form'], function ($, Backend, Table, Form) {
    var api = {
        list: '/admin/chat/stay',
    }
    var Controller = {
        // 访客留言
        index: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: api.list,
                cols: [[
                    { title: 'APP名称', field: 'title', width: 180 },
                    { title: 'IP', field: 'ip', width: 140, align: 'center', },
                    { title: '留言时间', field: 'create_time', width: 120, align: 'center', templet: function (d) { return Table.format.date(d.create_time) } },
                    { title: '姓名', field: 'username', width: 88 },
                    { title: '手机号', field: 'moble', align: 'center', width: 120 },
                    { title: '邮箱', field: 'email', width: 180 },
                    { title: '留言信息', field: 'content' },

                ]]
            })
            // 初始化搜索表单
            Form.render();
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Table.reload('table');
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.search_time) {
                    var iCreate_time = data.search_time.split(' - ')
                    data.start_time = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_time = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_time = ''
                    data.end_time = ''
                }
                Table.search('table', data)
            })
        },
    };
    return Controller;
});