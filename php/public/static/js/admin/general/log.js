define(['jquery', 'backend', 'table', 'form'], function ($, Backend, Table, Form) {
    var api = {
        admin: '/admin/general/log/admin',
    }
    var Controller = {
        // 管理员日志
        admin: function () {
            // 渲染表格
            Table.render({
                elem: '#table',
                url: api.admin,
                cols: [[
                    { title: 'ID', field: 'id', width: 80, align: 'center' },
                    {
                        title: '管理员', field: 'username', width: 120, align: 'center', templet: function (d) {
                            return d.username ? d.username : (d.uid === 0) ? '系统指令' : null;
                        }
                    },
                    { title: '类型', field: 'method', align: 'center', width: 80 },
                    { title: '路径', field: 'path', width: 200 },
                    { title: '操作', field: 'action', width: 160, align: 'center' },
                    { title: '内容', field: 'content', minWidth: 240, },
                    { title: 'IP', field: 'ip', width: 136, align: 'center' },
                    {
                        title: '操作时间', field: 'create_time', width: 168, align: 'center', templet: function (d) {
                            return Table.format.dateTime(d.create_time)
                        },
                    },
                ]]
            })
            // 绑定表格事件
            Table.bindEvent('table', api);
            // 初始化搜索表单
            Form.render();
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                var data = Form.val('search')
                if (data.create_time) {
                    var iCreate_time = data.create_time.split(' - ')
                    data.start_time = Mon.api.unixFormat(iCreate_time[0] + ' 00:00:00')
                    data.end_time = Mon.api.unixFormat(iCreate_time[1] + ' 23:59:59')
                } else {
                    data.start_time = ''
                    data.end_time = ''
                }
                Table.search('table', data)
            })
        },
        format: {
            status: function (value) {
                value = typeof value != 'object' ? value : value.status
                var display = value == 1 ? '成功' : value == 2 ? '失败' : '未知';
                return value == '1' ? '<i class="fa fa-check text-success" style="font-size: 14px"> ' + display + '</i>' : '<i class="fa fa-close text-danger" style="font-size: 14px"> ' + display + '</i>'
            },
        }
    };
    return Controller;
});