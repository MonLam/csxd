define(['jquery', 'form', 'layui', 'upload'], function ($, Form, Layui, Upload) {

    var api = {
        index: '/admin/general/assets',
        add: '/admin/general/assets/add',
        edit: '/admin/general/assets/edit',
        upload: '/admin/console/upload',
        config: {
            page: 1,
            limit: 12,
        }
    }

    var Controller = {
        index: function () {
            // 初始化搜索表单
            Form.render();
            // 新增按钮
            $('#toolbar').on('click', '.btn-add', function (e) {
                Mon.api.open(api.add, '新增', $(this).data() || {});
            })
            // 搜索按钮
            $('#toolbar').on('click', '.btn-search', function (e) {
                Controller.api.reload(true)
            })
            // 刷新
            $('#toolbar').on('click', '.btn-refresh', function (e) {
                Controller.api.reload(false)
            })
            // 修改按钮
            $('#card-list').on('click', '.btn-edit', function (e) {
                Mon.api.open(api.edit + '?idx=' + $(this).data('idx'), '编辑', $(this).data() || {})
            })

            // 渲染列表
            Controller.api.renderList({ page: api.config.page, limit: api.config.limit }, true);
        },
        add: function () {
            Controller.api.bindevent()
        },
        edit: function () {
            Controller.api.bindevent()
        },
        api: {
            bindevent: function () {
                Form.render();
                Form.submit('submit');

                Upload.render({
                    elem: '#upload',
                    url: api.upload,
                    data: {},
                    done: function (ret, index, upload) {
                        if (ret.code != '1') {
                            Toastr.error(ret.msg);
                            return;
                        }
                        var url = ret.data.url
                        $('#url').val(url);
                    },
                })
            },
            // 渲染资源列表
            renderList: function (data, renderPage) {
                // 渲染分页
                Layui.use(['laypage', 'laytpl'], function (Laypage, Laytpl) {
                    var index;
                    $.ajax({
                        url: api.index,
                        data: data,
                        type: 'get',
                        dataType: 'json',
                        beforeSend: function () {
                            index = Layer.load(2);
                        },
                        complete: function () {
                            index && Layer.close(index);
                        },
                        success: function (ret) {
                            if (ret.code == '1') {
                                // 操作成功
                                var tpl = document.getElementById('cart-item').innerHTML, view = document.getElementById('card-list');
                                // 没有数据
                                if (ret.count < 1) {
                                    view.innerHTML = '<div class="text-center not-data">暂无数据...</div>'
                                } else {
                                    Laytpl(tpl).render(ret.data, function (html) {
                                        view.innerHTML = html;
                                    });
                                }

                                // 是否渲染分页
                                if (renderPage) {
                                    Laypage.render({
                                        elem: 'page',
                                        count: ret.count,
                                        limit: api.config.limit,
                                        limits: [12, 24, 48],
                                        jump: function (obj, first) {
                                            //首次不执行
                                            if (!first) {
                                                // 需要补充搜索内容
                                                query = {
                                                    page: obj.curr,
                                                    limit: obj.limit
                                                }
                                                Controller.api.renderList(query, false)
                                            }
                                        }
                                    })
                                }
                            }
                        },
                        error: function (err) {
                            // 请求失败
                            Toastr.error(err.statusText);
                        }
                    })
                })
            },
            // 刷新列表
            reload: function (search) {
                var data = Form.val('search')
                if (data.update_time) {
                    var iUpdate_time = data.update_time.split(' - ')
                    data.start_time = Mon.api.unixFormat(iUpdate_time[0] + ' 00:00:00')
                    data.end_time = Mon.api.unixFormat(iUpdate_time[1] + ' 23:59:59')
                } else {
                    data.start_time = ''
                    data.end_time = ''
                }
                data.page = search ? 1 : $('#page .layui-laypage-curr em:not([class])').text();
                data.limit = api.config.limit;
                Controller.api.renderList(data, true)
            }
        }
    };
    return Controller;
});