define(['jquery', 'editormd', 'editormd-link', 'editormd-image', 'editormd-table', 'editormd-goto'], function ($, editormd) {
    var editor = {
        // 实例缓存
        instance: {},
        // 配置信息
        config: {
            // 图片上传，默认关闭 {success : '0 表示上传失败，1 表示上传成功', message: "提示的信息，上传成功或上传失败及错误信息等。", url: "图片地址"}
            imageUpload: false,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "/upload.php",
            height: 640,
            path: '/static/libs/editor-md/lib/',
            toolbarAutoFixed: false,
            toolbarIcons: function () {
                return [
                    "undo", "redo", "|",
                    "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|",
                    "h1", "h2", "h3", "h4", "h5", "h6", "|",
                    "list-ul", "list-ol", "hr", "|",
                    "link", "image", "table", "datetime", "pagebreak", "|",
                    "goto-line", "watch", "preview", "fullscreen", "clear", "|",
                    "info"
                ]
            },
        },
        // 初始化定义配置
        init: function (config) {
            this.config = $.extend(this.config, config)
            return this
        },
        // 创建渲染md编辑器
        render: function (id, config) {
            if (typeof id != 'string' || id == '') {
                console.error('mon-editmd hint：请输入容器ID');
                return false
            }
            // 获取配置
            var setting = {}
            if (typeof config == 'object') {
                setting = $.extend(this.config, config)
            } else {
                setting = this.config
            }
            // 创建实例
            this.instance[id] = editormd(id, setting);
        },
        // 获取实例
        getInstance: function (id) {
            return this.instance[id];
        },
        // 获取editor.md
        getEditor: function () {
            return editormd
        },
        // 获取MarkDown内容 
        getMarkdown: function (id) {
            return this.getInstance(id).getMarkdown();
        },
        // 获取HTML内容
        getHTML: function (id) {
            return this.getInstance(id).getHTML();
        }
    }

    return editor;
})