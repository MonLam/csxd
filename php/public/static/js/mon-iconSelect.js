define(['jquery', 'layui'], function ($, Layui) {
    // 模板
    var tpl = '<div id="chooseicon">\
                    <div>\
                        <form onsubmit="return false;">\
                            <div class="input-group input-groupp-md">\
                                <div class="input-group-addon">搜索图标</div>\
                                <input class="js-icon-search form-control" type="text" placeholder="">\
                            </div>\
                        </form>\
                    </div>\
                    <div>\
                        <ul class="list-inline">\
                            {{# for(var i = 0; i < d.iconList.length; i++){ }}\
                            <li data-font="{{=d.iconList[i]}}" data-toggle="tooltip" title="{{=d.iconList[i]}}">\
                                <i class="{{=d.iconList[i]}}"></i>\
                            </li>\
                            {{# } }}\
                        </ul>\
                    </div>\
                </div>';

    // 获取图标列表API
    var defaultAPI = Config.site.cdnurl + "/static/libs/font-awesome/variables.less"
    // 实例
    var IconSelect = {
        // 图标列表
        iconList: [],
        // 弹窗ID
        LayerID: null,
        // 绑定事件标志位
        isBind: false,
        // 选中图标时触发回调
        onSelect: null,
        // 显示icon选择
        show: function (data, notDefault, reset) {
            // 是否重新获取
            if (reset) {
                IconSelect.iconList = []
            }
            // 获取数据，渲染弹窗
            if (IconSelect.iconList.length == 0) {
                // 自定义增加的图标
                if (typeof data == 'object' || typeof data == 'array') {
                    for (var i = 0, j = data.length; i < j; i++) {
                        IconSelect.iconList.push(data[i]);
                    }
                }
                // 不需要默认数据，直接渲染
                if (notDefault) {
                    return IconSelect.render();
                }

                // 获取系统自带的fa图标
                var url = defaultAPI
                $.get(url, function (ret) {
                    var exp = /fa-var-(.*):/ig;
                    var result;
                    // 默认的解析less变量
                    while ((result = exp.exec(ret)) != null) {
                        IconSelect.iconList.push('fa fa-' + result[1]);
                    }

                    IconSelect.render();
                });
            } else {
                IconSelect.render();
            }
        },
        // 渲染模板
        render: function () {
            Layui.use('laytpl', function (Template) {
                Template(tpl).render({ iconList: IconSelect.iconList }, function (string) {
                    IconSelect.LayerID = Layer.open({
                        type: 1,
                        area: ['90%', '90%'],
                        scrollbar: false,
                        content: string
                    });
                });

                // 绑定事件
                IconSelect.bindEvent();
            })
        },
        // 绑定事件
        bindEvent: function () {
            if (!IconSelect.isBind) {
                // 图标选中回调
                $(document).on('click', '#chooseicon ul li', function () {
                    var value = $(this).data('font');
                    if (typeof IconSelect.onSelect == 'function') {
                        // 回传选中的icon值、layer弹窗ID，Layui实例
                        IconSelect.onSelect.call(this, value, IconSelect.LayerID, Layui)
                    }
                });

                // 搜索图标
                $(document).on('keyup', '#chooseicon input.js-icon-search', function () {
                    $("#chooseicon ul li").show();
                    if ($(this).val() != '') {
                        $("#chooseicon ul li:not([data-font*='" + $(this).val() + "'])").hide();
                    }
                });
            }
        },
        // 选中的icon后的回调绑定
        callback: function (callback) {
            if (typeof callback == 'function') {
                IconSelect.onSelect = callback
            } else {
                console.error('mon-iconSelect: icon selected callback not is function!');
            }
        }
    }

    return IconSelect
});