/**
 * jquery右键菜单组件
 * 
 * @author Mon
 * @version 1.0.0
 */
; (function (factory) {
    if (typeof define === "function" && define.amd) {
        define(factory);
    } else {
        window.monContextMenu = factory();
    }
})(function () {
    var operEl = null;
    var html = "";
    var that = null;
    var monContextMenu = {
        init: function (option) {
            if (typeof (option) != "object" || !option.hasOwnProperty('id') || !option.hasOwnProperty('el') || !option.hasOwnProperty('items') || !option.hasOwnProperty('callback')) {
                throw "json format faild!";
            }
            that = this
            this.showMenu(option);

            var rmMain = $('#' + option.id + ' .rm-main')
            var rmcontainer = $('#' + option.id + ' .rm-container')
            var areaHeight = $('body').height();
            var areaWidth = $('body').width();
            var menuHeight = rmMain.height();
            var menuWidth = rmMain.width();
            $(document).on('contextmenu', option.el, function (event) {
                // 先隐藏所有再显示，防止多个右键事件有冲突
                $('.rm-main').hide()

                operEl = $(this)
                var xPos = parseInt(event.pageX + 2);
                var yPos = event.pageY;
                if (areaWidth - xPos < menuWidth) {
                    xPos = (xPos - menuWidth - 20);
                    rmcontainer.css({
                        left: (xPos - menuWidth - 20) + "px",
                        // top: yPos + "px"
                    }).show();
                    $('#' + option.id + ' .rm-child').hide();
                }
                if (areaHeight - yPos < (menuHeight + 10)) {
                    yPos = (yPos - menuHeight - 10);
                }

                if (areaWidth - xPos < ((menuWidth * 2) + 10)) {
                    rmMain.attr('data-direction', 'left')
                } else {
                    rmMain.attr('data-direction', 'right')
                }

                rmMain.css({
                    left: xPos + "px",
                    top: yPos + "px"
                }).show();
                return false;
            })
            $('body').on('click', function () {
                rmcontainer.hide();
            });
            $('#' + option.id + ' .rm-container li').on('click', function (event) {
                event.stopPropagation();
                var content = $(this).data('content');
                rmcontainer.hide();
                option.callback({
                    type: content,
                    elem: operEl
                });
            });
            $('#' + option.id + ' .rm-container ul li,' + '#' + option.id + ' .rm-child li').mouseover(function (e) {
                if ($(this).find('i').hasClass('fa-icon-align-right')) {
                    var width = $(this).find('i').next('.rm-child').width();
                    if (rmMain.attr('data-direction') == 'left') {
                        $(this).find('i').next('.rm-child').css('right', width).css('left', 'auto').show();
                    } else {
                        $(this).find('i').next('.rm-child').css('left', width).css('right', 'auto').show();
                    }
                }
            });
            $('#' + option.id + ' .rm-container ul li,' + '#' + option.id + ' .rm-child li').mouseout(function () {
                $('#' + option.id + ' .rm-child').hide();
            });
        },
        contextMenu: function (option, key) {
            var key = key ? key : "main";
            html += '<div class="rm-container rm-' + key + '"><ul>';
            $.each(option.items, function (item, val) {
                var icon = val.icon ? '<i class="fa fa-' + val.icon + ' fa-fw ">&nbsp;' : '&nbsp;&nbsp;&nbsp;&nbsp;'
                var iconAfter = val.items ? '<i class="fa fa-chevron-right fa-fw fa-icon-align-right">&nbsp;' : '';
                html += '<li data-content=' + item + '>' + icon + '</i>' + val.name + iconAfter + '</i>';
                if (val.hasOwnProperty('items')) {
                    that.contextMenu(val, 'child');
                }
                html += '</li>';
            });
            html += "</ul></div>";
            return html;
        },
        showMenu: function (option) {
            var html = this.contextMenu(option);
            var id = option.id;
            $('body').append('<div id="' + id + '">' + html + '</div>');
            $('#' + id + ' .rm-container').hide();
        }
    };

    return monContextMenu;
});