# 表情包组件

## 使用方法：

1. 引入`mon-emoji.js`、`mon-emoji.css`
2. 将img目录移动至web目录(可自行使用表情包img)。
3. 调用实例方法：

```html

<!-- 表情包选择框，id可自定义 -->
<div id="emoji"></div>

<script>
// 加载表情包
var emoji = new monEmoji({
    // 容器
    wrapper: "#emoji",
    // 表情包访问根路径
    path: './monEmoji/img/',
    // 每列显示表情数
    col: 7,
    // 自定义表情，不填写默认使用img目录中的表情包
    emojis: [
        {title: '名称', url: '路径'}
    ],
    // 选择表情包回调
    callback: function (emojiCode) {
        document.getElementById('emoji').style.display = 'none'
        console.log(emojiCode)
        // 解析表情包未img标签
        var img = emoji.parse(emojiCode)
        console.log(img)
    },
});
// 选择表情
document.querySelector('#emoji-btn').onclick = function (e) {
    var el = document.getElementById('emoji')
    if (el.ownerDocument.defaultView.getComputedStyle(el, null).display === 'none') {
        el.style.display = '' || 'block';
    } else {
        el.style.display = 'none';
    }
}
// 绑定closet
if (!Element.prototype.closest) {
    Element.prototype.closest = function (el, selector) {
        const matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;
        while (el) {
            if (matchesSelector.call(el, selector)) {
                return el;
            } else {
                el = el.parentElement;
            }
        }
        return null;
    }
}
document.onclick = function (e) {
    if (e.target.closest('.mEmoji-content') == null && e.target.closest('#emoji-btn') == null) {
        document.getElementById('emoji').style.display = 'none'
    }
}
</script>
```