; (function (window, document) {
    var monEmoji = function (options) {
        options = options || {};
        // 容器，默认body
        var selector = options.wrapper || "body";
        // 包裹元素
        this.wrapper = document.querySelector(selector);
        // 每行表情的个数
        this.col = options.col || 8;
        // 当表情被点击时的回调，返回表情的code值
        this.callback = options.callback || function () { };
        this.path = options.path || ''
        // 加载表情包配置
        var default_emjis = [
            { title: '微笑', url: '1.gif' },
            { title: '撇嘴', url: '2.gif' },
            { title: '色', url: '3.gif' },
            { title: '发呆', url: '4.gif' },
            { title: '流泪', url: '5.gif' },
            { title: '害羞', url: '6.gif' },
            { title: '闭嘴', url: '7.gif' },
            { title: '睡觉', url: '8.gif' },
            { title: '可怜', url: '9.gif' },
            { title: '尴尬', url: '10.gif' },
            { title: '生气', url: '11.gif' },
            { title: '调皮', url: '12.gif' },
            { title: '呲牙', url: '13.gif' },
            { title: '惊讶', url: '14.gif' },
            { title: '难过', url: '15.gif' },
            { title: '冷汗', url: '16.gif' },
            { title: '抓狂', url: '17.gif' },
            { title: '呕吐', url: '18.gif' },
            { title: '偷笑', url: '19.gif' },
            { title: '可爱', url: '20.gif' },
            { title: '白眼', url: '21.gif' },
            { title: '傲慢', url: '22.gif' },
            { title: '饥饿', url: '23.gif' },
            { title: '困', url: '24.gif' },
            { title: '惊恐', url: '25.gif' },
            { title: '流汗', url: '26.gif' },
            { title: '大笑', url: '27.gif' },
        ];
        this.emojis = options.emojis || default_emjis;
        // .mEmoji-content
        this.content = null;
        this.wrapWidth = 0;
        this.count = this.emojis.length;

        this.init();
    }

    monEmoji.prototype = {
        constructor: this,
        // 初始化
        init: function () {
            this.wrapWidth = this.wrapper.clientWidth - 12
            var temp = '<div class="mEmoji-content"></div>'
            this.wrapper.innerHTML = temp
            this.content = this.wrapper.querySelector('.mEmoji-content')

            this._initData()
            this._initEvent()
        },
        // 载入数据
        _initData: function () {
            var content = this.content,
                emojis = this.emojis,
                count = this.count,
                col = this.col,
                path = this.path,
                wrapWidth = this.wrapWidth

            //减少重排
            content.style.display = 'none';
            var temp = document.createElement("div"), tpl = ''
            temp.className = "mEmoji-column";
            temp.style.width = wrapWidth + "px";
            for (var i = 0; i < count; i++) {
                var item = emojis[i];
                var isObj = (typeof item == "object");
                // 如果数据类型是对象,则按照对象格式进行读取
                url = isObj ? item.url : item;
                title = isObj ? item.title : "";
                if (!url) {
                    continue;
                }
                // 处理加密名称
                var imgArr = url.split("."), type = imgArr.pop();
                // 自动生成code,方便后面的自动解析
                var code = '[mon:' + (title ? title : i) + "_" + imgArr.join(".") + "_" + type + ':mon]';
                tpl += '<div class="mEmoji-item" data-emj="' + code + '" style="width:' + (100 / col) + '%;">' +
                    '<img src="' + path + url + '" title="' + title + '" />' + '</div>';
            }

            temp.innerHTML = tpl
            content.appendChild(temp)
            // 重新显示
            content.style.display = 'block'
        },
        // 绑定事件
        _initEvent: function () {
            var _self = this, content = this.content
            // 绑定表情选择事件
            content.onclick = function (e) {
                e = e || event;
                var target = e.target || e.srcElement,
                    trueTarget = getTargetNode(target, ".mEmoji-item"),
                    emjCode;

                if (trueTarget) {
                    emjCode = trueTarget.getAttribute("data-emj");
                }

                if (!emjCode) {
                    return false;
                }

                _self.callback.call(_self, emjCode);
            };
        },
        // 解析表情包
        parse: function (str) {
            // var reg = /\[mon:(\w+):mon\]/g,
            var reg = /\[mon:([a-zA-Z0-9_\u4e00-\u9fa5]+):mon\]/g,
                _self = this;

            return str.replace(reg, function (str, target) {
                var tempArr = target.split("_"),
                    title = tempArr.shift(),
                    type = tempArr.pop(),
                    name = tempArr.join("_");
                var path = _self.path;
                var url = name + "." + type;

                return '<img src="' + path + url + '" alt="' + title + '" />';
            });

        }
    }

    // 获取目标元素
    var getTargetNode = function (ele, selector) {
        var type = getSelectorType(selector),
            nSec = selector.replace(/.|#/, "");
        if (ele === document) {
            return null;
        }

        if (new RegExp(nSec).test(ele[type])) {
            return ele;
        } else {
            return getTargetNode(ele.parentNode, selector);
        }
    };

    // 判断selector类型
    var getSelectorType = (function () {
        var checkIdName = function (selector) {
            var idReg = new RegExp("#");
            return (idReg.test(selector) ? "id" : checkClassName(selector));
        };
        var checkClassName = function (selector) {
            var classReg = new RegExp(".");
            return (classReg.test(selector) ? "className" : "tagName");
        };
        return function (selector) {
            return checkIdName(selector);
        };
    })();

    if (typeof module != "undefined" && module.exports) {
        module.exports = monEmoji;
    } else if (typeof define == "function" && define.amd) {
        define(function () { return monEmoji; });
    } else {
        !('monEmoji' in window) && (window.monEmoji = monEmoji);
    }

})(window, document)