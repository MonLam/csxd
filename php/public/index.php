<?php

/**
 * 创建APP应用实例
 */
$app = require __DIR__ . '/../bootstrap/app.php';

if (!file_exists(STORAGE_PATH . '/install.lock')) {
    // 未安装，执行安装程序
    return $app->run('get', '/install')->send();
}

/**
 * 执行应用, 输出响应结果集
 */
$app->run()->send();
