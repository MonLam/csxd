<?php

/*
|--------------------------------------------------------------------------
| 后台管理端配置文件
|--------------------------------------------------------------------------
| 定义管理端配置信息
|
*/
return [
    // 管理员session-key
    'admin_session_key' => 'adminUser',
    // 管理端打开默认页面
    'admin_fixed_page'  => '/admin/console/desktop',
    // 不允许修改组别ID
    'supper_group'      => [1],
    // 不允许修改管理员ID
    'supper_admin'      => [1],
    // 过期天数距离多少天提示
    'deadline_tips'     => 3,
    // 页面配置
    'site'              => [
        // 系统名称
        'title'     => 'CSXD客服系统',
        // cdn
        'cdnurl'    => '',
        // js版本，发布版本请定义写死的值
        'version'   => RUN_MODE != 'dev' ? '20210324' : time(),
    ],
    // 开启多级导航菜单
    'multiplenav'       => false,
    // 管理端登录验证码key
    'login_captcha_id'  => 'admin_login',
    // 开启单点登录
    'sign_in'           => true,
    // 开启单点登录后，仍可以多点登录的用户ID
    'sign_in_filter'    => [],
    // 文件管理系统配置
    'fileSys'           => [
        // 启用文件管理系统
        'enable'    => true,
        // 文件管理根路径，只允许操作该路径下文件目录
        'root'      => ROOT_PATH . '/public',
        // 资源查看根路径，对应文件管理根路径的请求地址
        'cdn'       => '',
        // 允许上传最大尺寸, 10M
        'maxSize'   => 10000000,
        // 允许上传文件类型
        'exts'      => ['jpg', 'jpeg', 'png', 'gif', 'mp4', 'avi', 'mkv', 'flv', 'rmvb', 'pdf', 'md'],
    ],
    // 数据备份
    'migrate'       => [
        // 数据库备份路径
        'path'      => STORAGE_PATH . '/data/',
        // 数据库备份卷大小
        'part'      => 20971520,
        // 数据库备份文件是否启用压缩 0不压缩 1 压缩
        'compress'  => 1,
        // 压缩级别
        'level'     => 5,
    ],
    // RequireJS配置管理
    'assets'            => [
        // 资源根路径
        'baseUrl'   => '/static/',
        // 资源路径映射
        'paths'     => [
            // require-plugins
            'async'                 => 'libs/requirejs-plugins/src/async.js?v=20201207',
            'font'                  => 'libs/requirejs-plugins/src/font.js?v=20201207',
            'depend'                => 'libs/requirejs-plugins/src/depend.js?v=20201207',
            'image'                 => 'libs/requirejs-plugins/src/image.js?v=20201207',
            'json'                  => 'libs/requirejs-plugins/src/json.js?v=20201207',
            'noext'                 => 'libs/requirejs-plugins/src/noext.js?v=20201207',
            'mdown'                 => 'libs/requirejs-plugins/src/mdown.js?v=20201207',
            'propertyParser'        => 'libs/requirejs-plugins/src/propertyParser.js?v=20201207',
            'markdownConverter'     => 'libs/requirejs-plugins/lib/Markdown.Converter.js?v=20201207',
            'text'                  => 'libs/requirejs-plugins/lib/textr.js?v=20201207',
            // system dependence
            'mon'                   => 'js/mon.js?v=20210303',
            'backend'               => 'js/backend.js?v=20201204',
            'table'                 => 'js/mon-table.js?v=20201230',
            'form'                  => 'js/mon-form.js?v=20210108',
            'iconSelect'            => 'js/mon-iconSelect.js?v=20201204',
            'imgSelect'             => 'js/mon-imgSelect.js?v=20210316',
            'linkSelect'            => 'js/mon-linkSelect.js?v=20201231',
            'monEditMD'             => 'js/mon-editmd.js?v=20201204',
            'monTinymce'            => 'js/mon-tinymce.js?v=20201204',
            'upload'                => 'js/mon-upload.js?v=20201204',
            'adminlte'              => 'js/adminlte.js?v=20201204',
            'jquery'                => 'libs/jquery/jquery.min.js?v=20201204',
            'bootstrap'             => 'libs/bootstrap/js/bootstrap.min.js?v=20201204',
            'layui'                 => 'libs/layui/layui.js?v=20210406',
            'addtabs'               => 'libs/addtabs/jquery.addtabs.js?v=20201204',
            'jstree'                => 'libs/jstree/jstree.min.js?v=20201204',
            'toastr'                => 'libs/toastr/toastr.min.js?v=20201204',
            'slimscroll'            => 'libs/jquery-slimscroll/jquery.slimscroll.min.js?v=20201204',
            'xmSelect'              => 'libs/xm-select/xm-select.js?v=20201204',
            // 右键菜单
            'monContextMenu'        => 'libs/monContextMenu/monContextMenu.js?v=20201207',
            // 右侧小导航栏
            'monNav'                => 'libs/monNav/monNav.js?v=20201207',
            // markdown
            'marked'                => "libs/editor-md/lib/marked.min.js?v=20201204",
            'prettify'              => "libs/editor-md/lib/prettify.min.js?v=20201204",
            'raphael'               => "libs/editor-md/lib/raphael.min.js?v=20201204",
            'underscore'            => "libs/editor-md/lib/underscore.min.js?v=20201204",
            'flowchart'             => "libs/editor-md/lib/flowchart.min.js?v=20201204",
            'jqueryflowchart'       => "libs/editor-md/lib/jquery.flowchart.min.js?v=20201204",
            'sequenceDiagram'       => "libs/editor-md/lib/sequence-diagram.min.js?v=20201204",
            'katex'                 => "libs/editor-md/lib//katex.min.js?v=20201204",
            'editormd'              => 'libs/editor-md/editormd.amd.min.js?v=20201204',
            'editormd-link'         => 'libs/editor-md/plugins/link-dialog/link-dialog.js?v=20201204',
            'editormd-reference'    => 'libs/editor-md/plugins/reference-link-dialog/reference-link-dialog.js?v=20201204',
            'editormd-image'        => 'libs/editor-md/plugins/image-dialog/image-dialog.js?v=20201204',
            'editormd-code'         => 'libs/editor-md/plugins/code-block-dialog/code-block-dialog.js?v=20201204',
            'editormd-table'        => 'libs/editor-md/plugins/table-dialog/table-dialog.js?v=20201204',
            'editormd-goto'         => 'libs/editor-md/plugins/goto-line-dialog/goto-line-dialog.js?v=20201204',
            // tinymce
            'tinymce'               => 'libs/tinymce/tinymce.min.js?v=20201204',
            // 阿里图表
            'g2'                    => 'libs/AntV/g2plot.js?v=20201207',
            // 七牛云
            'qiniu'                 => 'js/mon-qiniu.js?v=20201204',
            // 表情包
            'emoji'                 => 'libs/monEmoji/mon-emoji.js?v=20210323',
            // 聊天消息解析
            'chatMessage'           => 'chat/message/chatMessage.js?v=' . time()
        ],
        // 资源依赖
        'shim'      => [
            'bootstrap'     => ['jquery'],
            'slimscroll'    => [
                'deps' => ['jquery'],
                'exports' => '$.fn.extend'
            ],
            'adminlte' => [
                'deps' => ['bootstrap', 'slimscroll'],
                'exports' => '$.AdminLTE'
            ],
            'layui' => [
                'deps' => ['css!./libs/layui/css/layui'],
                'exports' => 'layui'
            ],
            'jstree' => ['css!./libs/jstree/themes/default/style'],
            'xmSelect' => [
                'deps' => ['layui'],
                'exports' => 'xmSelect'
            ],
            'monContextMenu' => ['css!./libs/monContextMenu/monContextMenu'],
            'monNav' => ['css!./libs/monNav/monNav'],
            // markdown
            'editormd'  => [
                'deps'  => [
                    'jquery', 'marked', 'prettify', 'raphael', 'underscore', 'flowchart',
                    'jqueryflowchart', 'sequenceDiagram', 'katex',
                    'css!./libs/editor-md/css/editormd.min',
                    'css!./libs/editor-md/lib/codemirror/codemirror.min'
                ],
                'exports'   => 'editormd'
            ],
            'sequenceDiagram'       => [
                'deps'   => ['raphael']
            ],
            // tinymce
            'tinymce'   => [
                'exports'   => 'tinymce'
            ],
            'imgSelect' => ['css!./styles/img-select.css'],
            'emoji' => ['css!./libs/monEmoji/mon-emoji'],
            'chatMessage' => ['css!./chat/message/chatMessage.css'],
        ],
        // map处理
        'map'       => [
            '*' => [
                'css' => './libs/require-css/css.min'
            ]
        ],
    ],
];
