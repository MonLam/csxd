<?php

/**
 * 验证码配置
 */

return [
    // 验证码类型
    'type'      => '',
    // 验证码加密密钥
    'seKey'     => 'mon-captcha',
    // 验证码过期时间（s）
    'expire'    => 180,
    // 验证码字 体大小(px)
    'fontSize'  => 25,
    // 是否画混淆曲线
    'useCurve'  => false,
    // 是否添加杂点
    'useNoise'  => false,
    // 验证码图片高度
    'imageH'    => 0,
    // 验证码图片宽度
    'imageW'    => 0,
    // 验证码位数
    'length'    => 4,
    // 背景颜色
    'bg'        => [243, 251, 254],
    // 验证成功后是否重置
    'reset'     => true,
    // 使用的字体
    'font'      => '',
    // 存储驱动实例，需实现get、set、del方法
    'store'     => \Laf\Session::instance(),
];
