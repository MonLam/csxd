<?php 
/*
|--------------------------------------------------------------------------
| 客服应用配置文件
|--------------------------------------------------------------------------
| 定义客服应用配置信息
|
*/
return [
    // 登录图片验证码标识
    'login_captcha_id'      => 'chat_login',
    // 留言验证码标识
    'stay_captcha_id'       => 'chat_stay',
    // 用户信息session键名
    'userInfo'              => 'chat_user_info',
    // 客服默认头像
    'default_avatar'        => '/static/img/avatar.png',
    // 上传配置
    // 'upload'                => [
    //     // 保存路根径
    //     'root'      => ROOT_PATH . '/public',
    //     // 保存目录
    //     'save'      => '/upload/chat',
    //     // 最大尺寸, 2M
    //     'maxSize'   => 2000000,
    //     // 允许上传文件类型
    //     'exts'      => ['jpg', 'jpeg', 'png', 'gif'],
    // ],
];