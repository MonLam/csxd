<p align="center">
    <img width="180" src="https://gitee.com/MonLam/csxd/raw/master/doc/img/avatar.jpg" />
</p>
<h1 align="center">CSXD客服系统</h1> 
<p align="center">
    <a href="https://gitee.com/MonLam/csxd">
        <img src="https://img.shields.io/badge/OfficialWebsite-CSXD-yellow.svg" />
    </a>
    <a href="https://gitee.com/MonLam/csxd">
        <img src="https://img.shields.io/badge/Licence-Apache2.0-green.svg?style=flat" />
    </a>
    <a href="http://www.crmeb.com">
        <img src="https://img.shields.io/badge/Edition-1.0-blue.svg" />
    </a>
</p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

### 项目介绍

CSXD客服系统是基于GatewayWorker+Vue开发的一套多应用在线客服系统。

支持以Javascript、URL链接等形式快速接入站点，助力站点快速拥有在线客服功能。


### 主要技术栈

前端：VueJS + Vue-Router + Axios + Webpack

管理端：Bootstrap + Jquery + Layui + RequireJS

WebSocket框架：GatewayWorker

PHP框架：Laf


### 功能亮点

~~~
    1. 多客服多应用支持，单客服可对接多个应用
    2. 富文本支持，支持表情、图片、链接等
    3. 离线留言，客服不在线，自动进入留言
    4. 内置强大权限管理模块
    5. 内置文件在线管理系统模块
    6. 内置数据库在线备份系统模块
    7. 独立部署，不限应用、不限客服数
~~~


### 安装

1. 将php目录下storage、config、public/upload赋予可读可写权限

2. 进入php目录中，执行composer指令安装依赖

```base
composer install
```

3. 将Apache或者Nginx的web目录指向php/public目录

4. 访问站点，进行在线安装

5. 启动websocket服务

```base
# 在php目录下执行

php laf socket start -d

```

6. 优化

```base

# 缓存路由表

php laf route cache

# 优化composer加载

composer dump-autoload -o

```

注意：如果为windows环境，则需要将项目根目录中的windows目录移动到php目录中，运行windows/socket.bat

6. 如需使用wss，则可结合workerman相关文档，修改config/socket.php配置文件中对应的配置（[workerman创建wss服务文档](http://doc.workerman.net/faq/secure-websocket-server.html)）


### 页面展示

![环境要求](/doc/img/install1.png "环境要求.png")
![客户留言](/doc/img/stay.png "客户留言.png")
![客户咨询](/doc/img/chat.png "客户咨询.png")
![客户咨询](/doc/img/waiter.png "客户咨询2.png")
![管理端](/doc/img/admin1.png "管理端.png")


### 后续更新

- 移动端支持
- 自动回复支持
- 会话转移、主动邀请、游客咨询音乐播报
